# FaRim GTA V Mod ScriptHook


# About this Mod
This mod is aimed to give the Resource Managment style and gameplay experience in GTA V.


# How to Install
Simply, Copy Paste the DLL files from FaRimMod\bin\Debug\ folder to scripts Folder in GTAV Root Directory.

# Dependencies
Official/Orignal Copy of Grand Theft Auto V(PC Versions Only)
ScriptHookV by Alexander Blade http://www.dev-c.com/gtav/scripthookv
ScriptHookDotNet by Corsire - https://github.com/crosire/scripthookvdotnet/releases
NativeUI by Guad - https://github.com/Guad/NativeUI/releases
SimpleUI by LfxB https://github.com/LfxB/SimpleUI

# Credits
#CamxxCore (Cam Berry)
https://github.com/camxxcore
An amazing modder, scripter, coder and a great peer who is ready to help always.
Without his, Memory class to fetch the static objects from the memory pool, this mod wouldn't have been possible and I was stuck like forever. So, thanks cam. :)

LfxB aka stillhere
Another great friend, who always helped me with my mods, and his suggestions/feedback always helped me to progress further.