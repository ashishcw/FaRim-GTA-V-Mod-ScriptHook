﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;

namespace FaRimMod
{
    public partial class PedClass : Script
    {
        public static Ped AIAssitant1, AIAssitant2, AIAssitant3;

        public static PedHash[] allpedhashes;

        public PedClass()
        {
            AIAssitant1 = new Ped(1);
            AIAssitant2 = new Ped(2);
            AIAssitant3 = new Ped(3);
        }

    }
}
