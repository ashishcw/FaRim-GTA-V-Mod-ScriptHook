﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;
using NativeUI;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace FaRimMod
{
    class JobCreation : Script
    {
        public static TimerBarPool myPool;

        //public static BarTimerBar myBar, WoodBar, StoneBar, CopperBar, SteelBar,
        //                          WheatBar, RiceBar, BeetRootBar, WeedBar,
        //                          MilkBar, CheeseBar, MeatBar;

        public static bool AreaScannerPurchased;
        public static BarTimerBar myBar;

        private bool playersearcharea = false;

        //TextTimerBar CoppedWood, StoneBarText ;
        public static TextTimerBar MyBarText;
        //int _resourcefinderTimer;
        private bool choppingwood, Gathering_Stone, Gathering_Steel, Gathering_Copper;

        public static bool baradded, bardisplayed;
        Blip woodBlip;
        Ped PlayerPed = Class1.PlayerPed;

        Vector3 item1 = new Vector3(-1576.859f, 4527.854f, 17.702f);
        Vector3 item2 = new Vector3(-1544.063f, 4479.107f, 18.950f);
        Vector3 item3 = new Vector3(-1544.063f, 4479.107f, 18.950f);
        //Vector3 item4 = new Vector3(-1375.717f, 4422.273f, 29.050f);
        float[] floatrange = new float[10];

        //GTA.Blip tempblip;
        //List<Vector3> ZonePositionList = new List<Vector3>();
        Vector3 camPos = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_COORD);
        Vector3 camRot = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_ROT);

        static Random random = new Random();

        float retz, retx, tempzonedistance, tempholderdistance, ResourcesPercentage;

        public static string GetEntityName(IntPtr address)
        {
            // probably a better way to get this...
            if ((Marshal.ReadByte(address + 0xC0) & 0x40) != 0)
            {
                var fragInst = Marshal.ReadIntPtr(address + 0x30);

                if (fragInst != IntPtr.Zero)
                {
                    var fragType = Marshal.ReadIntPtr(fragInst + 0x78);

                    if (fragType != IntPtr.Zero)
                    {
                        var str = Marshal.ReadIntPtr(fragType + 0x58);

                        if (str != IntPtr.Zero)
                        {
                            var result = Marshal.PtrToStringAnsi(str);

                            return result?.Substring(result.IndexOf('/') + 1) ?? "";
                        }
                    }
                }
            }

            var drawHandler = Marshal.ReadIntPtr(address + 0x48);

            if (drawHandler == IntPtr.Zero) return null;
            {
                var gtaDrawable = Marshal.ReadIntPtr(drawHandler + 0x8);

                if (gtaDrawable == IntPtr.Zero) return null;

                var result = Marshal.PtrToStringAnsi(Marshal.ReadIntPtr(gtaDrawable + 0xA8));

                if (result != null)
                    return result.Substring(0, result.Length - 4);
            }

            return null;
        }

        //private void CollectNearbyProps()
        //{
        //    if (AreaScannerPurchased)
        //    {
        //        if (Game.GameTime > _resourcefinderTimer + 30000)
        //        {
        //            if (MenuUI.CurrentJobPlayer.Count > 0)
        //            {
        //                playersearcharea = true;
        //                //var props = World.GetNearbyProps(PlayerPed.Position, 20f).ToArray();
        //                Prop[] props = World.GetNearbyProps(PlayerPed.Position, 10f).ToArray();

        //                foreach (var item in props)
        //                {
        //                    if (MenuUI.CurrentJobPlayer[0] == "StoneMining")
        //                    {
        //                        if (item.Model.Hash.ToString() == "-1332461625" || item.Model.Hash.ToString() == "1797043157")
        //                        {
        //                            UI.Notify("Stone Resources Found Nearby");
        //                            //_resourcefinderTimer = Game.GameTime;
        //                        }
        //                        else
        //                        {
        //                            //UI.ShowSubtitle("Searching for Stone resources Nearby");
        //                        }
        //                    }
        //                    else if (MenuUI.CurrentJobPlayer[0] == "ChopWood")
        //                    {
        //                        if (item.Model.Hash.ToString() == "1366334172" || item.Model.Hash.ToString() == "1366334172" || item.Model.Hash.ToString() == "-1381557071")
        //                        {
        //                            UI.Notify("Wood Resources Found Nearby");
        //                            //_resourcefinderTimer = Game.GameTime;
        //                        }
        //                        else
        //                        {
        //                            //UI.ShowSubtitle("Searching for Wood Resources Nearby");
        //                        }
        //                    }
        //                    else if (MenuUI.CurrentJobPlayer[0] == "CopperMining")
        //                    {
        //                        if (item.Model.Hash.ToString() == "17064270" || item.Model.Hash.ToString() == "976638897") //prop_coral_stone_03
        //                        {
        //                            UI.Notify("Copper Resources Found Nearby");
        //                            //_resourcefinderTimer = Game.GameTime;
        //                        }
        //                        else
        //                        {
        //                            //UI.ShowSubtitle("Searching for Wood Resources Nearby");
        //                        }
        //                    }
        //                    else if (MenuUI.CurrentJobPlayer[0] == "SteelMining")
        //                    {
        //                        if (item.Model.Hash.ToString() == "-1053433850" || item.Model.Hash.ToString() == "-1215378248") //prop_rock_4_d
        //                        {
        //                            UI.Notify("Copper Resources Found Nearby");
        //                            //_resourcefinderTimer = Game.GameTime;
        //                        }
        //                        else
        //                        {
        //                            //UI.ShowSubtitle("Searching for Wood Resources Nearby");
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (playersearcharea)
        //                {
        //                    playersearcharea = false;
        //                }
        //            }
        //        }

        //    }

        //}



        private void BarAdd()
        {
            if (myBar != null)
            {
                myPool.Remove(myBar);
                myPool.Remove(MyBarText);
                MyBarText = null;
                myBar = null;
            }

            MyBarText = new TextTimerBar("Gathered Wood", AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString());
            myPool.Add(MyBarText);

            myBar = new BarTimerBar("Wood Chop");
            myBar.Percentage = 0.0f;
            myPool.Add(myBar);

            // You can easily modify the bar color.
            myBar.BackgroundColor = Color.Black;
            myBar.ForegroundColor = Color.Green;
            //choppingwood = true;                
            ResourcesPercentage = floatrange[random.Next(1, 10)];
            baradded = true;

        }


        public double GetRandomNumber(double minimum, double maximum)
        {
            return random.NextDouble() * (float.MaxValue - float.MinValue) + float.MinValue;
            //return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public JobCreation()
        {

            myPool = new TimerBarPool();

            retz = camRot.Z * 0.0174532924F;
            retx = camRot.X * 0.0174532924F;
            floatrange[1] = 0.01f;
            floatrange[2] = 0.02f;
            floatrange[3] = 0.03f;
            floatrange[4] = 0.04f;
            floatrange[5] = 0.05f;
            floatrange[6] = 0.06f;
            floatrange[7] = 0.07f;
            floatrange[8] = 0.08f;
            floatrange[9] = 0.09f;
            
            ////_resourcefinderTimer = Game.GameTime;
            KeyUp += onKeyUp;

            Tick += (o, e) =>
            {
                if (MenuUI.modactive)
                {
                    #region RawResourcesJobs                    

                    if (MenuUI.JobQueue.PlayerJobSelected != null)
                    {
                        if (MenuUI.ZoneBlip != null)
                        {
                            tempzonedistance = World.GetDistance(Game.Player.Character.Position, MenuUI.ZoneBlip.Position);
                            
                        }
                        
                        if (MenuUI.holder != null)
                        {
                            tempholderdistance = World.GetDistance(Game.Player.Character.Position, MenuUI.holder.Position);
                        }
                    }

                    if (MenuUI.CurrentJobPlayer.Count > 0)
                    {
                        if (!Attacker_Class.Trucking_Mission)
                        {
                            if (!baradded)
                            {
                                if (MenuUI.holder != null)
                                {
                                    if (tempholderdistance < 100f)
                                    {
                                        UI.Notify("Go to the Highlighted Item to perform the job");
                                        BarAdd();
                                        bardisplayed = true;
                                    }
                                }
                                else
                                {
                                    if (tempzonedistance < 100f)
                                    {
                                        UI.Notify("Go to the Highlighted Item to perform the job");
                                        BarAdd();
                                        bardisplayed = true;
                                    }
                                }
                            }

                            if (baradded)
                            {
                                #region First
                                if (MenuUI.JobQueue.PlayerJobSelected == "ChopWood")
                                {
                                    if (myBar != null)
                                    {
                                        if (myBar.Label != "Wood Chop")
                                        {
                                            myBar.Label = "Wood Chop";
                                            MyBarText.Label = "Gathered Wood";
                                        }
                                        else
                                        {
                                            MyBarText.Text = AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString();
                                        }
                                    }


                                    if (PlayerPed.Weapons.Current.Hash == WeaponHash.Hatchet && Game.IsControlJustReleased(2, GTA.Control.Attack))
                                    {
                                        choppingwood = true;
                                        ChoppingWoodTask();
                                    }
                                    else
                                    {
                                        if (PlayerPed.IsOnFoot && PlayerPed.Weapons.Current.Hash != WeaponHash.Hatchet && tempholderdistance <= 50f)
                                        {
                                            UI.ShowSubtitle(("You will need to hold the Hatchet to perform the wood chopping job"));
                                        }

                                    }
                                }
                                #endregion


                                #region Second
                                if (MenuUI.JobQueue.PlayerJobSelected == "StoneMining")
                                {
                                    if (myBar != null)
                                    {
                                        if (myBar.Label != "Gathered Stone")
                                        {
                                            myBar.Label = "Gathered Stone";
                                            MyBarText.Label = "Stone Collected";
                                        }
                                        else
                                        {
                                            MyBarText.Text = AvailableResources.ResourcesNumbers.StoneQuantity.ToString();
                                        }

                                    }

                                    if (PlayerPed.Weapons.Current.Hash == WeaponHash.Hammer && Game.IsControlJustReleased(2, GTA.Control.Attack))
                                    {
                                        Gathering_Stone = true;
                                        StoneTask();
                                    }
                                    else
                                    {
                                        if (PlayerPed.IsOnFoot && PlayerPed.Weapons.Current.Hash != WeaponHash.Hammer && tempholderdistance <= 50f)
                                        {
                                            UI.ShowSubtitle(("You will need to hold the Hammer as your current weapon, to perform the Stone Collection job"));
                                        }

                                    }
                                }
                                #endregion


                                #region Third
                                if (MenuUI.JobQueue.PlayerJobSelected == "SteelMining")
                                {
                                    if (myBar != null)
                                    {
                                        if (myBar.Label != "Gathered Steel")
                                        {
                                            myBar.Label = "Gathered Steel";
                                            MyBarText.Label = "Steel Collected";
                                        }
                                        else
                                        {
                                            MyBarText.Text = AvailableResources.ResourcesNumbers.RawSteelQuantity.ToString();
                                        }

                                    }

                                    if (PlayerPed.Weapons.Current.Hash == WeaponHash.Hammer && Game.IsControlJustReleased(2, GTA.Control.Attack))
                                    {
                                        Gathering_Steel = true;
                                        SteelTask();
                                    }
                                    else
                                    {
                                        if (PlayerPed.IsOnFoot && PlayerPed.Weapons.Current.Hash != WeaponHash.Hammer && tempholderdistance <= 50f)
                                        {
                                            UI.ShowSubtitle(("You will need to hold the Hammer as your current weapon, to perform the Steel Collection job"));
                                        }
                                    }
                                }
                                #endregion


                                #region Fourth
                                if (MenuUI.JobQueue.PlayerJobSelected == "CopperMining")
                                {
                                    if (myBar != null)
                                    {
                                        if (myBar.Label != "Gathered Copper")
                                        {
                                            myBar.Label = "Gathered Copper";
                                            MyBarText.Label = "Copper Collected";
                                        }
                                        else
                                        {
                                            MyBarText.Text = AvailableResources.ResourcesNumbers.CopperQuantity.ToString();
                                        }
                                    }

                                    if (PlayerPed.Weapons.Current.Hash == WeaponHash.Machete && Game.IsControlJustReleased(2, GTA.Control.Attack))
                                    {
                                        Gathering_Copper = true;
                                        CopperTask();
                                    }
                                    else
                                    {
                                        if (PlayerPed.IsOnFoot && PlayerPed.Weapons.Current.Hash != WeaponHash.Machete && tempholderdistance <= 50f)
                                        {
                                            //UI.Notify("You will need to hold Hammer to perform the wood chopping job");
                                            UI.ShowSubtitle(("You will need to hold the Machete as your current weapon, to perform the Copper Collection job"));
                                        }
                                    }
                                }
                                #endregion


                                myPool.Draw();
                            }
                        }
                    }
                    
                    #endregion
                    
                    
                    
                    //if (playersearcharea && AreaScannerPurchased)
                    //{
                    //    if (MenuUI.CurrentJobPlayer.Count > 0)
                    //    {
                    //        //World.DrawMarker(MarkerType.CheckeredFlagRect, new Vector3(PlayerPed.Position.X, PlayerPed.Position.Y, PlayerPed.Position.Z - 0.5f), Vector3.Zero, Vector3.Zero, new Vector3(5.0f, 5.0f, 0.1f), Color.Yellow);
                    //        World.DrawMarker(MarkerType.HorizontalCircleSkinny, new Vector3(PlayerPed.Position.X, PlayerPed.Position.Y, PlayerPed.Position.Z - 0.8f), Vector3.Zero, Vector3.Zero, new Vector3(20.0f, 20.0f, 0.1f), Color.Blue);
                    //    }
                        
                    //}


                    //CollectNearbyProps();
                }

            };//Tick event ends here






        }//Default Constructor ends here

        //void ResourceFinder()
        //{
            
        //    int count = 0;

        //    foreach (var buildingList in Memory.MemoryAccess.GetCBuildings(Game.Player.Character.Position, 100.0f))
        //    {

        //        if (count > Math.Min(49, 50)) break;
        //        count++;
        //        if (buildingList.ModelName == "prop_tree_stump")
        //        {
        //            UI.ShowSubtitle("Resource Found");
        //        }
        //        else
        //        {
        //            UI.ShowSubtitle("Resource NOT Found");
        //        }
        //    }
        //}

        
        public void onKeyUp(object sender, KeyEventArgs e)
        {
            if (Class1.DeveloperMode)
            {
                if (MenuUI.modactive == true)
                {
                    //int count = 0;
                    //List<dynamic> TempBuildings = new List<dynamic>();

                    if (e.KeyCode == Keys.O)
                    {
                        
                    }

                    if (e.KeyCode == Keys.G)
                    {
                        //if (TempBuildings != null)
                        //{
                        //    UI.ShowSubtitle(TempBuildings.Count.ToString());
                        //}
                    }

                }
            }

        }//onKeyup function ends here       

        //private void AreaandWayPointSet()
        //{
        //    //Blip area zone needs to be set here from the list of the vector 3 positions either hardcoded or softcoded
        //    //Then call this method where the status reads, Job has been assigned to player
        //    if (AllPositionsListed.ZonePositionList.Count > 0)
        //    {
        //        Random ask = new Random();

        //        int randomnumber = ask.Next(AllPositionsListed.ZonePositionList.Count);
        //        tempblip = GTA.World.CreateBlip(AllPositionsListed.ZonePositionList[randomnumber]);
        //        tempblip.Scale = 10;
        //        tempblip.Alpha = 80;
        //        tempblip.IsShortRange = true;
        //        tempblip.Color = BlipColor.Green;
        //        tempblip.IsFriendly = true;
        //        tempblip.HideNumber();
        //        ResourcesZoneActive = true;
        //    }
        //    else
        //    {
        //        tempblip = GTA.World.CreateBlip(new Vector3(-1375.717f, 4422.273f, 29.050f));
        //        tempblip.Scale = 10;
        //        tempblip.Alpha = 80;
        //        tempblip.IsShortRange = true;
        //        tempblip.Color = BlipColor.Green;
        //        tempblip.IsFriendly = true;
        //        tempblip.HideNumber();
        //        ResourcesZoneActive = true;
        //    }
            
        //}
        #region Resource_JobDefinition
        private void ChoppingWoodTask()
        {
            if (choppingwood)
            {
                Vector3 camPos = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_COORD);
                Vector3 camRot = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_ROT);
                float retz = camRot.Z * 0.0174532924F;
                float retx = camRot.X * 0.0174532924F;
                float absx = (float)Math.Abs(Math.Cos(retx));
                Vector3 camStuff = new Vector3((float)Math.Sin(retz) * absx * -1, (float)Math.Cos(retz) * absx, (float)Math.Sin(retx));
                //AimPostion Result

                RaycastResult ray = World.Raycast(camPos, camPos + camStuff * 1000, IntersectOptions.Everything);

                Vector3 Destination = ray.HitCoords + new Vector3(0f, 0f, 0.5f);

                if (ray.DitHitEntity)
                {
                    GTA.Entity tempmodel = ray.HitEntity;
                    //_resourcefinderTimer = Game.GameTime;
                  
                    if (tempmodel.Model.Hash.ToString() == "-1381557071")
                    {

                        if (myBar.Percentage < 1.0f)
                        {
                            if (Function.Call<bool>(Hash.HAS_ENTITY_ANIM_FINISHED, PlayerPed.Handle))
                            {
                                UI.Notify("Animation Finished");
                            }

                            if (MenuUI.holder != null)
                            {
                                if (tempholderdistance <= 03f)
                                {
                                    //myBar.Percentage += 0.01f;
                                    myBar.Percentage += ResourcesPercentage;

                                    AvailableResources.ResourcesNumbers.ChoppedWoodQuantity += 1.0f;
                                    choppingwood = false;
                                }
                            }
                            


                        }
                        else if (choppingwood)
                        {
                            choppingwood = false;
                            UI.Notify("You have gathered Enough Wood : " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString());
                            Class1.XPPoints.PlayerXP += 50;
                            AvailableResources.ResourcesNumbers.ParentResourceQuantity += AvailableResources.ResourcesNumbers.ChoppedWoodQuantity;
                            NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("XP Earned : 50", "You have completed the Job, thus it earned you some XP. Total XP : " + Class1.XPPoints.PlayerXP.ToString(), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                            myBar.Percentage = 0.0f;

                            GTA.Game.PlaySound("CANCEL", "HUD_MINI_GAME_SOUNDSET");
                            if (MenuUI.CurrentJobPlayer.Count > 0)
                            {
                                UI.Notify("Job Removed : " + MenuUI.CurrentJobPlayer[0]);
                                MenuUI.CurrentJobPlayer.RemoveAt(0);
                                MenuUI.JobQueue.PlayerJobSelected = "";
                                MenuUI.JobQueue.PlayerJobTypeSelected = "";


                                if (woodBlip != null)
                                {
                                    woodBlip.Remove();
                                    woodBlip = null;
                                }
                            }
                            if (MenuUI.holder != null)
                            {
                                if (MenuUI.holderblip != null)
                                {
                                    MenuUI.holderblip.Remove();
                                    MenuUI.holderblip = null;
                                }
                                MenuUI.holder.Delete();
                                MenuUI.holder = null;
                            }
                            //myPool.Remove(myBar);
                            //myPool.Remove(MyBarText);
                            //MyBarText = null;
                            //myBar = null;                            
                            XML_Import_Export.XMLSaving();
                            baradded = false;
                        }
                    }


                    //UI.ShowSubtitle(ray.HitEntity.Model.GetHashCode().ToString());
                }

            }
        }

        private void StoneTask()
        {
            if (Gathering_Stone)
            {
                Vector3 camPos = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_COORD);
                Vector3 camRot = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_ROT);
                float retz = camRot.Z * 0.0174532924F;
                float retx = camRot.X * 0.0174532924F;
                float absx = (float)Math.Abs(Math.Cos(retx));
                Vector3 camStuff = new Vector3((float)Math.Sin(retz) * absx * -1, (float)Math.Cos(retz) * absx, (float)Math.Sin(retx));
                //AimPostion Result

                RaycastResult ray = World.Raycast(camPos, camPos + camStuff * 1000, IntersectOptions.Everything);

                Vector3 Destination = ray.HitCoords + new Vector3(0f, 0f, 0.5f);

                if (ray.DitHitEntity)
                {
                    GTA.Entity tempmodel = ray.HitEntity;
                    //UI.ShowSubtitle(tempmodel.Model.Hash.ToString());
                    //_resourcefinderTimer = Game.GameTime;

                    if (tempmodel.Model.Hash.ToString() == "-1332461625" || tempmodel.Model.Hash.ToString() == "1797043157")//1797043157)
                    {


                        if (myBar.Percentage < 1.0f)
                        {
                            //myBar.Percentage += 0.01f;
                            myBar.Percentage += ResourcesPercentage;
                            //myBar.Percentage += 0.008f;

                            AvailableResources.ResourcesNumbers.StoneQuantity += 1.0f;
                            Gathering_Stone = false;

                        }
                        else if (Gathering_Stone)
                        {
                            Gathering_Stone = false;
                            UI.Notify("You have gathered Enough Stone : " + AvailableResources.ResourcesNumbers.StoneQuantity.ToString());
                            Class1.XPPoints.PlayerXP += 60;
                            AvailableResources.ResourcesNumbers.ParentResourceQuantity += AvailableResources.ResourcesNumbers.StoneQuantity;
                            NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("XP Earned : 60", "You have completed the Job, thus it earned you some XP. Total XP : " + Class1.XPPoints.PlayerXP.ToString(), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                            myBar.Percentage = 0.0f;

                            GTA.Game.PlaySound("CANCEL", "HUD_MINI_GAME_SOUNDSET");
                            if (MenuUI.CurrentJobPlayer.Count > 0)
                            {
                                UI.Notify("Job Removed : " + MenuUI.CurrentJobPlayer[0]);
                                MenuUI.CurrentJobPlayer.RemoveAt(0);
                                MenuUI.JobQueue.PlayerJobSelected = "";
                                MenuUI.JobQueue.PlayerJobTypeSelected = "";


                                if (woodBlip != null)
                                {
                                    woodBlip.Remove();
                                    woodBlip = null;
                                }
                            }

                            if (MenuUI.holder != null)
                            {
                                if (MenuUI.holderblip != null)
                                {
                                    MenuUI.holderblip.Remove();
                                    MenuUI.holderblip = null;
                                }
                                MenuUI.holder.Delete();
                                MenuUI.holder = null;
                            }
                            //myPool.Remove(myBar);
                            //myPool.Remove(MyBarText);
                            //MyBarText = null;
                            //myBar = null;
                            XML_Import_Export.XMLSaving();
                            baradded = false;
                        }
                    }

                }


            }
        }

        private void SteelTask()
        {
            if (Gathering_Steel)
            {
                Vector3 camPos = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_COORD);
                Vector3 camRot = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_ROT);
                float retz = camRot.Z * 0.0174532924F;
                float retx = camRot.X * 0.0174532924F;
                float absx = (float)Math.Abs(Math.Cos(retx));
                Vector3 camStuff = new Vector3((float)Math.Sin(retz) * absx * -1, (float)Math.Cos(retz) * absx, (float)Math.Sin(retx));
                //AimPostion Result

                RaycastResult ray = World.Raycast(camPos, camPos + camStuff * 1000, IntersectOptions.Everything);

                Vector3 Destination = ray.HitCoords + new Vector3(0f, 0f, 0.5f);

                if (ray.DitHitEntity)
                {
                    GTA.Entity tempmodel = ray.HitEntity;                    

                    if (tempmodel.Model.Hash.ToString() == "-1053433850")//1797043157)
                    {

                        if (myBar.Percentage < 1.0f)
                        {
                            //myBar.Percentage += 0.01f;                           
                            myBar.Percentage += ResourcesPercentage;
                            AvailableResources.ResourcesNumbers.RawSteelQuantity += 1.0f;
                            Gathering_Steel = false;

                        }
                        else if (Gathering_Steel)
                        {
                            Gathering_Steel = false;
                            UI.Notify("You have gathered Enough Steel : " + AvailableResources.ResourcesNumbers.RawSteelQuantity.ToString());
                            Class1.XPPoints.PlayerXP += 80;
                            AvailableResources.ResourcesNumbers.ParentResourceQuantity += AvailableResources.ResourcesNumbers.RawSteelQuantity;
                            NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("XP Earned : 80", "You have completed the Job. Total XP : " + Class1.XPPoints.PlayerXP.ToString(), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                            myBar.Percentage = 0.0f;

                            GTA.Game.PlaySound("CANCEL", "HUD_MINI_GAME_SOUNDSET");
                            if (MenuUI.CurrentJobPlayer.Count > 0)
                            {
                                UI.Notify("Job Removed : " + MenuUI.CurrentJobPlayer[0]);
                                MenuUI.CurrentJobPlayer.RemoveAt(0);
                                MenuUI.JobQueue.PlayerJobSelected = "";
                                MenuUI.JobQueue.PlayerJobTypeSelected = "";


                                if (woodBlip != null)
                                {
                                    woodBlip.Remove();
                                    woodBlip = null;
                                }
                            }

                            if (MenuUI.holder != null)
                            {
                                if (MenuUI.holderblip != null)
                                {
                                    MenuUI.holderblip.Remove();
                                    MenuUI.holderblip = null;
                                }
                                MenuUI.holder.Delete();
                                MenuUI.holder = null;
                            }
                            //myPool.Remove(myBar);
                            //myPool.Remove(MyBarText);
                            //MyBarText = null;
                            //myBar = null;
                            XML_Import_Export.XMLSaving();
                            baradded = false;
                        }
                    }

                }
            }
        }

        private void CopperTask()
        {
            if (Gathering_Copper)
            {
                Vector3 camPos = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_COORD);
                Vector3 camRot = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_ROT);
                float retz = camRot.Z * 0.0174532924F;
                float retx = camRot.X * 0.0174532924F;
                float absx = (float)Math.Abs(Math.Cos(retx));
                Vector3 camStuff = new Vector3((float)Math.Sin(retz) * absx * -1, (float)Math.Cos(retz) * absx, (float)Math.Sin(retx));
                //AimPostion Result

                RaycastResult ray = World.Raycast(camPos, camPos + camStuff * 1000, IntersectOptions.Everything);

                Vector3 Destination = ray.HitCoords + new Vector3(0f, 0f, 0.5f);

                if (ray.DitHitEntity)
                {
                    GTA.Entity tempmodel = ray.HitEntity;

                    if (tempmodel.Model.Hash.ToString() == "-1828462170")//1797043157)
                    {

                        if (myBar.Percentage < 1.0f)
                        {
                            //myBar.Percentage += 0.01f;
                            myBar.Percentage += ResourcesPercentage;
                            //myBar.Percentage += 0.008f;

                            AvailableResources.ResourcesNumbers.CopperQuantity += 1.0f;
                            Gathering_Copper = false;

                        }
                        else if (Gathering_Copper)
                        {
                            Gathering_Copper = false;
                            UI.Notify("You have gathered Enough Copper : " + AvailableResources.ResourcesNumbers.CopperQuantity.ToString());
                            Class1.XPPoints.PlayerXP += 100;
                            AvailableResources.ResourcesNumbers.ParentResourceQuantity += AvailableResources.ResourcesNumbers.CopperQuantity;
                            NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("XP Earned : 100", "You have completed the Job. Total XP : " + Class1.XPPoints.PlayerXP.ToString(), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                            myBar.Percentage = 0.0f;

                            GTA.Game.PlaySound("CANCEL", "HUD_MINI_GAME_SOUNDSET");
                            if (MenuUI.CurrentJobPlayer.Count > 0)
                            {
                                UI.Notify("Job Removed : " + MenuUI.CurrentJobPlayer[0]);
                                MenuUI.CurrentJobPlayer.RemoveAt(0);
                                MenuUI.JobQueue.PlayerJobSelected = "";
                                MenuUI.JobQueue.PlayerJobTypeSelected = "";


                                if (woodBlip != null)
                                {
                                    woodBlip.Remove();
                                    woodBlip = null;
                                }
                            }

                            if (MenuUI.holder != null)
                            {
                                if (MenuUI.holderblip != null)
                                {
                                    MenuUI.holderblip.Remove();
                                    MenuUI.holderblip = null;
                                }
                                MenuUI.holder.Delete();
                                MenuUI.holder = null;
                            }
                            //myPool.Remove(myBar);
                            //myPool.Remove(MyBarText);
                            //MyBarText = null;
                            //myBar = null;
                            baradded = false;
                            XML_Import_Export.XMLSaving();
                            
                        }
                    }

                }
            }
        }

        #endregion


    }//main class ends here

    public enum AllPositionsListedEnums
    {
        ResourceZone=1,
        WoodDelivery=2,
        StoneDelivery=3,
        SteelDelivery=4,
        CopperDelivery=5,
        WheatDelivery=6,
        RiceDelivery=7,
        BeetRootDelivery=8,
        WeedDelivery=9,
        MilkDelivery=10,
        CheeseDelivery=11,
        PorkDelivery=12,
    }
    

    public class AllPositionsListed
    {
        public static List<Vector3> ZonePositionList = new List<Vector3>();

        public static List<Vector3> ParentDrivingZonePosition = new List<Vector3>();

        #region Raw_Resources
        public static List<Vector3> WoodDrivingZonePositionList = new List<Vector3>();

        public static List<Vector3> StoneDrivingZonePositionList = new List<Vector3>();

        public static List<Vector3> SteelDrivingZonePositionList = new List<Vector3>();

        public static List<Vector3> CopperDrivingZonePositionList = new List<Vector3>();
        #endregion

        #region Crops_Resources
        public static List<Vector3> WheatCropsDrivingZonePositionList = new List<Vector3>();

        public static List<Vector3> RiceCropsDrivingZonePositionList = new List<Vector3>();

        public static List<Vector3> BeetRootCropsDrivingZonePositionList = new List<Vector3>();

        public static List<Vector3> WeedCropsDrivingZonePositionList = new List<Vector3>();
        #endregion

        #region Processed_Manufactured_Resources

        public static List<Vector3> MilkDrivingZonePositionList = new List<Vector3>();

        public static List<Vector3> CheeseDrivingZonePositionList = new List<Vector3>();

        public static List<Vector3> PorkDrivingZonePositionList = new List<Vector3>();

        #endregion
    }

}//namespace ends here