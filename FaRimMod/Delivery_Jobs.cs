﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;
using NativeUI;
using SimpleUI;
using System.Drawing;

namespace FaRimMod
{
    class Delivery_Jobs : Script
    {
        private static UIContainer container;
        private static UIText text, text2;
        //const int PANEL_WIDTH = 340;
        //const int PANEL_HEIGHT = 20;

        const int PANEL_WIDTH = 340;
        const int PANEL_HEIGHT = 70;
        //const int PANEL_HEIGHT = 50;

        Color backColor = Color.FromArgb(100, 255, 255, 255);
        Color textColor = Color.Red;
        Color textColor2 = Color.Green;

        static SimpleUI.MenuPool _menuPool2;
        static SimpleUI.UIMenu mainMenu2;

        public static GTA.Menu ShownMenu;
        static SimpleUI.UIMenuItem itemSelectFunction, itemSelectFunction2, itemSelectFunction3, itemSelectFunction4;
        static SimpleUI.UIMenuNumberValueItem itemIntegerControl;
        static SimpleUI.UIMenuNumberValueItem itemFloatControl;
        //SimpleUI.UIMenuNumberValueItem itemEnumControl;
        static SimpleUI.UIMenuListItem itemListControl;

        public static GTA.Vehicle WoodTruck, WoodTrailer;
        public static GTA.Blip WoodTruckblip;
        public static Vector3 Delear1, tempDestination_, Destination;

        public static List<dynamic> DrivingLocationsList = new List<dynamic>();

        public static List<dynamic> DrivingLocationsNameList = new List<dynamic>();

        private float distanceremaining;

        public static int DrivingJobXP, DrivingJobMoney, leastBodyhealththreashold, bodyhealth, previousbodyhealth, GoodsLoaded;

        public static bool DrivingJobInprogress, DisplayNotification, JobSelected, DamageMeterDisplay, leftnavigated, DestinationSet = false, MissionFailed;

        private string WooDlocationFolder;

        private static Ped PlayerPed;

        int MenuWidth = 200, Headerheight = 50, FooterHeight = 30;

        public Delivery_Jobs()
        {
            leastBodyhealththreashold = 5;
            previousbodyhealth = 1000;
            DrivingLocationsList.Add(new Vector3(-682.0571f, 5847.38f, 16.64597f));
            DrivingLocationsList.Add(new Vector3(2080.356f, 2341.643f, 93.95109f));
            DrivingLocationsList.Add(new Vector3(1061.937f, -2446.664f, 28.73324f));
            DrivingLocationsList.Add(new Vector3(1880.001f, -1040.597f, 78.88943f));
            DrivingLocationsList.Add(new Vector3(1707.123f, -1482.879f, 112.6227f));
            DrivingLocationsList.Add(new Vector3(-746.2863f, 5543.07f, 32.96365f));
            DrivingLocationsList.Add(new Vector3(-436.8799f, 6205.587f, 28.76165f));
            DrivingLocationsList.Add(new Vector3(1803.432f, -1344.814f, 98.06102f));
            DrivingLocationsList.Add(new Vector3(2370.615f, 2529.328f, 46.39578f));
            DrivingLocationsList.Add(new Vector3(2271.584f, 5153.612f, 56.27338f));

            container = new UIContainer(new Point(UI.WIDTH / 2 - PANEL_WIDTH / 2, 0), new Size(PANEL_WIDTH, PANEL_HEIGHT), backColor);
            text = new UIText("Goods Condition : ", new Point(PANEL_WIDTH / 2, 0), 0.95f, textColor, GTA.Font.HouseScript, true);
            bodyhealth = (int)((double)previousbodyhealth / 100 * 10);
            text2 = new UIText("100%", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.95f, Color.BlueViolet, GTA.Font.HouseScript, true);

            //text = new UIText("Goods Condition : " + bodyhealth + "%", new Point(PANEL_WIDTH / 2, 0), 0.95f, textColor, GTA.Font.HouseScript, true);
            container.Items.Add(text);
            container.Items.Add(text2);

            for (int i = 0; i < DrivingLocationsList.Count; i++)
            {
                DrivingLocationsNameList.Add("Location " + (i + 1));
            }

            PlayerPed = Game.Player.Character;
            Tick += OnTick;
            Menu_Intializer();

        }//Default Constructor Ends Here

        public static void Menu_Intializer()
        {
            _menuPool2 = new SimpleUI.MenuPool();

            // Initialize a menu, with name "Main Menu"
            mainMenu2 = new SimpleUI.UIMenu("~y~Delivery ~g~Jobs");

            // Add mainMenu2 to _menuPool2
            _menuPool2.AddMenu(mainMenu2);


            //if (DrivingLocationsList.Count == 0)
            //{
            //    DrivingLocationsList.Add(new Vector3(2370.615f, 2529.328f, 46.39578f));
            //    DrivingLocationsNameList.Add("Location 1");
            //}

            //itemListControl = new SimpleUI.UIMenuListItem("Available Destinations : ", "Select The Destinations to which You wish to Deliver the Goods", DrivingLocationsList);
            itemListControl = new SimpleUI.UIMenuListItem("Available Destinations : ", "Select The Destinations to which You wish to Deliver the Goods", DrivingLocationsNameList);
            mainMenu2.AddMenuItem(itemListControl);

            itemSelectFunction = new SimpleUI.UIMenuItem("Distance to Travel : ", null, "This shows the Distance to travel");

            itemSelectFunction4 = new SimpleUI.UIMenuItem("Pay : $", null, "This shows the money you will get upon complition of the job");

            mainMenu2.AddMenuItem(itemSelectFunction);
            mainMenu2.AddMenuItem(itemSelectFunction4);

            itemSelectFunction2 = new SimpleUI.UIMenuItem("Done", null, "Finalize the Current Setting");
            mainMenu2.AddMenuItem(itemSelectFunction2);

            itemSelectFunction3 = new SimpleUI.UIMenuItem("Cancle", null, "You will loose all loaded goods to the truck.");
            mainMenu2.AddMenuItem(itemSelectFunction3);

            mainMenu2.OnItemLeftRight += MainMenu_OnItemLeftRight;
            mainMenu2.OnItemSelect += MainMenu_OnItemSelect;

        }

        private static void MainMenu_OnItemSelect(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index)
        {
            if (selectedItem == itemSelectFunction2)
            {
                if (!leftnavigated)
                {
                    MenuUI.PlayerGoodLoadedfromInventory = false;
                    Destination = tempDestination_;
                    DeliveryAreaandWayPointSet();
                    _menuPool2.CloseAllMenus();
                    JobSelected = true;
                    DestinationSet = true;
                    previousbodyhealth = 1000;
                    if (container.Items != null)
                    {
                        container.Items.Remove(text2);
                    }
                    bodyhealth = (int)((double)previousbodyhealth / 100 * 10);
                    container.Items.Remove(text2);
                    text2 = new UIText(bodyhealth + "%", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.95f, Color.BlueViolet, GTA.Font.HouseScript, true);
                    container.Items.Add(text2);
                    if (WoodTruck != null)
                    {
                        WoodTruck.MaxHealth = 1000;
                        WoodTruck.BodyHealth = 1000;
                    }

                    if (WoodTrailer != null)
                    {

                        WoodTrailer.MaxHealth = 1000;
                        WoodTrailer.BodyHealth = 1000;
                        // bodyhealth = WoodTrailer.MaxHealth;

                    }
                    
                }
                else
                {
                    UI.ShowSubtitle("Select the destination from menu, by navigating to the right and not to the left.");
                }



            }

            if (selectedItem == itemSelectFunction3)
            {

                _menuPool2.CloseAllMenus();
                CancleJob();
            }
        }


        private static void MainMenu_OnItemLeftRight(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index, bool left)
        {
            if (selectedItem == itemListControl)
            {
                // if (itemListControl.SelectedIndex <= DrivingLocationsList.Count)
                if (!left)
                {
                    if (itemListControl.SelectedIndex >= DrivingLocationsList.Count)
                    {
                        itemListControl.SelectedIndex = 0;
                    }

                    tempDestination_ = (Vector3)(DrivingLocationsList[itemListControl.SelectedIndex]);
                    float tempdistanceindestination = World.GetDistance(PlayerPed.Position, tempDestination_);
                    itemSelectFunction.Text = "Distance (in Meters) : " + tempdistanceindestination;
                    if (MenuUI.PlayerGoodLoadedfromInventory)
                    {
                        DrivingJobMoney = ((int)tempdistanceindestination * GoodsLoaded / 100 * 10); //Player Inventory Sell
                        itemSelectFunction4.Text = "Pay you get(After 50% Vendor's Cut) : $" + DrivingJobMoney; //80% Vendor Charges
                    }
                    else
                    {
                        if (MenuUI.vendornumber == 1)
                        {
                            DrivingJobMoney = ((int)tempdistanceindestination / 100 * 5); //20% Player Money Pay & 80% Vendor Commision
                        }
                        else if(MenuUI.vendornumber == 2)
                        {
                            DrivingJobMoney = ((int)tempdistanceindestination / 100 * 10); //20% Player Money Pay & 80% Vendor Commision
                        }
                        else if (MenuUI.vendornumber == 3)
                        {
                            DrivingJobMoney = ((int)tempdistanceindestination / 100 * 20); //20% Player Money Pay & 80% Vendor Commision
                        }
                        itemSelectFunction4.Text = "Pay you get(After 80% Vendor's Cut) : $" + DrivingJobMoney; //80% Vendor Charges
                    }
                    

                    if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                    {
                        Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                    }
                    Function.Call(Hash.SET_NEW_WAYPOINT, tempDestination_.X, tempDestination_.Y);
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                    Function.Call(Hash.REFRESH_WAYPOINT);
                    leftnavigated = false;
                }
                else
                {
                    leftnavigated = true;
                    itemListControl.SelectedIndex = 0;
                    tempDestination_ = (Vector3)(DrivingLocationsList[itemListControl.SelectedIndex]);
                    float tempdistanceindestination = World.GetDistance(PlayerPed.Position, tempDestination_);
                    itemSelectFunction.Text = "Distance (in Meters) : " + tempdistanceindestination;

                    if (MenuUI.PlayerGoodLoadedfromInventory)
                    {
                        DrivingJobMoney = ((int)tempdistanceindestination * GoodsLoaded / 100 * 10); //Player Inventory Sell
                        itemSelectFunction4.Text = "Pay you get(After 50% Vendor's Cut) : $" + DrivingJobMoney; //80% Vendor Charges
                    }
                    else
                    {
                        DrivingJobMoney = ((int)tempdistanceindestination / 100 * 20); //20% Player Money Pay & 80% Vendor Commision
                        itemSelectFunction4.Text = "Pay you get(After 80% Vendor's Cut) : $" + DrivingJobMoney; //80% Vendor Charges
                    }

                    if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                    {
                        Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                    }
                    Function.Call(Hash.SET_NEW_WAYPOINT, tempDestination_.X, tempDestination_.Y);
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                    Function.Call(Hash.REFRESH_WAYPOINT);

                    UI.ShowSubtitle("Destination can only be selected pressing the right navigation key");
                }
            }
        }

        private void FailedJob()
        {
            if (MenuUI.CurrentJobPlayer.Count > 0)
            {
                NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Job Failed", "You failed to complete the Job. All the Loaded Resources were lost. No Money or XP Earned" + Class1.XPPoints.PlayerXP.ToString(), HudColor.HUD_COLOUR_RED, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                GTA.Game.PlaySound("Pre_Screen_Stinger", "DLC_HEISTS_FAILED_SCREEN_SOUNDS");
                #region JobCompleted Definition
                UI.Notify("Job Failed : " + MenuUI.CurrentJobPlayer[0]);
                MenuUI.CurrentJobPlayer.RemoveAt(0);
                if (JobCreation.myBar != null)
                {
                    MenuUI.JobQueue.PlayerJobSelected = "";
                    JobCreation.myBar.Percentage = 0.0f;
                    JobCreation.MyBarText.Text = "";
                    JobCreation.MyBarText.Label = "";
                    JobCreation.myBar.Label = "";
                }

                //UI.Notify("Goods Dumped");

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.SET_WAYPOINT_OFF);
                }
                if (Delivery_Jobs.WoodTruck != null)
                {
                    if (Delivery_Jobs.WoodTruck.CurrentBlip.Exists())
                    {
                        Delivery_Jobs.WoodTruck.CurrentBlip.Remove();
                    }
                    if (WoodTruck.IsDriveable)
                    {
                        Delivery_Jobs.WoodTruck.IsDriveable = false;
                    }
                    Delivery_Jobs.WoodTruck.MarkAsNoLongerNeeded();
                    if (Delivery_Jobs.WoodTrailer != null)
                    {
                        Delivery_Jobs.WoodTrailer.MarkAsNoLongerNeeded();
                        Delivery_Jobs.WoodTrailer = null;
                    }

                    Delivery_Jobs.WoodTruck = null;

                    Destination = Vector3.Zero;
                }
                XML_Import_Export.XMLSaving();

                DamageMeterDisplay = false;
                DrivingJobInprogress = false;
                DestinationSet = false;
                bodyhealth = 100;
                Delivery_Jobs.DrivingLocationsList.Clear();
                Delivery_Jobs.DrivingLocationsNameList.Clear();
                MissionFailed = true;
                #endregion
            }
        }

        public static void CancleJob()
        {
            if (MenuUI.CurrentJobPlayer.Count > 0)
            {
                NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Job Canclled", "You have Canclled the Job. All the Loaded Resources were lost. Total XP : " + Class1.XPPoints.PlayerXP.ToString(), HudColor.HUD_COLOUR_RED, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                GTA.Game.PlaySound("Pre_Screen_Stinger", "DLC_HEISTS_FAILED_SCREEN_SOUNDS");
                #region JobCompleted Definition
                UI.Notify("Job Failed : " + MenuUI.CurrentJobPlayer[0]);
                MenuUI.CurrentJobPlayer.RemoveAt(0);
                if (JobCreation.myBar != null)
                {
                    MenuUI.JobQueue.PlayerJobSelected = "";
                    JobCreation.myBar.Percentage = 0.0f;
                    JobCreation.MyBarText.Text = "";
                    JobCreation.MyBarText.Label = "";
                    JobCreation.myBar.Label = "";
                }

                UI.Notify("Goods Dumped");

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.SET_WAYPOINT_OFF);
                }
                if (Delivery_Jobs.WoodTruck != null)
                {
                    if (Delivery_Jobs.WoodTruck.Exists() && Delivery_Jobs.WoodTruck.CurrentBlip.Exists())
                    {
                        Delivery_Jobs.WoodTruck.CurrentBlip.Remove();
                    }
                    Delivery_Jobs.WoodTruck.IsDriveable = false;
                    Delivery_Jobs.WoodTruck.MarkAsNoLongerNeeded();
                    if (Delivery_Jobs.WoodTrailer != null)
                    {
                        Delivery_Jobs.WoodTrailer.MarkAsNoLongerNeeded();
                        Delivery_Jobs.WoodTrailer = null;
                    }

                    Delivery_Jobs.WoodTruck = null;

                    Destination = Vector3.Zero;
                }
                XML_Import_Export.XMLSaving();

                DamageMeterDisplay = false;
                DrivingJobInprogress = false;
                DestinationSet = false;
                bodyhealth = 100;
                Delivery_Jobs.DrivingLocationsList.Clear();
                Delivery_Jobs.DrivingLocationsNameList.Clear();
                MissionFailed = true;
                #endregion
            }
        }


        private void OnTick(object sender, EventArgs e)
        {
            if (MenuUI.modactive)
            {

                if (!PlayerPed.IsOnFoot)
                {
                    //WooDlocationFolder = @".\scripts\TempTexture.jpg";

                    if (WoodTruck != null)
                    {
                        if (PlayerPed.IsInVehicle(WoodTruck))
                        {
                            if (!DestinationSet)
                            {
                                if (!mainMenu2.IsVisible)
                                {
                                    PlayerPed.CurrentVehicle.EngineRunning = false;
                                    PlayerPed.CurrentVehicle.IsDriveable = false;
                                    UI.ShowSubtitle("Destination is not yet selected, select one by pressing E");
                                }
                            }
                            _menuPool2.ProcessMenus();

                            if (!DisplayNotification)
                            {
                                Class1.DisplayHelpTextThisFrame("Press E to Select Destinations to Deliver");
                                DisplayNotification = true;
                            }

                            if (Game.IsControlJustReleased(2, GTA.Control.VehicleHorn))
                            {
                                if (DisplayNotification && !DestinationSet)
                                {
                                    mainMenu2.IsVisible = true;
                                }
                            }

                            distanceremaining = World.GetDistance(PlayerPed.Position, Destination);

                            if (distanceremaining <= 100f)
                            {
                                World.DrawMarker(MarkerType.UpsideDownCone, new Vector3(Destination.X, Destination.Y, Destination.Z + 5.0f), new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f), new Vector3(1.0f, 1.0f, 1.0f), System.Drawing.Color.Red, true, false, 0, false, "", "", false);
                                if (distanceremaining > 50f && Destination != null)
                                {
                                    Class1.DisplayHelpTextThisFrame("Look for a Down Arrow and Park the truck beneth it");
                                }

                                if (distanceremaining <= 05f && DrivingJobInprogress == true)
                                {
                                    Class1.DisplayHelpTextThisFrame("Leave the Truck Here");
                                    Function.Call(Hash._TASK_BRING_VEHICLE_TO_HALT, WoodTruck, 01f, 1, 0);
                                    JobCompleted();
                                }
                            }

                        }
                        //else
                        {

                        }
                    }

                }

                if (WoodTruck != null)
                {
                    if (bodyhealth <= leastBodyhealththreashold)
                    {
                        FailedJob();
                    }
                    //if (PlayerPed.IsOnFoot || !PlayerPed.IsOnFoot)
                    if (PlayerPed.IsOnFoot)
                    {
                        if (WoodTruck != null)
                        {
                            World.DrawMarker(MarkerType.UpsideDownCone, new Vector3(WoodTruck.Position.X, WoodTruck.Position.Y, WoodTruck.Position.Z + 5.0f), new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f), new Vector3(1.0f, 1.0f, 1.0f), System.Drawing.Color.Red, true, false, 0, false, "", "", false);

                        }
                        if (DestinationSet)
                        {
                            //UI.Notify("Get into the Truck to deliver the goods");
                            UI.ShowSubtitle("Get into the Truck to deliver the goods");
                        }

                        //UI.Notify("Get in the Goods-Carrier");
                    }
                }

                if (DestinationSet)
                {
                    if (DestinationSet)
                    {
                        //UI.Notify(bodyhealth.ToString());
                        //DamageMeterDisplay = true;
                        //FuelandDamageDraw();

                        if (WoodTruck != null && WoodTruck.IsDriveable)
                        {
                            if (WoodTrailer != null)
                            {
                                if (container.Items != null && WoodTrailer.BodyHealth < previousbodyhealth)
                                {
                                    UI.ShowSubtitle("Goods Damaged!");
                                    if (WoodTrailer.BodyHealth < previousbodyhealth)
                                    {
                                        if (MenuUI.vendornumber == 1)
                                        {
                                            previousbodyhealth = (int)WoodTrailer.BodyHealth;
                                        }else if (MenuUI.vendornumber == 2)
                                        {
                                            previousbodyhealth = (int)WoodTrailer.BodyHealth - 10;
                                        }
                                        else if(MenuUI.vendornumber == 3)
                                        {
                                            previousbodyhealth = (int)WoodTrailer.BodyHealth - 20;
                                        }

                                        bodyhealth = (int)((double)previousbodyhealth / 100 * 10);
                                        container.Items.Remove(text2);
                                        text2 = new UIText(bodyhealth + "%", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.95f, Color.BlueViolet, GTA.Font.HouseScript, true);
                                        container.Items.Add(text2);
                                    }
                                }
                                else if (WoodTrailer.BodyHealth != previousbodyhealth)
                                {
                                    WoodTrailer.BodyHealth = previousbodyhealth;
                                }
                                container.Draw();
                                if (Function.Call<bool>(Hash.IS_ENTITY_ATTACHED_TO_ENTITY, WoodTrailer, WoodTruck))
                                {
                                    //UI.ShowSubtitle("It's Attached");
                                }
                                else
                                {
                                    //UI.ShowSubtitle("Trailer Detached, attach the trailer first.!");
                                    UI.Notify("Trailer Detached, attach the trailer first.!");
                                    World.DrawMarker(MarkerType.UpsideDownCone, new Vector3(WoodTrailer.Position.X, WoodTrailer.Position.Y, WoodTrailer.Position.Z + 5.0f), new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f), new Vector3(1.0f, 1.0f, 1.0f), System.Drawing.Color.Red, true, false, 0, false, "", "", false);

                                    if (World.GetDistance(WoodTruck.Position, WoodTrailer.Position) <= 05f)
                                    {
                                        //if (GTA.Game.IsControlJustPressed(2, Control.VehicleHorn) && WoodTrailer.IsOnAllWheels)
                                        if (GTA.Game.IsControlJustPressed(2, Control.VehicleHorn))
                                        {
                                            //Function.Call(Hash.IS_ENTITY_ATTACHED_TO_ENTITY, WoodTrailer, WoodTruck);
                                            Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 02f);
                                        }
                                    }
                                }
                            }
                            else if (WoodTruck != null)
                            {
                                //bodyhealth = (int)((double)previousbodyhealth / 100 * 10);                                
                                if (container.Items != null && WoodTruck.BodyHealth < previousbodyhealth)
                                {
                                    UI.ShowSubtitle("Goods Damaged!");
                                    if (WoodTruck.BodyHealth < previousbodyhealth)
                                    {
                                        if (MenuUI.vendornumber == 1)
                                        {
                                            previousbodyhealth = (int)WoodTruck.BodyHealth;
                                        }
                                        else if (MenuUI.vendornumber == 2)
                                        {
                                            previousbodyhealth = (int)WoodTruck.BodyHealth - 10;
                                        }
                                        else if (MenuUI.vendornumber == 3)
                                        {
                                            previousbodyhealth = (int)WoodTruck.BodyHealth - 20;
                                        }                                        
                                        bodyhealth = (int)((double)previousbodyhealth / 100 * 10);
                                        container.Items.Remove(text2);
                                        text2 = new UIText(bodyhealth + "%", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.95f, Color.BlueViolet, GTA.Font.HouseScript, true);
                                        container.Items.Add(text2);
                                    }
                                }
                                else if (WoodTruck.BodyHealth > previousbodyhealth)
                                {
                                    WoodTruck.BodyHealth = previousbodyhealth;
                                }
                                container.Draw();
                            }
                        }
                        else
                        {
                            if (WoodTruck != null)
                            {
                                if (!WoodTruck.IsDriveable || WoodTruck.IsDead)
                                //if (!WoodTruck.IsDriveable || WoodTruck.IsDead || bodyhealth <= 1)
                                {
                                    FailedJob();
                                }
                            }
                        }
                    }
                  
                }
            }
        }


        void DrawCustomText(string Message, float FontSize, int FontType, int Red, int Green, int Blue, int Alpha, float XPos, float YPos)
        {
            Function.Call(Hash.SET_TEXT_SCALE, 0.4f, FontSize);
            Function.Call(Hash.SET_TEXT_FONT, FontType);
            Function.Call(Hash.SET_TEXT_COLOUR, Red, Green, Blue, Alpha);
            Function.Call(Hash.SET_TEXT_DROPSHADOW, 0, 0, 0, 0, 0);
            Function.Call(Hash._SET_TEXT_ENTRY, "STRING");
            Function.Call(Hash._ADD_TEXT_COMPONENT_STRING, Message);
            Function.Call(Hash._DRAW_TEXT, XPos, YPos);


        }

        public static void DrivingJob()
        {

        }

        private void JobCompleted()
        {
            if (MenuUI.CurrentJobPlayer.Count > 0)
            {
                if (bodyhealth >= 98)
                {
                    //Class1.XPPoints.PlayerXP += 100 * 2; //passed
                    //Class1.XPPoints.PlayerMoney += DrivingJobMoney * 2;
                    DrivingJobXP = 100 * 2;
                    DrivingJobMoney = DrivingJobMoney * 2;
                    NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Job Completed", "Goods Deliverd with " + bodyhealth + "% quality." + " Bonus Doubled(XP & Money). XP Earned : " + DrivingJobXP + " Money Earned : " + DrivingJobMoney, HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 20000);
                    GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                    Class1.XPPoints.PlayerXP += DrivingJobXP; //passed
                    Class1.XPPoints.PlayerMoney += DrivingJobMoney;
                }
                else if (bodyhealth > 80 && bodyhealth <= 97)
                {
                    DrivingJobXP = 80;
                    int moneypercent = (int)((double)DrivingJobMoney / 100 * 10);
                    int xppercent = (int)((double)DrivingJobXP / 100 * 10);
                    NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Job Completed", (100 - bodyhealth) + "% Damaged Goods Penalty." + " XP Earned : " + (DrivingJobXP - xppercent) + " Money Earned : " + (DrivingJobMoney - moneypercent), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 20000);
                    GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                    Class1.XPPoints.PlayerXP += DrivingJobXP - xppercent;
                    Class1.XPPoints.PlayerMoney += DrivingJobMoney - moneypercent;
                }
                else if (bodyhealth > 60 && bodyhealth <= 80)
                {
                    DrivingJobXP = 60;
                    int moneypercent = (int)((double)DrivingJobMoney / 100 * 30);
                    int xppercent = (int)((double)DrivingJobXP / 100 * 30);
                    NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Job Completed", (100 - bodyhealth) + "% Damaged Goods Penalty." + " XP Earned : " + (DrivingJobXP - xppercent) + " Money Earned : " + (DrivingJobMoney - moneypercent), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 20000);
                    GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                    Class1.XPPoints.PlayerXP += DrivingJobXP - xppercent;
                    Class1.XPPoints.PlayerMoney += DrivingJobMoney - moneypercent;
                }
                else if (bodyhealth > 40 && bodyhealth <= 60)
                {
                    DrivingJobXP = 40;
                    int moneypercent = (int)((double)DrivingJobMoney / 100 * 50);
                    int xppercent = (int)((double)DrivingJobXP / 100 * 50);
                    NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Job Completed", (100 - bodyhealth) + "% Damaged Goods Penalty." + " XP Earned : " + (DrivingJobXP - xppercent) + " Money Earned : " + (DrivingJobMoney - moneypercent), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 20000);
                    GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                    Class1.XPPoints.PlayerXP += DrivingJobXP - xppercent;
                    Class1.XPPoints.PlayerMoney += DrivingJobMoney - moneypercent;
                }
                else if (bodyhealth > 20 && bodyhealth <= 40)
                {
                    DrivingJobXP = 20;
                    int moneypercent = (int)((double)DrivingJobMoney / 100 * 80);
                    int xppercent = (int)((double)DrivingJobXP / 100 * 80);
                    NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Job Completed", (100 - bodyhealth) + "% Damaged Goods Penalty." + " XP Earned : " + (DrivingJobXP - xppercent) + " Money Earned : " + (DrivingJobMoney - moneypercent), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 20000);
                    GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                    Class1.XPPoints.PlayerXP += DrivingJobXP - xppercent;
                    Class1.XPPoints.PlayerMoney += DrivingJobMoney - moneypercent;
                }
                else if (bodyhealth > leastBodyhealththreashold && bodyhealth <= 20)
                {
                    DrivingJobXP = 10;
                    int moneypercent = (int)((double)DrivingJobMoney / 100 * 90);
                    int xppercent = (int)((double)DrivingJobXP / 100 * 90);
                    NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Job Completed", (100 - bodyhealth) + "% Damaged Goods Penalty." + " XP Earned : " + (DrivingJobXP - xppercent) + " Money Earned : " + (DrivingJobMoney - moneypercent), HudColor.HUD_COLOUR_BLACK, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 20000);
                    GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                    Class1.XPPoints.PlayerXP += DrivingJobXP - xppercent;
                    Class1.XPPoints.PlayerMoney += DrivingJobMoney - moneypercent;
                }
                #region JobCompleted Definition
                UI.Notify("Job Completed : " + MenuUI.CurrentJobPlayer[0]);
                MenuUI.CurrentJobPlayer.RemoveAt(0);
                if (JobCreation.myBar != null)
                {
                    MenuUI.JobQueue.PlayerJobSelected = "";
                    JobCreation.myBar.Percentage = 0.0f;
                    JobCreation.MyBarText.Text = "";
                    JobCreation.MyBarText.Label = "";
                    JobCreation.myBar.Label = "";
                }

                UI.Notify("Goods Delivered");

                if (Delivery_Jobs.WoodTruck != null)
                {
                    if (Delivery_Jobs.WoodTruck.Exists() && Delivery_Jobs.WoodTruck.CurrentBlip.Exists())
                    {
                        Delivery_Jobs.WoodTruck.CurrentBlip.Remove();
                    }
                    Delivery_Jobs.WoodTruck.IsDriveable = false;
                    Delivery_Jobs.WoodTruck.MarkAsNoLongerNeeded();
                    if (Delivery_Jobs.WoodTrailer != null)
                    {
                        Delivery_Jobs.WoodTrailer.MarkAsNoLongerNeeded();
                        Delivery_Jobs.WoodTrailer = null;
                    }
                    Delivery_Jobs.WoodTruck = null;

                    Destination = Vector3.Zero;
                }
                XML_Import_Export.XMLSaving();
                DamageMeterDisplay = false;
                DrivingJobInprogress = false;
                DestinationSet = false;
                //bodyhealth = 100;
                Delivery_Jobs.DrivingLocationsList.Clear();
                Delivery_Jobs.DrivingLocationsNameList.Clear();
                #endregion
            }
        }


        #region VendorJobs
        public static void VendorWoodJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Vendor Delivery Job : Wood");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(1518533038), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(2016027501), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Wood Logs Loaded, and ready for Delivery");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 6;

            }

        }


        public static void VendorStoneJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Delivery Job : Stone");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(-947761570), Delear1);

                UI.Notify("Stone Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select a Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 7;
            }

        }

        public static void VendorSteelJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Delivery Job : Steel");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());                
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(850991848), Delear1);                

                UI.Notify("Steel Junk Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select a Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 6;


            }

        }


        public static void VendorMilkDeliveryJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {

                MenuUI.CurrentJobPlayer.Add("Vendor Delivery Job : Cow Milk");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(-2137348917), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(1956216962), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Milk Units Loaded, and ready for Delivery");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 7;

            }

        }

        public static void VendorCheeseDeliveryJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Vendor Delivery Job : Cheese");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(-2137348917), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(-1579533167), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Cheese Units Loaded, and ready for Delivery");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 6;

            }

        }

        public static void VendorMedicineDeliveryJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Vendor Delivery Job : Medicine");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(1171614426), Delear1);
                //WoodTrailer = World.CreateVehicle(new Model(-1579533167), Delear1);
                bodyhealth = 100;
                //Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Medicine Units Loaded, and ready for Delivery");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 8;
            }

        }


        public static void VendorBurgerDeliveryJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Vendor Delivery Job : Burgers");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(-2137348917), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(-1579533167), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Burgers Units Loaded, and ready for Delivery");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 7;
            }

        }


        public static void VendorCrudeOilDeliveryJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {

                MenuUI.CurrentJobPlayer.Add("Vendor Delivery Job : Oil");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(-2137348917), Delear1); //2112052861
                WoodTrailer = World.CreateVehicle(new Model(1956216962), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Oil Units Loaded, and ready for Delivery");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 6;
            }

        }

        public static void VendorAntiquesDeliveryJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {

                MenuUI.CurrentJobPlayer.Add("Vendor Delivery Job : Antiques");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(2112052861), Delear1);
                bodyhealth = 100;
                UI.Notify("Oil Units Loaded, and ready for Delivery");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("Deliver the Goods to earn the XP and Money");
                DrivingJobInprogress = true;
                MenuUI.JobCriticality = 8;

            }

        }

        #endregion


        #region Player_Delivery_Jobs

        public static void WoodJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Delivery Job : Wood");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);

                if (Vehicel_Shop_Dealer.PlayerTruck == null)
                {
                    WoodTruck = World.CreateVehicle(new Model(1518533038), Delear1);
                    WoodTrailer = World.CreateVehicle(new Model(2016027501), Delear1);
                    bodyhealth = 100;
                    Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                    UI.Notify("Wood Logs Loaded, and ready for Delivery");
                    UI.Notify("Get into the Truck to select the Destination");
                    WoodTruck.AddBlip();
                    WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                    WoodTruck.CurrentBlip.Scale = 0.7f;

                    if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                    {
                        Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                    }
                    Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                    Function.Call(Hash.REFRESH_WAYPOINT);
                    Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                }
                else
                {
                    WoodTruck = Vehicel_Shop_Dealer.PlayerTruck;
                    WoodTrailer = World.CreateVehicle(new Model(2016027501), Delear1);
                    bodyhealth = 100;
                    //Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                    UI.Notify("Wood Logs Loaded, and ready for Delivery");
                    UI.Notify("Drive the Truck to Attach the Trailer");
                    WoodTrailer.AddBlip();
                    WoodTrailer.CurrentBlip.Color = BlipColor.Yellow;
                    WoodTrailer.CurrentBlip.Scale = 0.7f;
                    if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                    {
                        Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                    }
                    Function.Call(Hash.SET_NEW_WAYPOINT, WoodTrailer.Position.X, WoodTrailer.Position.Y);
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                    Function.Call(Hash.REFRESH_WAYPOINT);
                    Class1.DisplayHelpTextThisFrame("You can Deliver the goods using your truck for more profit.");
                }
                DrivingJobInprogress = true;

            }

        }


        public static void StoneJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Delivery Job : Stone");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(-947761570), Delear1);
                //WoodTrailer = World.CreateVehicle(new Model(2016027501), Delear1);

                //Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);

                UI.Notify("Stone Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select a Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;




            }

        }


        public static void SteelJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Delivery Job : Steel");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(850991848), Delear1);
                //WoodTrailer = World.CreateVehicle(new Model(2016027501), Delear1);

                //Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);

                UI.Notify("Steel Junk Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select a Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;



            }

        }

        public static void CopperJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Delivery Job : Copper");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(-1705304628), Delear1);
                //WoodTrailer = World.CreateVehicle(new Model(2016027501), Delear1);

                //Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);

                UI.Notify("Copper Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select a Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;

            }

        }

        public static void WheatJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {

                MenuUI.CurrentJobPlayer.Add("Delivery Job : Wheat");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(2053223216), Delear1);
                //WoodTrailer = World.CreateVehicle(new Model(1956216962), Delear1);
                bodyhealth = 100;
                //Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Wheat Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select the Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;

            }

        }


        public static void RiceJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {

                MenuUI.CurrentJobPlayer.Add("Delivery Job : Rice");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(1518533038), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(-1579533167), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Rice Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select the Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;

            }

        }


        public static void BeetRootJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {

                MenuUI.CurrentJobPlayer.Add("Delivery Job : BeetRoot");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(1518533038), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(-1579533167), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("BeetRoot Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select the Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;

            }

        }


        public static void WeedJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {

                MenuUI.CurrentJobPlayer.Add("Delivery Job : Weed");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(1518533038), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(-1579533167), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Weed Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select the Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;

            }

        }

        //CowMIlk 1956216962 working -305727417,
        //Cheese Trailer, Pork -1579533167
        public static void CowMilkJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {

                MenuUI.CurrentJobPlayer.Add("Delivery Job : Cow Milk");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(1518533038), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(1956216962), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Milk Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select the Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;

            }

        }

        public static void CheeseJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Delivery Job : Cheese");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(1518533038), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(-1579533167), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Cheese Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select the Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;

            }

        }

        public static void PorkJob()
        {
            if (MenuUI.CurrentJobPlayer.Count == 0)
            {
                MenuUI.CurrentJobPlayer.Add("Delivery Job : Pork");
                UI.ShowSubtitle(MenuUI.CurrentJobPlayer[0].ToString());
                //UI.Notify("WoodJOB LOADED");
                Delear1 = new Vector3(-288.558f, 6053.295f, 31.505f);
                WoodTruck = World.CreateVehicle(new Model(1518533038), Delear1);
                WoodTrailer = World.CreateVehicle(new Model(-1579533167), Delear1);
                bodyhealth = 100;
                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, WoodTruck, WoodTrailer, 01f);
                UI.Notify("Pork Units Loaded, and ready for Delivery");
                UI.Notify("Get into the Truck to select the Destination");
                WoodTruck.AddBlip();
                WoodTruck.CurrentBlip.Color = BlipColor.Yellow;
                WoodTruck.CurrentBlip.Scale = 0.7f;

                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, WoodTruck.Position.X, WoodTruck.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                Class1.DisplayHelpTextThisFrame("You can use Vendor Trucks, until you purchase one of your own");
                DrivingJobInprogress = true;

            }

        }
        #endregion




        private static void DeliveryAreaandWayPointSet()
        {
            bodyhealth = 100;
            //Blip area zone needs to be set here from the list of the vector 3 positions either hardcoded or softcoded
            //Then call this method where the status reads, Job has been assigned to player
            //if (AllPositionsListed.WoodDrivingZonePositionList.Count > 0)
            if (DrivingLocationsList.Count > 0)
            {
                UI.Notify("Drive the Truck to Marker waypoint");
                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                {
                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                }
                Function.Call(Hash.SET_NEW_WAYPOINT, Destination.X, Destination.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);

                PlayerPed.CurrentVehicle.EngineRunning = true;
                PlayerPed.CurrentVehicle.IsDriveable = true;

            }
            else
            {
                UI.Notify("Drive the Truck to Marker waypoint");
                Function.Call(Hash.SET_NEW_WAYPOINT, -850.576f, 5413.238f);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);
                WoodTruck.EngineRunning = false;
            }

        }


        //fuelbardraw function
        void FuelandDamageDraw()
        {
            if (DamageMeterDisplay)
            {
                GTA.MenuLabel menu1 = new MenuLabel("TestMenu");
                //View.CloseAllMenus();
                ShownMenu = new GTA.Menu("Damage Meter", new GTA.IMenuItem[] { menu1 });


                //Height & Width
                ShownMenu.HeaderHeight = Headerheight;
                ShownMenu.FooterHeight = FooterHeight;
                ShownMenu.Width = MenuWidth;

                //Color
                ShownMenu.HeaderColor = Color.Black;
                ShownMenu.SelectedItemColor = Color.Red;
                ShownMenu.SelectedTextColor = Color.LawnGreen;
                ShownMenu.HeaderTextColor = Color.AliceBlue;
                ShownMenu.FooterTextColor = Color.Blue;
                ShownMenu.UnselectedItemColor = Color.BlanchedAlmond;
                ShownMenu.UnselectedTextColor = Color.Black;

                //font                
                ShownMenu.HeaderFont = GTA.Font.ChaletLondon;
                ShownMenu.FooterFont = GTA.Font.HouseScript;

                ShownMenu.Position = new System.Drawing.Point(20, Game.ScreenResolution.Height / 2);

                //text scale
                ShownMenu.ItemTextScale = 0.9f;
                ShownMenu.HeaderTextScale = 0.6f;
                ShownMenu.FooterTextScale = 0.4f;


                //text positioning
                ShownMenu.FooterCentered = true;
            }

           
        }

    }//Main class ends here


}//Namespace ends here
