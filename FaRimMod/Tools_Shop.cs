﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Math;
using GTA.Native;
using System.Drawing;
using SimpleUI;
using System.Windows.Forms;


namespace FaRimMod
{

    class Tools_Shop : Script
    {
        public static bool HammerPurchased, HatchetPurchased, MatchetPurchased, KnifePurchased;
        private SimpleUI.MenuPool ToolShop_menuPool;

        private SimpleUI.UIMenu mainMenu2;

        SimpleUI.UIMenuItem FirstItem, DoneItem, PriceItem;
        SimpleUI.UIMenuNumberValueItem itemIntegerControl;
        SimpleUI.UIMenuNumberValueItem itemFloatControl;
        SimpleUI.UIMenuNumberValueItem itemEnumControl;

        #region StoreLocations
        public static Vector3 A = new Vector3(377.999f, 326.656f, 103.566f);
        public static Vector3 B = new Vector3(1137.728f, -981.551f, 46.416f);
        public static Vector3 C = new Vector3(-1223.76f, -905.604f, 12.326f);
        public static Vector3 D = new Vector3(-1488.766f, -380.33f, 40.163f);
        public static Vector3 E = new Vector3(-2970.112f, 390.817f, 15.043f);
        public static Vector3 F = new Vector3(1165.886f, 2706.462f, 38.158f);
        public static Vector3 G = new Vector3(29.839f, -1345.9f, 29.497f);
        public static Vector3 H = new Vector3(-51.545f, -1753.753f, 29.421f);
        public static Vector3 I = new Vector3(1157.933f, -323.593f, 69.205f);
        public static Vector3 J = new Vector3(2555.16f, 385.334f, 108.623f);
        public static Vector3 K = new Vector3(-1824.991f, 790.218f, 138.211f);
        public static Vector3 L = new Vector3(-3042.18f, 588.288f, 7.909f);
        public static Vector3 M = new Vector3(-3243.737f, 1004.556f, 12.831f);
        public static Vector3 N = new Vector3(-712.511f, -913.182f, 19.216f);
        public static Vector3 O = new Vector3(544.962f, 2668.66f, 42.157f);
        public static Vector3 P = new Vector3(2678.268f, 3284.534f, 55.241f);
        public static Vector3 Q = new Vector3(1963.079f, 3744.127f, 32.344f);
        public static Vector3 R = new Vector3(1702.28f, 4927.774f, 42.064f);
        public static Vector3 S = new Vector3(1733.161f, 6415.16f, 35.037f);
        public static Vector3 T = new Vector3(446.018f, -1242.455f, 30.286f);
        #endregion

        static System.Collections.Generic.Dictionary<Vector3, float> AllShopLocations = new Dictionary<Vector3, float>();

        static List<Vector3> Shoplocations = new List<Vector3>();
        public static Ped PlayerPed = GTA.Game.Player.Character;

        public static bool DisplayToolShopMenu, ToolShopMarkeron, Showcone, nearestshopenabled = false;

        //private bool DisplayToolShopMenu = false;

        //private int AreaScannerCost = 1000, HammerPrice = 600, HatchetPrice = 100, MatchetPrice = 800, KnifePrice = 1000, SeedPrice;
        private int Price;





        VehicleHash[] allVehicleHashes = (VehicleHash[])Enum.GetValues(typeof(VehicleHash));
        WeaponHash[] allweaponhashes = (WeaponHash[])Enum.GetValues(typeof(WeaponHash));

        private enum ToolsToDisplay
        {
            //SeachingRadius,
            Hatchet,
            Hammer,
            Machete,
            Knife,
            Wheat_Seed,
            Rice_Seed,
            BeetRoot_Seed,
            Weed_Seed
            //Tool4,
            //Tool5
        }



        ToolsToDisplay testEnum = ToolsToDisplay.Hammer;

        private float testFloat;

        private int testInt;

        private Weapon currentWeapon;
        private bool timescalenable;

        //static GTA.Blip Storelocationsblip1;

        public Tools_Shop()
        {
            //var AllShopLocations = new Dictionary<Vector3, float>();
            MenuIntializer();
            Tick += onTick;
            KeyUp += onKeyUp;
            ShopLocationsAddFunction();
            SelectNearestStore();
        }

        void MenuIntializer()
        {
            ToolShop_menuPool = new MenuPool();

            mainMenu2 = new SimpleUI.UIMenu("~y~Tools ~g~Shop");
            ToolShop_menuPool.AddMenu(mainMenu2);

            itemEnumControl = new SimpleUI.UIMenuNumberValueItem("Available Tools : ", testEnum, "Select the Tool to Purchase");
            mainMenu2.AddMenuItem(itemEnumControl);

            itemIntegerControl = new SimpleUI.UIMenuNumberValueItem("Quantity : ", testInt, "Select the Quantity");
            mainMenu2.AddMenuItem(itemIntegerControl);

            PriceItem = new SimpleUI.UIMenuItem("Price", Price);
            mainMenu2.AddMenuItem(PriceItem);

            FirstItem = new SimpleUI.UIMenuItem("Automated Tools (WIP)", null, "Automated Tools to Purchase/Upgrade. Work In Progress.");
            mainMenu2.AddMenuItem(FirstItem);

            DoneItem = new SimpleUI.UIMenuItem("Done", null, "Finalize the Purchase");
            mainMenu2.AddMenuItem(DoneItem);


            mainMenu2.OnItemLeftRight += MainMenu_OnItemLeftRight;
            mainMenu2.OnItemSelect += MainMenu_OnItemSelect;
        }


        private void MainMenu_OnItemLeftRight(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index, bool left)
        {
            if (selectedItem == itemEnumControl)
            {
                mainMenu2.ControlEnumValue(ref testEnum, itemEnumControl, left);

                if (testEnum.ToString() == "SeachingRadius")
                {
                    itemEnumControl.Description = "Area Scanner is helpful to find the resources nearby, Radius Can Be upgraded";                    
                    Price = 10000;
                    PriceItem.Value = "$" + Price;
                    
                }
                else if (testEnum.ToString() == ToolsToDisplay.Hammer.ToString())
                {
                    itemEnumControl.Description = "Hammer is useful to collect the raw resources such as stone, steel";
                    Price = 600;
                    PriceItem.Value = "$" + Price;
                    HammerPurchased = true;
                }
                else if (testEnum.ToString() == ToolsToDisplay.Hatchet.ToString())
                {
                    itemEnumControl.Description = "Hatchet is useful to collect the raw resources such as Wood";
                    Price = 100;
                    PriceItem.Value = "$" + Price;
                    HatchetPurchased = true;
                }
                else if (testEnum.ToString() == ToolsToDisplay.Machete.ToString())
                {
                    itemEnumControl.Description = "Matchet is useful to Crop the fully grown plants";
                    Price = 800;
                    PriceItem.Value = "$" + Price;
                    MatchetPurchased = true;
                }
                //else if (testEnum.ToString() == ToolsToDisplay.Knife)
                //{
                //    itemEnumControl.Description = "Sow & Grow the Wheat Seeds";
                //}
                else if (testEnum.ToString() == ToolsToDisplay.Wheat_Seed.ToString())
                {
                    itemIntegerControl.Value = 0;
                    testInt = 0;
                    PriceItem.Value = 0;
                    itemEnumControl.Description = "Sow & Grow the Wheat Crops";             
                }
                else if (testEnum.ToString() == ToolsToDisplay.Rice_Seed.ToString())
                {
                    itemIntegerControl.Value = 0;
                    testInt = 0;
                    PriceItem.Value = 0;
                    itemEnumControl.Description = "Sow & Grow the Rice Crops";
                }
                else if (testEnum.ToString() == ToolsToDisplay.BeetRoot_Seed.ToString())
                {
                    itemIntegerControl.Value = 0;
                    testInt = 0;
                    PriceItem.Value = 0;
                    itemEnumControl.Description = "Sow & Grow the BeetRoot Crops";
                }
                else if (testEnum.ToString() == ToolsToDisplay.Weed_Seed.ToString())
                {
                    itemIntegerControl.Value = 0;
                    testInt = 0;
                    PriceItem.Value = 0;
                    itemEnumControl.Description = "Sow & Grow the Weed Seeds";
                }
            }
            else if (selectedItem == itemIntegerControl)
            {
                if (testEnum.ToString() == ToolsToDisplay.Hammer.ToString() || testEnum.ToString() == ToolsToDisplay.Hatchet.ToString() || testEnum.ToString() == ToolsToDisplay.Knife.ToString() || testEnum.ToString() == ToolsToDisplay.Machete.ToString())
                {
                    UI.ShowSubtitle("You can not set the quantity for these items");
                    itemIntegerControl.Value = 0;
                    testInt = 0;
                }
                else
                {
                    mainMenu2.ControlIntValue(ref testInt, itemIntegerControl, left, 01, 50, true, 1, 200);
                    //mainMenu2.ControlEnumValue(ref testInt, itemIntegerControl, left);


                    if (testEnum.ToString() == ToolsToDisplay.BeetRoot_Seed.ToString())
                    {   
                        Price = 3 * testInt;
                        PriceItem.Value = "$" + Price;

                        //set seed quantity
                    }
                    else if (testEnum.ToString() == ToolsToDisplay.Rice_Seed.ToString())
                    {
                        Price = 2 * testInt;
                        PriceItem.Value = "$" + Price;
                        //set seed quantity
                    }
                    else if (testEnum.ToString() == ToolsToDisplay.Wheat_Seed.ToString())
                    {
                        Price = 1 * testInt;
                        PriceItem.Value = "$" + Price;
                        //set seed quantity
                    }
                    else if (testEnum.ToString() == ToolsToDisplay.Weed_Seed.ToString())
                    {
                        Price = 5 * testInt;
                        PriceItem.Value = "$" + Price;
                        //set seed quantity
                    }

                }                
            }
        }

        private void MainMenu_OnItemSelect(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index)
        {
            if (selectedItem == DoneItem)
            {
                if (testEnum.ToString() == "SeachingRadius")
                {
                    if (Class1.XPPoints.PlayerXP >= Price)
                    {
                        Class1.XPPoints.PlayerXP -= Price;
                        UI.Notify("This Item is Unlocked now.");

                        if (Class1.XPPoints.PlayerMoney >= Price)
                        {
                            if (!JobCreation.AreaScannerPurchased)
                            {
                                JobCreation.AreaScannerPurchased = true;
                                Class1.XPPoints.PlayerMoney -= Price;
                            }
                            UI.ShowSubtitle("Area Scanner Purchased");
                            ToolShop_menuPool.CloseAllMenus();
                        }
                        else
                        {
                            UI.ShowSubtitle("more $" + (600 - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                        }
                    }
                    else
                    {
                        UI.ShowSubtitle("Low XP. More " + (Price - Class1.XPPoints.PlayerXP) + " XP Needed to Unlock this item");
                    }

                }
                else if (testEnum.ToString() == WeaponHash.Machete.ToString())
                {
                    if (Class1.XPPoints.PlayerXP >= Price)
                    {
                        Class1.XPPoints.PlayerXP -= Price;
                        UI.Notify("This Item is Unlocked now.");

                        if (Class1.XPPoints.PlayerMoney >= Price)
                        {
                            if (PlayerPed.Weapons.HasWeapon(WeaponHash.Machete))
                            {
                                UI.Notify("You already have a Machete");
                                return;
                            }
                            else
                            {
                                Class1.XPPoints.PlayerMoney -= Price;
                                PlayerPed.Weapons.Give(WeaponHash.Machete, 0, true, true);
                                PlayerPed.Weapons.Select(WeaponHash.Machete, true);
                                Class1.XPPoints.PlayerMoney -= Price;
                                UI.ShowSubtitle("Machete Purchased");
                                UI.Notify("You can Now perform Copper Collecting Jobs from Jobs Menu");
                                ToolShop_menuPool.CloseAllMenus();
                            }

                        }
                        else
                        {
                            UI.ShowSubtitle("more $" + (Price - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                        }
                    }
                    else
                    {
                        UI.ShowSubtitle("Low XP. More " + (Price - Class1.XPPoints.PlayerXP) + " XP Needed to Unlock this item");
                    }
                }
                else if (testEnum.ToString() == WeaponHash.Hammer.ToString())
                {
                    if (Class1.XPPoints.PlayerXP >= Price)
                    {
                        Class1.XPPoints.PlayerXP -= Price;
                        UI.Notify("This Item is Unlocked now.");

                        if (Class1.XPPoints.PlayerMoney >= Price)
                        {
                            if (PlayerPed.Weapons.HasWeapon(WeaponHash.Hammer))
                            {
                                UI.Notify("You already have a Hammer");
                                return;
                            }
                            else
                            {
                                Class1.XPPoints.PlayerMoney -= Price;
                                PlayerPed.Weapons.Give(WeaponHash.Hammer, 0, true, true);
                                PlayerPed.Weapons.Select(WeaponHash.Hammer, true);
                                UI.ShowSubtitle("Hammer Purchased");
                                UI.Notify("Stone & Steel Mining Job are unlocked now.");
                                ToolShop_menuPool.CloseAllMenus();
                            }

                        }
                        else
                        {
                            UI.ShowSubtitle("more $" + (Price - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                        }
                    }
                    else
                    {
                        UI.ShowSubtitle("Low XP. More " + (Price - Class1.XPPoints.PlayerXP) + " XP Needed to Unlock this item");
                    }


                }
                else if (testEnum.ToString() == WeaponHash.Hatchet.ToString())
                {
                    if (Class1.XPPoints.PlayerXP >= Price)
                    {
                        if (Class1.XPPoints.PlayerMoney >= 0)
                        {
                            Class1.XPPoints.PlayerXP -= Price;
                            UI.Notify("This Item is Unlocked now.");

                            if (Class1.XPPoints.PlayerMoney >= Price)
                            {
                                if (PlayerPed.Weapons.HasWeapon(WeaponHash.Hatchet))
                                {
                                    UI.Notify("You already have a Hatchet");
                                    return;
                                    //PlayerPed.Weapons.Remove(WeaponHash.Hatchet);
                                }
                                else
                                {
                                    Class1.XPPoints.PlayerMoney -= Price;
                                    PlayerPed.Weapons.Give(WeaponHash.Hatchet, 0, true, true);
                                    PlayerPed.Weapons.Select(WeaponHash.Hatchet, true);
                                    UI.ShowSubtitle("Hatchet Purchased");
                                    UI.Notify("You can Now perform Wood Chopping Jobs from Jobs Menu");
                                    ToolShop_menuPool.CloseAllMenus();
                                }
                            }
                            else
                            {
                                UI.ShowSubtitle("more $" + (Price - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                            }
                            
                            //PlayerPed.Weapons.Give(WeaponHash.Hatchet, 1, true, true); 1317494643
                        }
                        else
                        {
                            UI.ShowSubtitle("Low XP. More " + (Price - Class1.XPPoints.PlayerXP) + " XP Needed to Unlock this item");
                        }
                    }
                }
                else if (testEnum.ToString() == WeaponHash.Knife.ToString())
                {
                    if (Class1.XPPoints.PlayerXP >= Price)
                    {
                        Class1.XPPoints.PlayerXP -= Price;
                        UI.Notify("This Item is Unlocked now.");

                        if (Class1.XPPoints.PlayerMoney >= Price)
                        {
                            if (PlayerPed.Weapons.HasWeapon(WeaponHash.Knife))
                            {
                                UI.Notify("You already have a Knife");
                                return;
                            }
                            else
                            {
                                Class1.XPPoints.PlayerMoney -= Price;
                                PlayerPed.Weapons.Give(WeaponHash.Knife, 0, true, true);
                                PlayerPed.Weapons.Select(WeaponHash.Knife, true);
                                Class1.XPPoints.PlayerMoney -= Price;
                                UI.ShowSubtitle("Knife Purchased");
                                UI.Notify("You can Now perform Butchering Jobs");
                                ToolShop_menuPool.CloseAllMenus();
                            }

                        }
                        else
                        {
                            UI.ShowSubtitle("more $" + (Price - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                        }
                    }
                    else
                    {
                        UI.ShowSubtitle("Low XP. More " + (Price - Class1.XPPoints.PlayerXP) + " XP Needed to Unlock this item");
                    }
                }
                else if (testEnum.ToString() == ToolsToDisplay.BeetRoot_Seed.ToString())
                {
                    if (Class1.XPPoints.PlayerMoney >= Price)
                    {
                        AvailableResources.ResourcesNumbers.BeetRootSeedQuantity += testInt;
                        Class1.XPPoints.PlayerMoney -= Price;
                        UI.ShowSubtitle(testInt + " BeetRoot Seeds Purchased");
                        UI.Notify("You can Now Sow BeetRoot Crops on the Land you purchased");
                        ToolShop_menuPool.CloseAllMenus();
                    }
                    else
                    {
                        UI.ShowSubtitle("more $" + (Price - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                    }
                }
                else if (testEnum.ToString() == ToolsToDisplay.Rice_Seed.ToString())
                {
                    if (Class1.XPPoints.PlayerMoney >= Price)
                    {
                        AvailableResources.ResourcesNumbers.RiceSeedQuantity += testInt;
                        Class1.XPPoints.PlayerMoney -= Price;
                        UI.ShowSubtitle(testInt + " Rice Seeds Purchased");
                        UI.Notify("You can Now Sow Rice Crops on the Land you purchased");
                        ToolShop_menuPool.CloseAllMenus();
                    }
                    else
                    {
                        UI.ShowSubtitle("more $" + (Price - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                    }
                }
                else if (testEnum.ToString() == ToolsToDisplay.Wheat_Seed.ToString())
                {
                    if (Class1.XPPoints.PlayerMoney >= Price)
                    {
                        AvailableResources.ResourcesNumbers.WheatSeedQuantity += testInt;
                        Class1.XPPoints.PlayerMoney -= Price;
                        UI.ShowSubtitle(testInt + " Wheat Seeds Purchased");
                        UI.Notify("You can Now Sow Wheat Crops on the Land you purchased");
                        ToolShop_menuPool.CloseAllMenus();
                    }
                    else
                    {
                        UI.ShowSubtitle("more $" + (Price - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                    }
                }
                else if (testEnum.ToString() == ToolsToDisplay.Weed_Seed.ToString())
                {
                    if (Class1.XPPoints.PlayerMoney >= Price)
                    {
                        AvailableResources.ResourcesNumbers.WeedSeedQuantity += testInt;
                        Class1.XPPoints.PlayerMoney -= Price;
                        UI.ShowSubtitle(testInt + " Weed Seeds Purchased");
                        UI.Notify("You can Now Sow Weed Crops on the Land you purchased");
                        ToolShop_menuPool.CloseAllMenus();
                    }
                    else
                    {
                        UI.ShowSubtitle("more $" + (Price - Class1.XPPoints.PlayerMoney) + " Needed to Purchase this");
                    }
                }

            }
        }

        void onTick(Object sender, EventArgs e)
        {
            if (MenuUI.modactive)
            {
                ToolShop_menuPool.ProcessMenus();
                //if (Game.IsControlJustPressed(2, GTA.Control.Aim))
                if (Game.IsControlPressed(2, GTA.Control.Aim))
                {
                    Showcone = true;
                }
                else
                {
                    Showcone = false;
                }
                if (Showcone)
                {
                    Vector3 camPos = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_COORD);
                    Vector3 camRot = Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_ROT);
                    float retz = camRot.Z * 0.0174532924F;
                    float retx = camRot.X * 0.0174532924F;
                    float absx = (float)Math.Abs(Math.Cos(retx));
                    Vector3 camStuff = new Vector3((float)Math.Sin(retz) * absx * -1, (float)Math.Cos(retz) * absx, (float)Math.Sin(retx));                 
                    RaycastResult ray = World.Raycast(camPos, camPos + camStuff * 1000, IntersectOptions.Everything);                
                    if (ray.DitHitEntity)
                    {
                        GTA.Entity tempmodel = ray.HitEntity;                        
                        UI.ShowSubtitle(tempmodel.Model.GetHashCode().ToString());

                    }

                }
                
                if (!DisplayToolShopMenu)
                {
                    MarkerDraw();
                }

                if (nearestshopenabled)
                {
                    if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                    {

                    }
                    else
                    {
                        nearestshopenabled = false;
                    }

                }
                if (mainMenu2.IsVisible)
                {
                    
                    //UI.Notify("Every item needs to be unlocked with your XP, before it can be purchased.");
                    //UI.Notify("Item's unlock price will be deducted from your XP.");
                }
                if (DisplayToolShopMenu)
                {
                    if (mainMenu2.IsVisible)
                    {
                        Competitors_Class.MenuActive = true;
                        DisplayToolShopMenu = false;
                    }
                    else
                    {
                        Competitors_Class.MenuActive = false;
                    }
                }
            }
        }

        private void ShopLocationsAddFunction()
        {
            #region StoreLocations
            //A = new Vector3(377.999f, 326.656f, 103.566f);
            //B = new Vector3(1137.728f, -981.551f, 46.416f);
            //C = new Vector3(-1223.76f, -905.604f, 12.326f);
            //D = new Vector3(-1488.766f, -380.33f, 40.163f);
            //E = new Vector3(-2970.112f, 390.817f, 15.043f);
            //F = new Vector3(1165.886f, 2706.462f, 38.158f);
            //G = new Vector3(29.839f, -1345.9f, 29.497f);
            //H = new Vector3(-51.545f, -1753.753f, 29.421f);
            //I = new Vector3(1157.933f, -323.593f, 69.205f);
            //J = new Vector3(2555.16f, 385.334f, 108.623f);
            //K = new Vector3(-1824.991f, 790.218f, 138.211f);
            //L = new Vector3(-3042.18f, 588.288f, 7.909f);
            //M = new Vector3(-3243.737f, 1004.556f, 12.831f);
            //N = new Vector3(-712.511f, -913.182f, 19.216f);
            //O = new Vector3(544.962f, 2668.66f, 42.157f);
            //P = new Vector3(2678.268f, 3284.534f, 55.241f);
            //Q = new Vector3(1963.079f, 3744.127f, 32.344f);
            //R = new Vector3(1702.28f, 4927.774f, 42.064f);
            //S = new Vector3(1733.161f, 6415.16f, 35.037f);
            //T = new Vector3(446.018f, -1242.455f, 30.286f);
            #endregion


            Shoplocations.Add(A);
            Shoplocations.Add(B);
            Shoplocations.Add(C);
            Shoplocations.Add(D);
            Shoplocations.Add(E);
            Shoplocations.Add(F);
            Shoplocations.Add(G);
            Shoplocations.Add(H);
            Shoplocations.Add(I);
            Shoplocations.Add(J);
            Shoplocations.Add(K);
            Shoplocations.Add(L);
            Shoplocations.Add(M);
            Shoplocations.Add(N);
            Shoplocations.Add(O);
            Shoplocations.Add(P);
            Shoplocations.Add(Q);
            Shoplocations.Add(R);
            Shoplocations.Add(S);
            Shoplocations.Add(T);
        }


        public static void MarkerDraw()
        {
            //if (nearestshopenabled)
            {
                if (!DisplayToolShopMenu && ToolShopMarkeron)
                {
                    if ((PlayerPed.Position.DistanceTo2D(A) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(B) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(C) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(D) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(E) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(F) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(G) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(H) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(I) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(J) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(K) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(L) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(M) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(N) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(O) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(P) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(Q) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(R) <= 20.0f) || (PlayerPed.Position.DistanceTo2D(S) <= 20.0f))
                    {

                        foreach (var item in Shoplocations)
                        {
                            if (PlayerPed.Position.DistanceTo(item) <= 20.0f)
                            {
                                {
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(A.X, A.Y, A.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(B.X, B.Y, B.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(C.X, C.Y, C.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(D.X, D.Y, D.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(E.X, E.Y, E.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(F.X, F.Y, F.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(G.X, G.Y, G.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(H.X, H.Y, H.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(I.X, I.Y, I.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(J.X, J.Y, J.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(K.X, K.Y, K.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(L.X, L.Y, L.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(M.X, M.Y, M.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(N.X, N.Y, N.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(O.X, O.Y, O.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(P.X, P.Y, P.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(Q.X, Q.Y, Q.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(R.X, R.Y, R.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(S.X, S.Y, S.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(T.X, T.Y, T.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.40f, 0.45f, 0.3f), Color.Red);
                                    Class1.DisplayHelpTextThisFrame("Walk to the Red Marker and Press Interaction(E on Keyboard) Key, to Display available items to buy");


                                    //UI.ShowSubtitle("Purchase Menu key is : " + PurchaseMenuKey);

                                    if (PlayerPed.Position.DistanceTo(item) <= 01.0f)
                                    {
                                        DisplayToolShopMenu = true;
                                    }
                                }
                            }
                            //else
                            //{
                            //    DisplayToolShopMenu = false;
                            //    UI.ShowSubtitle(DisplayToolShopMenu.ToString());
                            //    //ToolShopMarkeron = false;
                            //}
                        }
                    }
                    else
                    {
                        DisplayToolShopMenu = false;

                        //ToolShopMarkeron = false;
                    }
                }
            }

        }


        public static void SelectNearestStore()
        {
            if (MenuUI.modactive == true)
            {
                if (!nearestshopenabled)
                {


                    if (nearestshopenabled == false)
                    {

                        AllShopLocations[A] = World.GetDistance(PlayerPed.Position, A);
                        AllShopLocations[B] = World.GetDistance(PlayerPed.Position, B);
                        AllShopLocations[C] = World.GetDistance(PlayerPed.Position, C);
                        AllShopLocations[D] = World.GetDistance(PlayerPed.Position, D);
                        AllShopLocations[E] = World.GetDistance(PlayerPed.Position, E);
                        AllShopLocations[F] = World.GetDistance(PlayerPed.Position, F);
                        AllShopLocations[G] = World.GetDistance(PlayerPed.Position, G);
                        AllShopLocations[H] = World.GetDistance(PlayerPed.Position, H);
                        AllShopLocations[I] = World.GetDistance(PlayerPed.Position, I);
                        AllShopLocations[J] = World.GetDistance(PlayerPed.Position, J);
                        AllShopLocations[K] = World.GetDistance(PlayerPed.Position, K);
                        AllShopLocations[L] = World.GetDistance(PlayerPed.Position, L);
                        AllShopLocations[M] = World.GetDistance(PlayerPed.Position, M);
                        AllShopLocations[N] = World.GetDistance(PlayerPed.Position, N);
                        AllShopLocations[O] = World.GetDistance(PlayerPed.Position, O);
                        AllShopLocations[P] = World.GetDistance(PlayerPed.Position, P);
                        AllShopLocations[Q] = World.GetDistance(PlayerPed.Position, Q);
                        AllShopLocations[R] = World.GetDistance(PlayerPed.Position, R);
                        AllShopLocations[S] = World.GetDistance(PlayerPed.Position, S);
                        AllShopLocations[T] = World.GetDistance(PlayerPed.Position, T);


                        var _minimumValue = AllShopLocations.Min(x => x.Value);


                        foreach (var DictValue in AllShopLocations)
                        {
                            if (DictValue.Value == _minimumValue)
                            {
                                UI.Notify("Nearest Store is " + _minimumValue.ToString() + " Meters Away");
                                //UI.Notify(DictValue.ToString());
                                Function.Call(Hash.SET_NEW_WAYPOINT, DictValue.Key.X, DictValue.Key.Y);
                                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                                Function.Call(Hash.REFRESH_WAYPOINT);
                            }
                        }
                        nearestshopenabled = true;
                        ToolShopMarkeron = true;
                    }
                }

            }


        }

        private void onKeyUp(object sender, KeyEventArgs e)
        {
            if (MenuUI.modactive)
            {

                if (e.KeyCode == Keys.E || Game.IsControlPressed(2, GTA.Control.Context) || Game.IsControlPressed(2, GTA.Control.VehicleHorn))
                {
                    if (DisplayToolShopMenu)
                    {
                        DisplayShopMenu();
                    }
                }

            }

            if (Class1.DeveloperMode)
            {
               

                //if (e.KeyCode == Keys.U)
                //{
                //    Showcone = !Showcone;

                //    if (Showcone)
                //    {
                //        UI.ShowSubtitle("Enabled");
                //    }
                //    else
                //    {
                //        UI.ShowSubtitle("Disabled");
                //    }
                //}
            }

        }



        private void DisplayShopMenu()
        {
            if (DisplayToolShopMenu)
            {
                if (!mainMenu2.IsVisible)
                {
                    mainMenu2.IsVisible = true;
                }

                ToolShopMarkeron = false;
            }
        }

    }//Main Class ends here

}//Namespace ends here
