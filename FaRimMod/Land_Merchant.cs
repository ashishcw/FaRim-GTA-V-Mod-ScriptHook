﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GTA;
using GTA.Native;
using GTA.Math;
using System.Windows.Forms;
using SimpleUI;
using System.Drawing;

namespace FaRimMod
{
    class Land_Merchant : Script
    {
        public static bool ShopSelected, MarkerOn, DisplayVehicleMenu, camSet, Land1Purchased;

        GTA.Ped Player;
        Vector3 LandDealerOfficeLocation = new Vector3(-67.303f, -801.868f, 44.227f);
        float DistanceRemaining;

        private Vector3 Player_Current_Position;

        private SimpleUI.MenuPool LandMerchant_menuPool;
        private SimpleUI.UIMenu mainMenu2;
        private SimpleUI.UIMenuItem PriceItem, DoneItem, CancleItem;
        private SimpleUI.UIMenuListItem Land_Locations_List;
        private SimpleUI.UIMenuNumberValueItem itemIntegerControl;

        private GTA.Camera tempcamera;

        private int Land_Price = 60000, LandLocation;

        public Land_Merchant()
        {
            Player = GTA.Game.Player.Character;
            Tick += onTick;
            KeyDown += keyDown;
            MenuIntializer();

        }//Default Constructor Ends here        

        private void onTick(object sender, EventArgs e)
        {
            if (MenuUI.modactive)
            {
                if (ShopSelected)
                {
                    LandMerchant_menuPool.ProcessMenus();

                    DistanceRemaining = World.GetDistance(Player.Position, LandDealerOfficeLocation);

                    if (ShopSelected && DistanceRemaining <= 30f)
                    {
                        MarkerOn = true;
                        Class1.DisplayHelpTextThisFrame("Go to the Red Marker and Pres E");
                        MarkerDrawVehicleShop();
                        DisplayVehicleShopMenu();
                    }
                    else
                    {
                        
                        DisplayVehicleMenu = false;
                    }


                    if (camSet && !mainMenu2.IsVisible)
                    {
                        CancleFunction();
                    }

                    if (MarkerOn)
                    {
                        if (DisplayVehicleMenu)
                        {
                            if (mainMenu2.IsVisible)
                            {
                                SetupCamera();
                                DisplayVehicleMenu = false;
                                MarkerOn = false;
                            }
                        }
                    }
                }
            }
        }

        private void DoneFunction()
        {
            if (tempcamera != null)
            {
                World.DestroyAllCameras();
                World.RenderingCamera = null;
                tempcamera = null;
                camSet = false;
            }

            if (mainMenu2.IsVisible)
            {
                mainMenu2.IsVisible = false;
            }

            if (Player_Current_Position != Vector3.Zero)
            {
                Player.Position = Player_Current_Position;
                Player_Current_Position = Vector3.Zero;
            }
            DisplayVehicleMenu = false;
            ShopSelected = false;
            Function.Call(Hash.DISPLAY_RADAR, true);
        }

        private void CancleFunction()
        {
            if (tempcamera != null)
            {
                World.DestroyAllCameras();
                World.RenderingCamera = null;
                tempcamera = null;
                camSet = false;
            }

            if (mainMenu2.IsVisible)
            {
                mainMenu2.IsVisible = false;
            }

            if (Player_Current_Position != Vector3.Zero)
            {
                Player.Position = Player_Current_Position;
                Player_Current_Position = Vector3.Zero;
            }
            DisplayVehicleMenu = false;
            ShopSelected = false;
            Function.Call(Hash.DISPLAY_RADAR, true);
        }

        private void MarkerDrawVehicleShop()
        {
            if (MarkerOn)
            {
                World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(LandDealerOfficeLocation.X, LandDealerOfficeLocation.Y, LandDealerOfficeLocation.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(1.5f, 1.5f, 1.5f), Color.Red);
            }
        }

        private void DisplayVehicleShopMenu()
        {
            if (DisplayVehicleMenu)
            {
                if (!mainMenu2.IsVisible)
                {
                    mainMenu2.IsVisible = true;
                    MarkerOn = false;
                }

            }
        }
        private void keyDown(object sender, KeyEventArgs e)
        {
            if (MenuUI.modactive)
            {
                float DistanceRemaining1 = World.GetDistance(Player.Position, LandDealerOfficeLocation);
                if (Game.IsControlPressed(2, GTA.Control.VehicleHorn) && DistanceRemaining1 <= 01f)
                {
                    if (Player.IsOnFoot)
                    {
                        var allvechicles = World.GetAllVehicles();

                        if (allvechicles.Length > 0)
                        {
                            foreach (var item in allvechicles)
                            {
                                if (World.GetDistance(item.Position, Player.Position) <= 20f)
                                {
                                    item.Delete();
                                }
                            }
                        }
                        Player_Current_Position = Player.Position;
                        camSet = true;
                        DisplayVehicleMenu = true;
                    }
                    else
                    {
                        UI.Notify("Exit the Current Vehicle");
                    }
                    
                }

            }
        }

        private void MenuIntializer()
        {
            LandMerchant_menuPool = new SimpleUI.MenuPool();

            mainMenu2 = new SimpleUI.UIMenu("~g~Land ~y~Menu");
            LandMerchant_menuPool.AddMenu(mainMenu2);

            //itemEnumControl = new SimpleUI.UIMenuNumberValueItem("Available Tools : ", testEnum, "Select the Tool to Purchase");
            //mainMenu2.AddMenuItem(itemEnumControl);
            List<dynamic> Locations_List = new List<dynamic>()
            {
                "Location 1",                
                "Location 2",
                "Location 3"
            };

            Land_Locations_List = new SimpleUI.UIMenuListItem("Locations : ", "The land Locations", Locations_List);
            mainMenu2.AddMenuItem(Land_Locations_List);

            PriceItem = new SimpleUI.UIMenuItem("Price : ", null, "Price of the Land");
            mainMenu2.AddMenuItem(PriceItem);

            DoneItem = new SimpleUI.UIMenuItem("Done", null, "Done to finalize the Purchase");
            mainMenu2.AddMenuItem(DoneItem);

            CancleItem = new SimpleUI.UIMenuItem("Cancle", null, "Cancle and close this menu");
            mainMenu2.AddMenuItem(CancleItem);
            
            mainMenu2.OnItemSelect += MainMenu2_OnItemSelect;
            mainMenu2.OnItemLeftRight += MainMenu2_OnItemLeftRight;
        }

        private void MainMenu2_OnItemLeftRight(UIMenu sender, UIMenuItem selectedItem, int index, bool left)
        {
            if (selectedItem == Land_Locations_List)
            {
                if (Land_Locations_List.SelectedIndex == 0)
                {
                    PriceItem.Value = "$" + Land_Price;
                    //Display First Land Location here
                }
                else if (Land_Locations_List.SelectedIndex == 1)
                {
                    PriceItem.Value = "Not Available Yet";                    
                }
                else if (Land_Locations_List.SelectedIndex == 2)
                {
                    PriceItem.Value = "Not Available Yet";
                }
            }
        }

        private void MainMenu2_OnItemSelect(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index)
        {
            if (selectedItem == DoneItem)
            {
                //Done Item Code Goes here
                if (Land_Locations_List.SelectedIndex == 0)
                {
                    if (Land_Price <= Class1.XPPoints.PlayerMoney)
                    {
                        NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Land Purchased", "You can Now farm", NativeUI.HudColor.HUD_COLOUR_BLACK, NativeUI.HudColor.HUD_COLOUR_FACEBOOK_BLUE, 5000);
                        GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                        MenuUI.LandPurchased = true;
                        Land1Purchased = true;
                        LandLocation = 1;
                        DoneFunction();

                    }
                    else
                    {
                        UI.Notify("Try performing other jobs to earn the money.");
                        UI.ShowSubtitle("You don't have enough money to buy this property yet.");
                    }
                }
                else
                {
                    UI.ShowSubtitle("This Location is not yet available to purchase.");
                }            

            }
            else if (selectedItem == CancleItem)
            {
                LandMerchant_menuPool.CloseAllMenus();
                //Cancle Item Code Goes here
            }
        }

        private void SetupCamera()
        {
            if (camSet)
            {
                if (tempcamera == null)
                {
                    Vector3 farmpositioncam = new Vector3(284.8731f, 6459.515f, 31.23755f);//(319.5343f, 6583.507f, 29.17677f);
                    GTA.Game.Player.Character.Position = farmpositioncam;
                    tempcamera = World.CreateCamera(new Vector3(284.8731f, 6459.515f, 31.23755f), new Vector3(-10f, 0f, -02f), 75f);
                    tempcamera.Position = new Vector3(tempcamera.Position.X - 50f, tempcamera.Position.Y + 5f, tempcamera.Position.Z + 05f);
                    World.RenderingCamera = tempcamera;
                    Function.Call(Hash.DISPLAY_RADAR, false);
                }
            }

        }//camera function ends here


    }//Default Class ends here

    

}//Namespace Ends here
