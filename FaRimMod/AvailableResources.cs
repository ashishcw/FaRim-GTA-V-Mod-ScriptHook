﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using NativeUI;

namespace FaRimMod
{
    class AvailableResources : Script
    {
        public static class ResourcesNumbers
        {
            public static float ParentResourceQuantity { get; set; }


            #region RawResources Stat
            public static float ChoppedWoodQuantity { get; set; }
            public static float StoneQuantity { get; set; }
            public static float RawSteelQuantity { get; set; }
            public static float CopperQuantity { get; set; }
            #endregion


            #region ProcessedResources Stat
            public static float WheatQuantity { get; set; }
            public static float RiceQuantity { get; set; }
            public static float BeetRootQuantity { get; set; }
            public static float WeedQuantity { get; set; }
            #endregion

            #region HarvestingResources Stat
            public static int WheatSeedQuantity { get; set; }
            public static int RiceSeedQuantity { get; set; }
            public static int BeetRootSeedQuantity { get; set; }
            public static int WeedSeedQuantity { get; set; }
            #endregion


            #region CattleResources Stat
            public static float CowMilkQuantity { get; set; }
            public static float CheeseQuantity { get; set; }
            public static float PigMeatQuantity { get; set; }
            public static float CowDunkQuantity { get; set; }
            #endregion

        }

        public static class PlayerActivityNumbers
        {
            #region ChoppingSpeed
            public static float ChoppingSpeed { get; set; }
            public static float MiningSpeed { get; set; }
            public static float HarvestingSpeed { get; set; }
            public static float CattlingSpeed { get; set; }
            #endregion
        }
        public AvailableResources()
        {

        }//Default Constructor Ends Here

    }//Main Class ends here
}//Namespace ends here
