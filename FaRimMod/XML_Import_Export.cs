﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Xml;
using System.Security.Cryptography;


namespace FaRimMod
{

    class XML_Import_Export
    {
        static string PlayerXPTocheck = Class1.XPPoints.PlayerXP.ToString();
        static XElement wood_resources, stone_resources, steel_resources, copper_resources, Wheat_Seed_resources, Rice_Seed_resources, Beet_Root_Seed_resources, Weed_Seed_resources, Wheat_resources, Rice_resources, BeetRoot_resources, Weed_resources, CowMilk_resources, Cheese_resources, PigMeat_resources;

        public XML_Import_Export()
        {

        }//Default constructor ends here


        public static void XMLSaving()
        {

            //Encryption.Encrypt(PlayerXPTocheck, "PlayerXP");
            //string xmlreaddfile = @".\scripts\FaRim.xml";
            string xmlreaddfile = @".\scripts\FaRim.xml";
            if (File.Exists(xmlreaddfile))
            {
                File.Delete(xmlreaddfile);
            }
            //Create the XML Document
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            //Creates The Parent Node of all the Objects
            XmlNode MainNode = doc.CreateElement("FaRim");            
            doc.AppendChild(MainNode);
            


            XmlNode Node1 = doc.CreateElement("AvailablePlayerXP");
            MainNode.AppendChild(Node1);

            XmlNode nameNode = doc.CreateElement("PlayerXP");
            string EncryptedString = Encryption_Decryption.Encrypt(Class1.XPPoints.PlayerXP.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));

            //nameNode.AppendChild(doc.CreateTextNode(Class1.XPPoints.PlayerXP.ToString()));

            Node1.AppendChild(nameNode);


            #region Player_Money
            Node1 = doc.CreateElement("Player_Money");
            MainNode.AppendChild(Node1);

            nameNode = doc.CreateElement("Money");
            string EncryptedMoneyString = Encryption_Decryption.Encrypt(Class1.XPPoints.PlayerMoney.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedMoneyString));
            Node1.AppendChild(nameNode);

            #endregion


            #region Tools_Purchased
            Node1 = doc.CreateElement("Tools_Purchased");
            MainNode.AppendChild(Node1);

            //Hatchet Purchased Value Goes here
            nameNode = doc.CreateElement("Hatchet_Purchased");
            EncryptedString = Tools_Shop.HatchetPurchased.ToString();
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Hammer Purchased Value Goes here
            nameNode = doc.CreateElement("Hammer_Purchased");
            EncryptedString = Tools_Shop.HammerPurchased.ToString();
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Matchet Purchased Value Goes here
            nameNode = doc.CreateElement("Matchet_Purchased");
            EncryptedString = Tools_Shop.MatchetPurchased.ToString();
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);

            //Knife Purchased Value Goes here
            nameNode = doc.CreateElement("Knife_Purchased");
            EncryptedString = Tools_Shop.KnifePurchased.ToString();
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);
            #endregion


            #region Vehicles_Purchased
            Node1 = doc.CreateElement("Vehicles_Purchased");
            MainNode.AppendChild(Node1);

            //Tractor Purchased Value Goes here
            nameNode = doc.CreateElement("Tractor_Purchased");
            EncryptedString = Vehicel_Shop_Dealer.Tractor_Cultivator_Purchased.ToString();
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Truck Purchased Value Goes here
            nameNode = doc.CreateElement("Truck_Purchased");
            EncryptedString = Vehicel_Shop_Dealer.Truck_Purchased.ToString();
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Mini_Truck Purchased Value Goes here
            nameNode = doc.CreateElement("Mini_Truck_Purchased");
            EncryptedString = Vehicel_Shop_Dealer.Mini_TruckPurchased.ToString();
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);

            #endregion


            #region Land_Purchased
            Node1 = doc.CreateElement("Land_Purchased");
            MainNode.AppendChild(Node1);

            //Tractor Purchased Value Goes here
            nameNode = doc.CreateElement("Land1_Purchased");
            EncryptedString = Land_Merchant.Land1Purchased.ToString();
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);

            #endregion


            #region Raw_Resources
            // Create and add another node.
            Node1 = doc.CreateElement("Gathered_Resources");
            MainNode.AppendChild(Node1);

            //Chopped Wood Value Goes here
            nameNode = doc.CreateElement("Wood");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Gatherd Stone Value Goes here
            nameNode = doc.CreateElement("Stone");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.StoneQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");            
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);



            //Gatherd Steel Value Goes here
            nameNode = doc.CreateElement("Steel");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.RawSteelQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Gatherd Copper Value Goes here
            nameNode = doc.CreateElement("Copper");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.CopperQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);
            #endregion



            #region Seeds_Resources
            // Create and add another node.
            Node1 = doc.CreateElement("Available_Seeds");
            MainNode.AppendChild(Node1);

            //Chopped Wood Value Goes here
            nameNode = doc.CreateElement("Wheat_Seeds");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.WheatSeedQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Gatherd Stone Value Goes here
            nameNode = doc.CreateElement("Rice_Seeds");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.RiceSeedQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);



            //Gatherd Steel Value Goes here
            nameNode = doc.CreateElement("BeetRoot_Seeds");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.BeetRootSeedQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Gatherd Copper Value Goes here
            nameNode = doc.CreateElement("Weed_Seeds");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.WeedSeedQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);
            #endregion



            #region Crop_Resources
            // Create and add another node.
            Node1 = doc.CreateElement("Available_Crops");
            MainNode.AppendChild(Node1);

            //Chopped Wood Value Goes here
            nameNode = doc.CreateElement("Wheat_Crops");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.WheatQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Gatherd Stone Value Goes here
            nameNode = doc.CreateElement("Rice_Crops");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.RiceQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);



            //Gatherd Steel Value Goes here
            nameNode = doc.CreateElement("BeetRoot_Crops");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.BeetRootQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Gatherd Copper Value Goes here
            nameNode = doc.CreateElement("Weed_Crops");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.WeedQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);
            #endregion


            #region Manufactured_Resources
            // Create and add another node.
            Node1 = doc.CreateElement("Processed_Products");
            MainNode.AppendChild(Node1);

            //Chopped Wood Value Goes here
            nameNode = doc.CreateElement("Milk");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.CowMilkQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);


            //Gatherd Stone Value Goes here
            nameNode = doc.CreateElement("Cheese");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.CheeseQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);



            //Gatherd Steel Value Goes here
            nameNode = doc.CreateElement("Pork");
            EncryptedString = Encryption_Decryption.Encrypt(AvailableResources.ResourcesNumbers.PigMeatQuantity.ToString(), "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
            nameNode.AppendChild(doc.CreateTextNode(EncryptedString));
            Node1.AppendChild(nameNode);

            //Gatherd Copper Value Goes here
            //nameNode = doc.CreateElement("Cow_Dunk");
            //nameNode.AppendChild(doc.CreateTextNode(AvailableResources.ResourcesNumbers.CowDunkQuantity.ToString()));
            //Node1.AppendChild(nameNode);

            #endregion




            #region All_Resources_Locations
            // Create and add another node.
            Node1 = doc.CreateElement("Resource_Gather_Locations");
            MainNode.AppendChild(Node1);

            //First Vector3 Value Goes here
            for (int i = 0; i < AllPositionsListed.ZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.ZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");
                
                nameNode = doc.CreateElement("Resource_Zone_Location");
                
                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }

            #endregion



            #region All_Driving_Locations
            // Create and add another node.
            Node1 = doc.CreateElement("Driving_Goods_Locations");
            MainNode.AppendChild(Node1);

            //WoodDelivery Value Goes here
            for (int i = 0; i < AllPositionsListed.WoodDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.WoodDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Wood_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }


            for (int i = 0; i < AllPositionsListed.StoneDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.StoneDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Stone_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }

            for (int i = 0; i < AllPositionsListed.SteelDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.SteelDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Steel_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }
            for (int i = 0; i < AllPositionsListed.CopperDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.CopperDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Copper_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }
            for (int i = 0; i < AllPositionsListed.WheatCropsDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.WheatCropsDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Wheat_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }
            for (int i = 0; i < AllPositionsListed.RiceCropsDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.RiceCropsDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Rice_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }
            for (int i = 0; i < AllPositionsListed.BeetRootCropsDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.BeetRootCropsDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("BeetRoot_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }
            for (int i = 0; i < AllPositionsListed.WeedCropsDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.WeedCropsDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Weed_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }

            for (int i = 0; i < AllPositionsListed.MilkDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.MilkDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Milk_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }

            for (int i = 0; i < AllPositionsListed.CheeseDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.CheeseDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Cheese_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }

            for (int i = 0; i < AllPositionsListed.PorkDrivingZonePositionList.Count; i++)
            {
                string tempstring = AllPositionsListed.PorkDrivingZonePositionList[i].ToString();

                tempstring = tempstring.Replace("X:", "").Replace(" Y:", ", ").Replace(" Z:", ", ");

                nameNode = doc.CreateElement("Pork_Delivery_Locations");

                nameNode.AppendChild(doc.CreateTextNode(tempstring));
                Node1.AppendChild(nameNode);
            }

            #endregion

            doc.Save(xmlreaddfile);

        }
        

    }//Main class ends here


}//Namespace ends here
