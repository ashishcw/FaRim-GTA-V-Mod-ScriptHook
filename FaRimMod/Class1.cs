﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq.Expressions;
using System.Windows.Forms;
using System.Timers;
using System.Linq;
using System.Media;
using System.IO;
//using SimpleUI;
using NativeUI;
using System.Xml;
using System.Text.RegularExpressions;
//using iFruitAddon;


namespace FaRimMod
{
    public partial class Class1 : Script
    {
        
        public Vector3 orignalcameraposition;

        GTA.Ped currentplayerdetails = GTA.Game.Player.Character;

        int player_group;

        public static Ped PlayerPed;

        public string TimeBarName;
                
        string Scriptname = "FaRim";
        public static string ScriptVersion = " 1.0 ";
        string ScriptAuthor = " Ashish Rathi ";

        public static bool modactive = false;

     
        bool markerneedstobedrawn, onjob;

        bool drawimage = false;

        bool farming = false;

        NativeUI.TimerBarPool myPool;
        NativeUI.TextTimerBar MyBarText;
        NativeUI.BarTimerBar myBar;

        List<dynamic> Gathered_Resources, AvailableJobs, Staffing;

        Vector3 TargetCordi { get; }

        public static Keys ShowMenuKey;
        public static bool DeveloperMode;

        Vector3 ChopWoodPosition = new Vector3(-1576.859f, 4527.854f, 17.702f);

        private class PlayerCharacterGeter
        {
            //public static Model PlayerCharactet { get; set; }
            public static string PlayerCharactet { get; set; }
        }



        public class XPPoints
        {
            public static int PlayerXP { get; set; }
            public static string PlayerXPString { get; set; }
            public static int PlayerMoney { get; set; }
        }

        public class LoadSavedData
        {

            public static void SavedDataFound()
            {
                string xmlreaddfile = @".\scripts\FaRim.xml";
                if (File.Exists(xmlreaddfile))
                {
                    string xmlFile = File.ReadAllText(xmlreaddfile);
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(xmlFile);



                    #region Player_XP
                    XmlNodeList nodeList = xmldoc.GetElementsByTagName("AvailablePlayerXP");
                    foreach (XmlNode node in nodeList)
                    {
                        //string tempstring = node.InnerText;
                        //Encryption.Decrypt(tempstring, "PlayerXP");
                        //Class1.XPPoints.PlayerXP = int.Parse(tempstring);
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        Class1.XPPoints.PlayerXP = int.Parse(DecryptedString);

                    }
                    #endregion


                    #region Player_Money
                    nodeList = xmldoc.GetElementsByTagName("Money");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedMoneyString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        Class1.XPPoints.PlayerMoney = int.Parse(DecryptedMoneyString);                        
                    }
                    #endregion

                    #region Tools_Purchased
                    nodeList = xmldoc.GetElementsByTagName("Hatchet_Purchased");
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.InnerText == "True")
                        {
                            Tools_Shop.HatchetPurchased = true;
                            PlayerPed.Weapons.Give(WeaponHash.Hatchet, 0, true, true);                            
                        }
                    }

                    nodeList = xmldoc.GetElementsByTagName("Hammer_Purchased");
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.InnerText == "True")
                        {
                            Tools_Shop.HammerPurchased = true;
                            PlayerPed.Weapons.Give(WeaponHash.Hammer, 0, true, true);
                        }
                    }

                    nodeList = xmldoc.GetElementsByTagName("Matchet_Purchased");
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.InnerText == "True")
                        {
                            Tools_Shop.MatchetPurchased = true;
                            PlayerPed.Weapons.Give(WeaponHash.Machete, 0, true, true);
                        }
                    }

                    nodeList = xmldoc.GetElementsByTagName("Knife_Purchased");
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.InnerText == "True")
                        {
                            Tools_Shop.KnifePurchased = true;
                            PlayerPed.Weapons.Give(WeaponHash.Knife, 0, true, true);
                        }
                    }
                    #endregion

                    #region Vehicles_Purchased
                    nodeList = xmldoc.GetElementsByTagName("Tractor_Purchased");
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.InnerText == "True")
                        {   
                            //Tractor Spawning Location 614.0592, 6456.311, 29.72505                            
                            Vehicel_Shop_Dealer.Tractor_Cultivator_Purchased = true;
                            Vehicel_Shop_Dealer.PlayerTractor = World.CreateVehicle(new Model(-2076478498), new Vector3(614.0592f, 6456.311f, 29.72505f));
                            Vehicel_Shop_Dealer.PlayerCultivator = World.CreateVehicle(new Model(390902130), new Vector3(614.0592f, 6456.311f, 29.72505f));
                            if (!Vehicel_Shop_Dealer.PlayerTractor.IsAttachedTo(Vehicel_Shop_Dealer.PlayerCultivator))
                            {
                                Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, Vehicel_Shop_Dealer.PlayerTractor, Vehicel_Shop_Dealer.PlayerCultivator, 01f);
                            }
                            GTA.Blip TractorBlip = Vehicel_Shop_Dealer.PlayerTractor.AddBlip();
                            TractorBlip.Sprite = BlipSprite.Minigun;
                            TractorBlip.Name = "Tractor";
                        }
                    }

                    nodeList = xmldoc.GetElementsByTagName("Truck_Purchased");
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.InnerText == "True")
                        {
                            //Truck Spawning Location 433.894, 6510.327, 27.90392
                            Vector3 TruckSpawningLocation = new Vector3(433.894f, 6510.327f, 27.90392f);
                            Vehicel_Shop_Dealer.Truck_Purchased = true;                            
                            Vehicel_Shop_Dealer.PlayerTruck = World.CreateVehicle(new Model(-2137348917), new Vector3(TruckSpawningLocation.X, TruckSpawningLocation.Y, TruckSpawningLocation.Z));
                            GTA.Blip TruckBlip = Vehicel_Shop_Dealer.PlayerTruck.AddBlip();
                            TruckBlip.Sprite = BlipSprite.GarbageTruck;
                            TruckBlip.Name = "Cargo_Truck";
                        }
                    }


                    nodeList = xmldoc.GetElementsByTagName("Mini_Truck_Purchased");
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.InnerText == "True")
                        {   
                            //Mini_Truck Spawning Location 450.6838, 6520.471, 28.97419
                            Vector3 MiniTruckSpawningLocation = new Vector3(450.6838f, 6520.471f, 28.97419f);
                            Vehicel_Shop_Dealer.Mini_TruckPurchased = true;
                            Vehicel_Shop_Dealer.PlayerCargoMiniTruck = World.CreateVehicle(new Model(904750859), new Vector3(MiniTruckSpawningLocation.X, MiniTruckSpawningLocation.Y, MiniTruckSpawningLocation.Z));                            
                            GTA.Blip Mini_TruckBlip = Vehicel_Shop_Dealer.PlayerCargoMiniTruck.AddBlip();
                            Mini_TruckBlip.Sprite = BlipSprite.TowTruck;
                            Mini_TruckBlip.Name = "Cargo_Truck";                            
                        }
                    }
                    #endregion


                    #region Land_Purchase_Status
                    nodeList = xmldoc.GetElementsByTagName("Land1_Purchased");
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.InnerText == "True")
                        {   
                            //Farm Location 662.4656, 6469.922, 30.1526
                            Vector3 Farm_Location = new Vector3(662.4656f, 6469.922f, 30.1526f);
                            Land_Merchant.Land1Purchased = true;                            
                            
                            GTA.Blip Land_Blip = GTA.World.CreateBlip(Farm_Location);                            
                            Land_Blip.Sprite = BlipSprite.Omega;
                            Land_Blip.Name = "Farm 1";
                        }
                    }
                    #endregion

                    #region RawResourcesQuanityties
                    nodeList = xmldoc.GetElementsByTagName("Wood");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");                        
                        AvailableResources.ResourcesNumbers.ChoppedWoodQuantity = int.Parse(DecryptedString);
                    }


                    nodeList = xmldoc.GetElementsByTagName("Stone");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.StoneQuantity = int.Parse(DecryptedString);
                    }

                    nodeList = xmldoc.GetElementsByTagName("Steel");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.RawSteelQuantity = int.Parse(DecryptedString);
                    }

                    nodeList = xmldoc.GetElementsByTagName("Copper");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.CopperQuantity = int.Parse(DecryptedString);
                    }
                    #endregion

                    #region Crops_Resources
                    nodeList = xmldoc.GetElementsByTagName("Wheat_Crops");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.WheatQuantity = int.Parse(DecryptedString);
                    }


                    nodeList = xmldoc.GetElementsByTagName("Rice_Crops");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.RiceQuantity = int.Parse(DecryptedString);
                    }

                    nodeList = xmldoc.GetElementsByTagName("BeetRoot_Crops");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.BeetRootQuantity = int.Parse(DecryptedString);
                    }


                    nodeList = xmldoc.GetElementsByTagName("Weed_Crops");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.WeedQuantity = int.Parse(DecryptedString);
                    }

                    #endregion

                    #region Available_Seeds                    

                    nodeList = xmldoc.GetElementsByTagName("Wheat_Seeds");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.WheatSeedQuantity = int.Parse(DecryptedString);
                    }


                    nodeList = xmldoc.GetElementsByTagName("Rice_Seeds");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.RiceSeedQuantity = int.Parse(DecryptedString);
                    }

                    nodeList = xmldoc.GetElementsByTagName("BeetRoot_Seeds");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.BeetRootQuantity = int.Parse(DecryptedString);
                    }

                    nodeList = xmldoc.GetElementsByTagName("Weed_Seeds");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.WeedSeedQuantity = int.Parse(DecryptedString);
                    }
                    #endregion


                    #region Processed_Products                    

                    nodeList = xmldoc.GetElementsByTagName("Milk");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.CowMilkQuantity = int.Parse(DecryptedString);
                    }


                    nodeList = xmldoc.GetElementsByTagName("Cheese");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.CheeseQuantity = int.Parse(DecryptedString);
                    }

                    nodeList = xmldoc.GetElementsByTagName("Pork");
                    foreach (XmlNode node in nodeList)
                    {
                        string DecryptedString = Encryption_Decryption.Decrypt(node.InnerText, "bxwD5j4LK4NEvlakuPp1g==", "JU=JVGLSFSgN4=!-LW", "H2+_=S*QFA=P!Gu_");
                        AvailableResources.ResourcesNumbers.PigMeatQuantity = int.Parse(DecryptedString);
                    }

                    //nodeList = xmldoc.GetElementsByTagName("Weed_Seeds");
                    //foreach (XmlNode node in nodeList)
                    //{
                    //    AvailableResources.ResourcesNumbers.WeedSeedQuantity = int.Parse(node.InnerText);
                    //}
                    #endregion




                    #region Resources_Gather_Region/ZONE
                    float position0 = 0f;
                    float position1 = 0f;
                    float position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Resource_Zone_Location");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }
                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();
                        Vector3 temposition = new Vector3(position0, position1, position2);
                        AllPositionsListed.ZonePositionList.Add(temposition);


                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.ZonePositionList.Add(temposition);
                        //}                        
                        //9420213738 - Shraddha 8408805150
                    }                  


                    #endregion



                    #region Wood_Delivery_Locations
                    float Delivery_position0 = 0f;
                    float Delivery_position1 = 0f;
                    float Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Wood_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {  
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.WoodDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);


                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.WoodDrivingZonePositionList.Add(temposition);
                        //    AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //}





                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();



                    }



                    #endregion

                    #region Stone_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Stone_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.StoneDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                  

                    }



                    #endregion


                    #region Steel_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Steel_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.SteelDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.SteelDrivingZonePositionList.Add(temposition);
                        //    AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //}




                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();



                    }



                    #endregion

                    #region Copper_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Copper_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.CopperDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.CopperDrivingZonePositionList.Add(temposition);
                        //    AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //}




                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();



                    }



                    #endregion


                    #region Wheat_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Wheat_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.WheatCropsDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.WheatCropsDrivingZonePositionList.Add(temposition);
                        //    AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //}




                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();



                    }



                    #endregion

                    #region Rice_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Rice_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.RiceCropsDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.RiceCropsDrivingZonePositionList.Add(temposition);
                        //    AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //}




                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();



                    }



                    #endregion

                    #region BeetRoot_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("BeetRoot_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.BeetRootCropsDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.BeetRootCropsDrivingZonePositionList.Add(temposition);
                        //    AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //}




                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();



                    }



                    #endregion

                    #region Weed_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Weed_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.WeedCropsDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.WeedCropsDrivingZonePositionList.Add(temposition);
                        //    AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //}




                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();



                    }



                    #endregion

                    #region Milk_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Milk_Delivery_Locations");

                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;                        
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.MilkDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                       
                    }



                    #endregion

                    #region Cheese_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Cheese_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.CheeseDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                     
                    }



                    #endregion


                    #region Pork_Delivery_Locations
                    Delivery_position0 = 0f;
                    Delivery_position1 = 0f;
                    Delivery_position2 = 0f;
                    nodeList = xmldoc.GetElementsByTagName("Pork_Delivery_Locations");
                    foreach (XmlNode node in nodeList)
                    {
                        string str = null;
                        string[] strArr = null;
                        int count = 0;
                        //str = "CSharp split test";
                        str = node.InnerText;
                        //char[] splitchar = { ' ' };
                        char[] splitchar = { ',' };
                        strArr = str.Split(splitchar);
                        for (count = 0; count <= strArr.Length - 1; count++)
                        {
                            if (count == 0)
                            {
                                Delivery_position0 = float.Parse(strArr[count]);
                            }
                            else if (count == 1)
                            {
                                Delivery_position1 = float.Parse(strArr[count]);
                            }
                            else if (count == 2)
                            {
                                Delivery_position2 = float.Parse(strArr[count]);
                            }
                            //UI.Notify(strArr[count]);


                        }

                        Vector3 temposition = new Vector3(Delivery_position0, Delivery_position1, Delivery_position2);
                        AllPositionsListed.PorkDrivingZonePositionList.Add(temposition);
                        AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //for (int i = 0; i < 10; i++)
                        //{
                        //    AllPositionsListed.PorkDrivingZonePositionList.Add(temposition);
                        //    AllPositionsListed.ParentDrivingZonePosition.Add(temposition);
                        //}




                        //string temptext = node.InnerText;

                        //UI.Notify(temptext);

                        //AllPositionsListed.ZonePositionList.Add();



                    }



                    #endregion


                    
                    UI.Notify("FaRim Mod Data Loaded");


                }
            }
        }

        public Class1()
        {

            PedClass.allpedhashes = (PedHash[])Enum.GetValues(typeof(PedHash));

            Tick += onTick;
            KeyUp += onKeyUp;
            PlayerPed = Game.Player.Character;
            UI.Notify(Scriptname + ScriptVersion + "by " + ScriptAuthor);
            //Resources = Resources = new List<dynamic> { "Item 1", "Item 2", "Item 3" };
            AvailableJobs = new List<dynamic> { "Item 1", "Item 2", "Item 3" };
            Gathered_Resources = new List<dynamic> { "Wood : 100", "Stone : 542", "Copper : 421" };
            Staffing = new List<dynamic> { "James C.", "David Williams", "TEST PED" };
            LoadSettings();
            PlayerXPManager();
            player_group = Function.Call<int>(GTA.Native.Hash.GET_PLAYER_GROUP, GTA.Game.Player.Character.Handle);

            //JobQueue = new UIMenu("Available Jobs : ", "You will able to find the available Jobs here");
            LoadSavedData.SavedDataFound();
           

        }//Default Constructor Ends here.

      

        void onTick(Object sender, EventArgs e)
        {
            if (MenuUI.modactive)
            {
              
                if (PlayerPed != Game.Player.Character)
                {
                    PlayerPed = Game.Player.Character;
                }

                if (drawimage)
                {
                    World.DrawMarker(MarkerType.UpsideDownCone, new Vector3(PlayerPed.Position.X, PlayerPed.Position.Y, PlayerPed.Position.Z), new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f), new Vector3(0.2f, 0.2f, 0.2f), Color.Blue, false, false, 0, false, "", "", false);
                }

                if (XPPoints.PlayerMoney < 0)
                {
                    XPPoints.PlayerMoney = 0;
                }

                if (XPPoints.PlayerXP < 0)
                {
                    XPPoints.PlayerXP = 0;
                }               
            }
        }
        
        void LoadSettings()
        {
            try
            {
                ScriptSettings config = ScriptSettings.Load(@".\scripts\FaRim.ini");
                
                ShowMenuKey = config.GetValue<Keys>("Keys", "SHOWMENUKEY", Keys.F12);
                DeveloperMode = config.GetValue<bool>("Keys", "DeveloperMode", false);

            }

            catch (Exception e)
            {
                UI.Notify(e.Message);
            }
        }


        public static void PlayerXPManager()
        {
            //XPPoints.PlayerXP = 10;
        }


        public static void DisplayHelpTextThisFrame(string text)
        {
            Function.Call(Hash._SET_TEXT_COMPONENT_FORMAT, "STRING");
            Function.Call(Hash._ADD_TEXT_COMPONENT_STRING, text);
            Function.Call(Hash._0x238FFE5C7B0498A6, 0, 0, 1, -1);
        }

       
        public void onKeyUp(object sender, KeyEventArgs e)
        {
            
            if (DeveloperMode)
            {
                if (e.KeyCode == Keys.T)
                {
                    //XPPoints.PlayerXP += 10000;
                    //XPPoints.PlayerMoney += 10000;
                    //UI.ShowSubtitle("XP Left : " + XPPoints.PlayerXP.ToString());
                    //UI.Notify("Money left : " + XPPoints.PlayerMoney.ToString());                   
                }

                if (e.KeyCode == Keys.B)
                {
                    //XPPoints.PlayerXP = 0;
                    //XPPoints.PlayerMoney = 0;                    

                    //UI.ShowSubtitle("XP Left : " + XPPoints.PlayerXP.ToString());
                    //UI.Notify("Money left : " + XPPoints.PlayerMoney.ToString());
                }
                
                if (MenuUI.modactive == true)
                {
                    if (e.KeyCode == Keys.L)
                    {
                        
                    }

                }
            }

        }//onKeyup function ends here       



    }//Main class ends here


}//main namespace ends here
