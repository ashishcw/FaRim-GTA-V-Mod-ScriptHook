﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using NativeUI;
using GTA.Math;
using GTA.Native;
using System.Windows.Forms;

namespace FaRimMod
{
    public static class Priorities_Class
    {
        public static int GunFight_Assistant1 { set; get; }
        public static int Driving_Assistant1 { set; get; }
        public static int Farming_Assistant1 { set; get; }
        public static int Negotiation_Assistant1 { set; get; }


        public static int GunFight_Assistant2 { set; get; }
        public static int Driving_Assistant2 { set; get; }
        public static int Farming_Assistant2 { set; get; }
        public static int Negotiation_Assistant2 { set; get; }


        public static int GunFight_Assistant3 { set; get; }
        public static int Driving_Assistant3 { set; get; }
        public static int Farming_Assistant3 { set; get; }
        public static int Negotiation_Assistant3 { set; get; }


        public static int GunFight_Assistant4 { set; get; }
        public static int Driving_Assistant4 { set; get; }
        public static int Farming_Assistant4 { set; get; }
        public static int Negotiation_Assistant4 { set; get; }

        public static int GunFight_Assistant5 { set; get; }
        public static int Driving_Assistant5 { set; get; }
        public static int Farming_Assistant5 { set; get; }
        public static int Negotiation_Assistant5 { set; get; }

        public static int GunFight_Assistant6 { set; get; }
        public static int Driving_Assistant6 { set; get; }
        public static int Farming_Assistant6 { set; get; }
        public static int Negotiation_Assistant6 { set; get; }

    }
    class MenuUI : Script
    {
        public static class JobQueue
        {
            public static int PlayerXP { get; set; }
            public static string PlayerJobSelected { get; set; }
            public static string PlayerJobTypeSelected { get; set; }
            
        }


        public static Vehicle TemporaryVehicle;
        public static GTA.Blip ZoneBlip, holderblip;
        public static Prop holder;
        private MenuPool _menuPool; //Creating an Empty object of MainMenu Pool, which will hold all the options for this mod.

        public UIMenuItem Quantityitem, ActivateJob, CancleCurrentJob, //Jobs Menu Items
                          InformatioAboutMod, ResourceZone, DrivingZone, CheatMenu, //Options Menu Items
                          Priority, Seasonal_Help, Fire_Staff; //Staff Menu Items

        SimpleUI.MenuPool _menuPool2, _menuPool3;
        private SimpleUI.UIMenu mainMenu2, mainMenu3;

        float floatStoredistance1, floatStoredistance2, floatStoredistance3, floatStoredistance4, floatStoredistance5, floatStoredistance6, floatStoredistance7, floatStoredistance8, floatStoredistance9, floatStoredistance10, floatStoredistance11, floatStoredistance12, floatStoredistance13, floatStoredistance14, floatStoredistance15, floatStoredistance16, floatStoredistance17, floatStoredistance18, floatStoredistance19, floatStoredistance20;

        SimpleUI.UIMenuItem itemSelectFunction, itemSelectFunction2, itemSelectFunction3;
        SimpleUI.UIMenuNumberValueItem itemIntegerControl;
        SimpleUI.UIMenuNumberValueItem itemFloatControl;
        SimpleUI.UIMenuNumberValueItem itemEnumControl;


        SimpleUI.UIMenuItem VendoritemSelectFunction, VendoritemSelectFunction2, VendoritemSelectFunction3;
        SimpleUI.UIMenuNumberValueItem VendoritemIntegerControl;
        SimpleUI.UIMenuNumberValueItem VendoritemFloatControl;
        SimpleUI.UIMenuNumberValueItem VendoritemEnumControl, VendoritemEnumControl2;
        SimpleUI.UIMenuListItem vendorperlist;

        public UIMenuItem InventoryNumbers, PlayerXPAndMoney, Woodinfo, Stoneinfo, Steelinfo, Copperinfo, //RawResources
                          Wheatinfo, Riceinfo, BeetRootinfo, Weedinfo, //CropsResources
                          WheatSeedsinfo, RiceSeedsinfo, BeetRootSeedsinfo, WeedSeedsinfo,//SeedResources
                          CowMilkinfo, Cheeseinfo, PigMeatinfo, CowDunkinfo;//All the information Tab

        public static UIMenuCheckboxItem DisplayCompititorStat;
        NativeUI.UIMenu mainMenu, DrivingJobsMenu;

        public static bool ModStatusbool, modactive, LandPurchased, ResourcesZoneActive, modtourdisplay, Compititor_Stat, PlayerGoodLoadedfromInventory;
        public static List<string> CurrentJobPlayer;
        public static List<dynamic> Vendor1JobList;
               
        public static int testInt, vendornumber, JobCriticality = 1;
        public static int availableresources, selectedresources;

        List<dynamic> numberList = new List<dynamic>();

        string stringValue;

        float testFloat, tempdistance;

        int selectedquantity;

        Attacker_Class AttackerClassTempObject = new Attacker_Class();

        enum AvailableJobList
        {
            ChoppedWood = 1,
            Stone = 2,
            Steel = 3,
            Copper = 4,
            Wheat = 5,
            Rice = 6,
            BeetRoot = 7,
            Weed = 8,
            Milk = 9,
            Cheese = 10,
            PigMeat = 11,
            //CowDunk = 12
        }
        AvailableJobList testEnum = AvailableJobList.ChoppedWood;

        enum AvailableVendors
        {
            Vendor1 = 1,
            Vendor2 = 2,
            Vendor3 = 3,
        }
        AvailableVendors VendorsEnum = AvailableVendors.Vendor1;
        
        private bool UpdateInventory;

        List<dynamic> InventoryList;

        UIMenuListItem InventoryListSection;

        Vector3 playerorignalposition;

        string nameuser;

        //private string []CurrentJobPlayer;
        //Default Constructor
        public MenuUI()
        {
            _menuPool = new MenuPool();

            mainMenu = new UIMenu("~y~Far ~g~Rim", "Mod Created by : ~g~Ashish R. aka ~r~YCSM!!!");
            //DrivingJobsMenu = new UIMenu("~y~Driving ~g~Jobs", "Select one Driving job from the available List"); //This shows the Avaible Goods in your inventory to deliver to market
            DrivingJobsMenu = new UIMenu("~y~Driving ~g~Jobs", "Avaible Goods, ready to be delivered"); //This shows the Avaible Goods in your inventory to deliver to market
            UIMenuCheckboxItem ModStatus = new UIMenuCheckboxItem("Mod Active", ModStatusbool, "checks the status of the mod");
            //DisplayCompititorStat = new UIMenuCheckboxItem("Display Compititor Stat", Compititor_Stat, "Check to Display Compititor Stat");
            InventoryNumbers = new UIMenuItem("In Stock : ");
            _menuPool.Add(mainMenu);
            _menuPool.Add(DrivingJobsMenu);


            mainMenu.AddItem(ModStatus);
            //mainMenu.AddItem(DisplayCompititorStat);

            Menu1(mainMenu);
            Menu2(mainMenu);
            Menu3(mainMenu);
            DrivingGoodsMenu(mainMenu);
            Nearest_Shop(mainMenu);
            OptionMenu(mainMenu);

            

            Vendor1JobList = new List<dynamic>
            {
                "WoodLogsDeliveryJob",
                "StoneDeliveryJob",
                "DumpDeliveryJob",
            };
            DrivingMenu1(DrivingJobsMenu);

            int gametime = Game.GameTime;
            
            _menuPool.RefreshIndex();//This will refresh all the menus and submenus to reflect it's content correctly            
            CurrentJobPlayer = new List<string>();            
            InitMenu();

            nameuser = Function.Call<string>(Hash._SC_GET_NICKNAME);

            //DisplayCompititorStat.Checked = true;           
            //InitMenu2();
            KeyUp += OnKeyUp;

            Tick += (o, e) =>
            {
                _menuPool.ProcessMenus();
                _menuPool2.ProcessMenus();
                if (_menuPool3 != null)
                {
                    _menuPool3.ProcessMenus();
                }

                
                
                if (ModStatus.Checked && !modactive)
                {
                    if (nameuser != "UKNOWN")
                    {
                        modactive = true;
                        ModStatusbool = true;
                        UI.ShowSubtitle("Mod Activated");
                    }
                    else
                    {
                        UI.Notify("This isn't a valid version of the game");
                        UI.ShowSubtitle("Possibly, it's a cracked version");
                        if (ModStatus.Checked)
                        {
                            ModStatus.Checked = false;
                        }
                    }
                    
                }
                else if (!ModStatus.Checked && modactive)
                {
                    modactive = false;
                    ModStatusbool = false;
                    UI.ShowSubtitle("Mod Deativated");
                }


                if (modactive)
                {
                    if (mainMenu.Visible)
                    {
                        Competitors_Class.MenuActive = true;
                    }
                    else
                    {
                        Competitors_Class.MenuActive = false;
                    }

                    if (modtourdisplay)
                    {

                        playerorignalposition = GTA.Game.Player.Character.Position;
                        if (Function.Call<bool>(Hash.IS_SCREEN_FADED_OUT))
                        {
                            Function.Call(Hash.DISPLAY_RADAR, false);
                            Game.DisableAllControlsThisFrame(2);                           
                            GTA.Camera schoolcamera = World.CreateCamera(new Vector3(-1962.336f, -1348.624f, 05.129f), new Vector3(0f, 0f, 80f), 75f);                            
                            schoolcamera.Position = new Vector3(schoolcamera.Position.X - 20f, schoolcamera.Position.Y + 20, schoolcamera.Position.Z + 02f);                            
                            World.RenderingCamera = schoolcamera;                            
                            GTA.Native.Function.Call(Hash.DO_SCREEN_FADE_IN, 1000);
                            Function.Call(Hash.DISPLAY_RADAR, false);
                            Class1.DisplayHelpTextThisFrame("Welcome to FaRim Mod by Ashish Rathi");
                            Wait(6000);
                            Class1.DisplayHelpTextThisFrame("This mod is intended to give you a gameplay experience of resource management game style.");
                            Wait(8000);
                            Class1.DisplayHelpTextThisFrame("You play as a start-up farmer, and your aim is to be the dominating farmer of Los Santos.");
                            Wait(8000);
                            Class1.DisplayHelpTextThisFrame("You start with nothing. Money and XP can be earned by performing various jobs.");
                            Wait(8000);
                            Class1.DisplayHelpTextThisFrame("Jobs such as Delivering & gathering Goods, materials by assigning a job from menu.");
                            Wait(8000);
                            Class1.DisplayHelpTextThisFrame("Once enough XP earned, new job types will be unlocked.");
                            Wait(6000);
                            Vector3 woodposition = new Vector3(-348.5256f, 5959.203f, 43.25525f);
                            Function.Call(Hash.DISPLAY_RADAR, false);
                            //GTA.Game.Player.Character.Position = new Vector3(woodposition.X + 20f, woodposition.Y, woodposition.Z); //GTA.Game.Player.Character.Position = new Vector3(-1576.859f, 4527.854f, 17.702f);
                            GTA.Game.Player.Character.Position = new Vector3(woodposition.X + 20f, woodposition.Y - 20f, woodposition.Z); //GTA.Game.Player.Character.Position = new Vector3(-1576.859f, 4527.854f, 17.702f);
                            Wait(1000);
                            World.DestroyAllCameras();
                            GTA.Camera schoolcamera2 = World.CreateCamera(woodposition, new Vector3(-30f, 0f, 150f), 75f);
                            //schoolcamera2.Position = new Vector3(schoolcamera2.Position.X + 10f, schoolcamera2.Position.Y + 10f, schoolcamera2.Position.Z + 05f);
                            schoolcamera2.Position = new Vector3(schoolcamera2.Position.X, schoolcamera2.Position.Y + 10f, schoolcamera2.Position.Z + 05f);
                            World.RenderingCamera = schoolcamera2;
                            Class1.DisplayHelpTextThisFrame("These resources can be gathered by visiting diffrent locations such as this one.");
                            Wait(8000);
                            Class1.DisplayHelpTextThisFrame("Completion of tasks will gain XP and money, which can be used to buy the tools & Lands.");
                            Wait(6000);
                            Vector3 farmpositioncam = new Vector3(284.8731f, 6459.515f, 31.23755f);//(319.5343f, 6583.507f, 29.17677f);
                            GTA.Game.Player.Character.Position = farmpositioncam;
                            Function.Call(Hash.DISPLAY_RADAR, false);
                            Wait(1000);
                            Class1.DisplayHelpTextThisFrame("Buying the lands will enable player to Farm(harvest, crop).");
                            World.DestroyAllCameras();
                            GTA.Camera schoolcamera3 = World.CreateCamera(farmpositioncam, new Vector3(-30f, 0f, 80f), 75f);
                            schoolcamera3.Position = new Vector3(schoolcamera3.Position.X - 50, schoolcamera3.Position.Y + 05, schoolcamera3.Position.Z + 05);
                            World.RenderingCamera = schoolcamera3;
                            Wait(8000);
                            Class1.DisplayHelpTextThisFrame("or advanced jobs like cattling(Cow Milk) for better XP & Money generation.");
                            Wait(8000);
                            //World.DestroyAllCameras();
                            //GTA.Camera schoolcamera4 = World.CreateCamera(farmpositioncam, new Vector3(-30f, 0f, 80f), 75f);
                            //schoolcamera4.Position = new Vector3(schoolcamera4.Position.X - 90, schoolcamera4.Position.Y + 10, schoolcamera4.Position.Z + 10);
                            //World.RenderingCamera = schoolcamera4;
                            Class1.DisplayHelpTextThisFrame("AI driven assistants will be unlocked eventually. Which will help you automate the jobs.");
                            Wait(8000);
                            Class1.DisplayHelpTextThisFrame("As other AI compititors will try to push you down, either attacking your goods and farms.");
                            Wait(8000);
                            Class1.DisplayHelpTextThisFrame("Your ultimatum goal is to be the best farmer in Los Santos.");
                            Wait(4000);
                            Class1.DisplayHelpTextThisFrame("This mod is fully customizable and various options can be added via options menu");
                            Wait(6000);
                            Class1.DisplayHelpTextThisFrame("Progress will be auto-saved in an XML file and same will be loaded the next time");
                            Wait(6000);
                            Class1.DisplayHelpTextThisFrame("Be the protector and the leader of your own Farming Journey...!!!");                            
                            Wait(5000);
                            GTA.Game.Player.Character.Position = playerorignalposition;
                            Wait(1000);
                            Game.EnableAllControlsThisFrame(2);                            
                            World.DestroyAllCameras();
                            World.RenderingCamera = null;                            
                            Class1.DisplayHelpTextThisFrame("Hope you will enjoy this mod, Goodluck. (:");
                            UI.Notify("You can start by getting a job from available jobs menu.");
                            modtourdisplay = false;
                            Function.Call(Hash.DISPLAY_RADAR, true);
                        }
                    }
                    if (JobQueue.PlayerJobSelected != null)
                    {
                        if (ResourcesZoneActive)
                        {
                            if (ZoneBlip != null)
                            {
                                tempdistance = World.GetDistance(Game.Player.Character.Position, ZoneBlip.Position);
                            }
                            if (tempdistance <= 100f)
                            {
                                if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                                {
                                    Function.Call(Hash.SET_WAYPOINT_OFF);
                                }

                                if (JobQueue.PlayerJobSelected == "ChopWood")
                                {
                                    holder = World.CreateProp(-1381557071, ZoneBlip.Position.Around(10f), true, true);
                                    holder.Health = 100;
                                }
                                else if (JobQueue.PlayerJobSelected == "StoneMining")
                                {
                                    holder = World.CreateProp(-1332461625, ZoneBlip.Position.Around(10f), true, true); //1797043157
                                    holder.Health = 100;
                                }
                                else if (JobQueue.PlayerJobSelected == "SteelMining")
                                {
                                    holder = World.CreateProp(-1053433850, ZoneBlip.Position.Around(10f), true, true); //-1215378248
                                    holder.Health = 100;
                                }
                                else if (JobQueue.PlayerJobSelected == "CopperMining")
                                {
                                    //holder = World.CreateProp(17064270, ZoneBlip.Position.Around(10f), true, true); //976638897
                                    holder = World.CreateProp(new Model("prop_conc_sacks_02a"), ZoneBlip.Position.Around(10f), true, true); //976638897
                                    holder.Health = 100;
                                }
                                ZoneBlip.Remove();
                                ZoneBlip = null;
                                ResourcesZoneActive = false;
                            }
                        }

                        if (holder != null)
                        {
                            if (holder.Exists())
                            {
                                World.DrawMarker(MarkerType.UpsideDownCone, new Vector3(holder.Position.X, holder.Position.Y, holder.Position.Z + 5.0f), new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f), new Vector3(1.0f, 1.0f, 1.0f), System.Drawing.Color.Blue, true, false, 0, false, "", "", false);
                                if (holderblip == null)
                                {
                                    holderblip = GTA.World.CreateBlip(holder.Position);
                                    holderblip.Color = BlipColor.Yellow;
                                    holderblip.Scale = 0.7f;
                                    holderblip.Name = MenuUI.JobQueue.PlayerJobSelected.ToString();
                                }
                            }
                            else
                            {

                            }
                        }
                    }

                    if (DrivingJobsMenu.Visible)
                    {
                        #region raw_resources
                        InventoryList.Add("Chooped Wood : " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString());
                        InventoryList.Add("Stone : " + AvailableResources.ResourcesNumbers.StoneQuantity.ToString());
                        InventoryList.Add("Steel : " + AvailableResources.ResourcesNumbers.RawSteelQuantity.ToString());
                        InventoryList.Add("Copper : " + AvailableResources.ResourcesNumbers.CopperQuantity.ToString());
                        #endregion

                        #region crop_resources
                        InventoryList.Add("Wheat : " + AvailableResources.ResourcesNumbers.WheatQuantity.ToString());
                        InventoryList.Add("Rice : " + AvailableResources.ResourcesNumbers.RiceQuantity.ToString());
                        InventoryList.Add("BeetRoot : " + AvailableResources.ResourcesNumbers.BeetRootQuantity.ToString());
                        InventoryList.Add("Weed : " + AvailableResources.ResourcesNumbers.WeedQuantity.ToString());
                        #endregion

                        #region cattle_products
                        InventoryList.Add("Milk : " + AvailableResources.ResourcesNumbers.CowMilkQuantity.ToString());
                        InventoryList.Add("Cheese : " + AvailableResources.ResourcesNumbers.CheeseQuantity.ToString());
                        InventoryList.Add("Meat : " + AvailableResources.ResourcesNumbers.PigMeatQuantity.ToString());
                        InventoryList.Add("CowDunk : " + AvailableResources.ResourcesNumbers.CowDunkQuantity.ToString());
                        #endregion


                        if (InventoryListSection.Index == 0)
                        //if (DrivingJobsMenu.CurrentSelection == 0)
                        {
                            int indexx = InventoryListSection.Index;
                            UI.ShowSubtitle(indexx.ToString());
                            InventoryNumbers.Text = "In Stock : " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString();
                            DrivingJobsMenu.RefreshIndex();

                            //UI.ShowSubtitle("In Stock : " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString());
                        }
                        if (InventoryListSection.Index == 1)
                        {
                            DrivingJobsMenu.RefreshIndex();
                            InventoryNumbers.Text = "In Stock : " + AvailableResources.ResourcesNumbers.StoneQuantity.ToString();
                            //UI.ShowSubtitle("In Stock : " + AvailableResources.ResourcesNumbers.StoneQuantity.ToString());
                            int indexx = InventoryListSection.Index;
                            UI.ShowSubtitle(indexx.ToString());
                        }
                    }
                }

            }; //Tick event ends here

            KeyDown += (o, e) =>
            {
                if (e.KeyCode == Class1.ShowMenuKey && !_menuPool.IsAnyMenuOpen())
                {
                    mainMenu.Visible = !mainMenu.Visible;
                }

            };



        }//Default Constructor Ends here

        #region InitMenu1
        void InitMenu()
        {
            // First initialize an instance of a SimpleUI.MenuPool.
            // A SimpleUI.MenuPool object will manage all the interconnected
            // menus that you add to it.
            _menuPool2 = new SimpleUI.MenuPool();

            // Initialize a menu, with name "Main Menu"
            mainMenu2 = new SimpleUI.UIMenu("~y~Driving ~g~Jobs");
            // Add mainMenu2 to _menuPool2
            _menuPool2.AddMenu(mainMenu2);



            itemEnumControl = new SimpleUI.UIMenuNumberValueItem("Available Resources : ", testEnum, "Select the Goods type to deliver to market");
            mainMenu2.AddMenuItem(itemEnumControl);

            itemSelectFunction = new SimpleUI.UIMenuItem("Available Stock", null, "Available Stock with you");
            mainMenu2.AddMenuItem(itemSelectFunction);

            itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString();            

            itemIntegerControl = new SimpleUI.UIMenuNumberValueItem("Load to the Truck", testInt, "Resources, do you wish to sell/exchange to market");
            mainMenu2.AddMenuItem(itemIntegerControl);

            itemSelectFunction2 = new SimpleUI.UIMenuItem("Done", null, "Select this to Load the Goods in the Truck");
            mainMenu2.AddMenuItem(itemSelectFunction2);

            itemSelectFunction3 = new SimpleUI.UIMenuItem("Back", null, "Go back to the previous Menu");
            //mainMenu2.AddMenuItem(itemSelectFunction3);


            mainMenu2.OnItemLeftRight += MainMenu_OnItemLeftRight;
            mainMenu2.OnItemSelect += MainMenu_OnItemSelect;
        }
             

        private void MainMenu_OnItemLeftRight(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index, bool left)
        {
            if (selectedItem == itemEnumControl)
            {
                mainMenu2.ControlEnumValue(ref testEnum, itemEnumControl, left);

                if (testEnum.ToString() == AvailableJobList.ChoppedWood.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.ChoppedWoodQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.Stone.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.StoneQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.StoneQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.Steel.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.RawSteelQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.RawSteelQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.Copper.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.CopperQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.CopperQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.Wheat.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.WheatQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.WheatQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.Rice.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.RiceQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.RiceQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.BeetRoot.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.BeetRootQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.BeetRootQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.Weed.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.WeedQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.WeedQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.Milk.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.CowMilkQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.CowMilkQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.Cheese.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.CheeseQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.CheeseQuantity;
                }
                else if (testEnum.ToString() == AvailableJobList.PigMeat.ToString())
                {
                    itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.PigMeatQuantity.ToString();
                    availableresources = (int)AvailableResources.ResourcesNumbers.PigMeatQuantity;
                }
            }
            else if (selectedItem == itemIntegerControl)
            {
                mainMenu2.ControlIntValue(ref testInt, itemIntegerControl, left, 1, 20, true, 0, availableresources);

                if (testInt <= availableresources)
                {
                    selectedresources = testInt;
                    Delivery_Jobs.GoodsLoaded = selectedresources;                    
                }
            }
        }



        private void MainMenu_OnItemSelect(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index)
        {
            int minimumlimit = (int)((double)availableresources / 100 * 10);

            if (selectedItem == itemSelectFunction2)
            {
                if (selectedresources <= availableresources)
                {
                    if (selectedresources == 0)
                    {
                        UI.ShowSubtitle("Unable to load the truck with Zero Quantity");
                    }
                    else
                    {
                        if (selectedresources < minimumlimit)
                        {
                            UI.ShowSubtitle("Minimum " + (minimumlimit).ToString() + "(10% of available) units are needed to load the cart");
                        }
                        else
                        {
                            PlayerGoodLoadedfromInventory = true;
                            if (testEnum.ToString() == AvailableJobList.ChoppedWood.ToString())
                            {
                                AvailableResources.ResourcesNumbers.ChoppedWoodQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString();

                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();

                                foreach (var item in AllPositionsListed.WoodDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.WoodDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());                                         
                                        }
                                    }                                    
                                }

                                Delivery_Jobs.WoodJob();                                
                            }
                            else if (testEnum.ToString() == AvailableJobList.Stone.ToString())
                            {
                                AvailableResources.ResourcesNumbers.StoneQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.StoneQuantity.ToString();

                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();                                
                                foreach (var item in AllPositionsListed.StoneDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.StoneDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }                                    
                                }

                                Delivery_Jobs.StoneJob();                                
                            }
                            else if (testEnum.ToString() == AvailableJobList.Steel.ToString())
                            {
                                AvailableResources.ResourcesNumbers.RawSteelQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.RawSteelQuantity.ToString();

                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();

                                foreach (var item in AllPositionsListed.SteelDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.SteelDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }                                    
                                }

                                Delivery_Jobs.SteelJob();                                
                            }
                            else if (testEnum.ToString() == AvailableJobList.Copper.ToString())
                            {
                                AvailableResources.ResourcesNumbers.CopperQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.CopperQuantity.ToString();

                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();
                                
                                foreach (var item in AllPositionsListed.CopperDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.CopperDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }
                                    
                                }
                                Delivery_Jobs.CopperJob();                                
                            }
                            else if (testEnum.ToString() == AvailableJobList.Wheat.ToString())
                            {
                                AvailableResources.ResourcesNumbers.WheatQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.WheatQuantity.ToString();
                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();
                                foreach (var item in AllPositionsListed.WheatCropsDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.WheatCropsDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());

                                        }
                                    }                                    
                                }
                                Delivery_Jobs.WheatJob();
                            }
                            else if (testEnum.ToString() == AvailableJobList.Rice.ToString())
                            {
                                AvailableResources.ResourcesNumbers.RiceQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.RiceQuantity.ToString();
                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();
                                foreach (var item in AllPositionsListed.RiceCropsDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.RiceCropsDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }
                                    
                                }
                                Delivery_Jobs.RiceJob();
                            }
                            else if (testEnum.ToString() == AvailableJobList.BeetRoot.ToString())
                            {
                                AvailableResources.ResourcesNumbers.BeetRootQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.BeetRootQuantity.ToString();
                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();
                                foreach (var item in AllPositionsListed.BeetRootCropsDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.BeetRootCropsDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }
                                    
                                }
                                Delivery_Jobs.BeetRootJob();
                            }
                            else if (testEnum.ToString() == AvailableJobList.Weed.ToString())
                            {
                                AvailableResources.ResourcesNumbers.WeedQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.WeedQuantity.ToString();
                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();
                                foreach (var item in AllPositionsListed.WeedCropsDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.WeedCropsDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }
                                    
                                }
                                Delivery_Jobs.WeedJob();                                
                            }
                            else if (testEnum.ToString() == AvailableJobList.Milk.ToString())
                            {
                                AvailableResources.ResourcesNumbers.CowMilkQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.CowMilkQuantity.ToString();
                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();
                                foreach (var item in AllPositionsListed.MilkDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.MilkDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }                                    
                                }
                                Delivery_Jobs.CowMilkJob();                                
                            }
                            else if (testEnum.ToString() == AvailableJobList.Cheese.ToString())
                            {
                                AvailableResources.ResourcesNumbers.CheeseQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.CheeseQuantity.ToString();
                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();
                                foreach (var item in AllPositionsListed.CheeseDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.CheeseDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }
                                    
                                }
                                Delivery_Jobs.CheeseJob();
                            }
                            else if (testEnum.ToString() == AvailableJobList.PigMeat.ToString())
                            {
                                AvailableResources.ResourcesNumbers.PigMeatQuantity -= selectedresources;
                                itemSelectFunction.Text = "In Stock : " + AvailableResources.ResourcesNumbers.PigMeatQuantity.ToString();
                                Delivery_Jobs.DrivingLocationsList.Clear();
                                Delivery_Jobs.DrivingLocationsNameList.Clear();
                                foreach (var item in AllPositionsListed.PorkDrivingZonePositionList)
                                {
                                    if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                                    {
                                        Delivery_Jobs.DrivingLocationsList.Add(item);
                                        for (int i = 0; i < AllPositionsListed.PorkDrivingZonePositionList.Count; i++)
                                        {
                                            Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                        }
                                    }
                                }
                                Delivery_Jobs.PorkJob();
                            }
                            availableresources -= selectedresources;                            
                            selectedresources = 0;
                            testInt = 0;
                            itemIntegerControl.Value = "< " + testInt + " >";                            
                            _menuPool2.CloseAllMenus();


                        }

                    }
                    if (Delivery_Jobs.DrivingLocationsList.Count <= 0)
                    {
                        List<Vector3> tempdrivinglocationsallinall = new List<Vector3>
                    {
                        new Vector3(-682.0571f, 5847.38f, 16.64597f),
                        new Vector3(2080.356f, 2341.643f, 93.95109f),
                        new Vector3(1061.937f, -2446.664f, 28.73324f),
                        new Vector3(1880.001f, -1040.597f, 78.88943f),
                        new Vector3(1707.123f, -1482.879f, 112.6227f),
                        new Vector3(-746.2863f, 5543.07f, 32.96365f),
                        new Vector3(-436.8799f, 6205.587f, 28.76165f),                        
                        new Vector3(2672.756f, 3514.619f, 52.21802f),
                        //new Vector3(-682.0571f, 5847.38f, 16.64597f),
                        //new Vector3(2080.356f, 2341.643f, 93.95109f),
                        //new Vector3(1061.937f, -2446.664f, 28.73324f),
                        //new Vector3(1880.001f, -1040.597f, 78.88943f),
                        //new Vector3(1707.123f, -1482.879f, 112.6227f),
                        //new Vector3(-746.2863f, 5543.07f, 32.96365f),
                        //new Vector3(-436.8799f, 6205.587f, 28.76165f),
                        //new Vector3(1803.432f, -1344.814f, 98.06102f),
                        //new Vector3(2370.615f, 2529.328f, 46.39578f),
                        //new Vector3(2271.584f, 5153.612f, 56.27338f),
                        //new Vector3(2672.756f, 3514.619f, 52.21802f),
                    };

                        foreach (var item in tempdrivinglocationsallinall)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < tempdrivinglocationsallinall.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }
                        }
                        UI.Notify("No Driving Locations Found, Adding default ones");
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(-682.0571f, 5847.38f, 16.64597f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(2080.356f, 2341.643f, 93.95109f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(1061.937f, -2446.664f, 28.73324f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(1880.001f, -1040.597f, 78.88943f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(1707.123f, -1482.879f, 112.6227f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(-746.2863f, 5543.07f, 32.96365f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(-436.8799f, 6205.587f, 28.76165f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(1803.432f, -1344.814f, 98.06102f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(2370.615f, 2529.328f, 46.39578f));
                        //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(2271.584f, 5153.612f, 56.27338f));
                    }

                }
                else
                {
                    UI.ShowSubtitle("Invalid Quantity Selected.! You can only sell the quantity you have. Lower the Quantity");
                }              
            }

            if (selectedItem == itemSelectFunction3)
            {
                MainNAtiveUIMenu();
            }
        }

        #endregion

        #region InitMenu2

        void InitMenu2()
        {
            // First initialize an instance of a SimpleUI.MenuPool.
            // A SimpleUI.MenuPool object will manage all the interconnected
            // menus that you add to it.            
            _menuPool3 = new SimpleUI.MenuPool();

            // Initialize a menu, with name "Main Menu"
            mainMenu3 = new SimpleUI.UIMenu("~y~Driving ~g~Jobs");
            // Add mainMenu2 to _menuPool2
            _menuPool3.AddMenu(mainMenu3);

            VendoritemEnumControl = new SimpleUI.UIMenuNumberValueItem("Available Vendors : ", VendorsEnum, "Select the Appropriate Vendor to Deliver the goods from");
            mainMenu3.AddMenuItem(VendoritemEnumControl);

            vendorperlist = new SimpleUI.UIMenuListItem("Vendor Per List", "VendorPerList", Vendor1JobList);
            mainMenu3.AddMenuItem(vendorperlist);            

            VendoritemSelectFunction2 = new SimpleUI.UIMenuItem("Done", null, "Select this to Load the Goods in the Truck");
            mainMenu3.AddMenuItem(VendoritemSelectFunction2);

            mainMenu3.OnItemLeftRight += MainMenu3_OnItemLeftRight;
            mainMenu3.OnItemSelect += MainMenu3_OnItemSelect;
        }

        private void MainMenu3_OnItemLeftRight(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index, bool left)
        {
            if (selectedItem == VendoritemEnumControl)
            {
                mainMenu3.ControlEnumValue(ref VendorsEnum, VendoritemEnumControl, left);


                if (VendorsEnum.ToString() == "Vendor1")
                {
                    mainMenu3.DisableItem(vendorperlist);
                    mainMenu3.DisableItem(VendoritemSelectFunction2);
                    Vendor1JobList.Clear();
                    Vendor1JobList.Add("WoodLogs Delivery");
                    Vendor1JobList.Add("Stone Delivery");
                    Vendor1JobList.Add("Steel Delivery");
                    
                    vendorperlist = new SimpleUI.UIMenuListItem("Vendor Per List", "VendorPerList", Vendor1JobList);
                    mainMenu3.AddMenuItem(vendorperlist);
                    mainMenu3.AddMenuItem(VendoritemSelectFunction2);
                    vendornumber = 1;
                                       
                    
                }
                else if (VendorsEnum.ToString() == "Vendor2")
                {
                    mainMenu3.DisableItem(vendorperlist);
                    mainMenu3.DisableItem(VendoritemSelectFunction2);
                    Vendor1JobList.Clear();
                    Vendor1JobList.Add("Milk Delivery");
                    Vendor1JobList.Add("Cheese Delivery");
                    Vendor1JobList.Add("Medicine Delivery");
                    vendorperlist = new SimpleUI.UIMenuListItem("Vendor Per List", "VendorPerList", Vendor1JobList);
                    mainMenu3.AddMenuItem(vendorperlist);
                    mainMenu3.AddMenuItem(VendoritemSelectFunction2);
                    vendornumber = 2;

                }
                else if (VendorsEnum.ToString() == "Vendor3")
                {
                    mainMenu3.DisableItem(vendorperlist);
                    mainMenu3.DisableItem(VendoritemSelectFunction2);
                    Vendor1JobList.Clear();
                    Vendor1JobList.Add("Burger Delivery");
                    Vendor1JobList.Add("Crude Oil Delivery");
                    Vendor1JobList.Add("Antiques Delivery");
                    vendorperlist = new SimpleUI.UIMenuListItem("Vendor Per List", "VendorPerList", Vendor1JobList);
                    mainMenu3.AddMenuItem(vendorperlist);
                    mainMenu3.AddMenuItem(VendoritemSelectFunction2);
                    vendornumber = 3;
                }
            }
        }



        private void MainMenu3_OnItemSelect(SimpleUI.UIMenu sender, SimpleUI.UIMenuItem selectedItem, int index)
        {
            if (selectedItem == VendoritemSelectFunction2)
            {
                if (VendorsEnum.ToString() == "Vendor1")
                {
                    if (vendorperlist.CurrentListItem.ToString() == "WoodLogs Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();
                        
                        foreach (var item in AllPositionsListed.WoodDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.WoodDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }                            
                        }
                        Delivery_Jobs.VendorWoodJob();
                    }else if (vendorperlist.CurrentListItem.ToString() == "Stone Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();

                        foreach (var item in AllPositionsListed.StoneDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.StoneDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }                            
                        }                        
                        Delivery_Jobs.VendorStoneJob();
                    }
                    else if (vendorperlist.CurrentListItem.ToString() == "Steel Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();

                        foreach (var item in AllPositionsListed.SteelDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.SteelDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }                            
                        }
                        Delivery_Jobs.VendorSteelJob();
                    }

                    UI.Notify(vendorperlist.CurrentListItem.ToString() + " Job Selected from " + VendorsEnum.ToString());
                    
                    //Delivery_Jobs.VendorWoodJob();
                }
                else if (VendorsEnum.ToString() == "Vendor2")
                {
                    if (vendorperlist.CurrentListItem.ToString() == "Milk Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();

                        
                        foreach (var item in AllPositionsListed.MilkDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.MilkDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }
                            
                        }
                        Delivery_Jobs.VendorMilkDeliveryJob();
                    }else if (vendorperlist.CurrentListItem.ToString() == "Cheese Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();

                        foreach (var item in AllPositionsListed.CheeseDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.CheeseDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }                            
                        }
                        Delivery_Jobs.VendorCheeseDeliveryJob();
                    }
                    else if (vendorperlist.CurrentListItem.ToString() == "Medicine Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();

                        foreach (var item in AllPositionsListed.WeedCropsDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.WeedCropsDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }                            
                        }
                        Delivery_Jobs.VendorMedicineDeliveryJob();
                    }
                    UI.Notify(vendorperlist.CurrentListItem.ToString());
                    UI.Notify(VendorsEnum.ToString());
                }
                else if (VendorsEnum.ToString() == "Vendor3")
                {
                    if (vendorperlist.CurrentListItem.ToString() == "Burger Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();

                        foreach (var item in AllPositionsListed.PorkDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.PorkDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }
                            
                        }
                        Delivery_Jobs.VendorBurgerDeliveryJob();
                    }
                    else if (vendorperlist.CurrentListItem.ToString() == "Crude Oil Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();

                        foreach (var item in AllPositionsListed.CopperDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.CopperDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }
                            
                        }
                        Delivery_Jobs.VendorCrudeOilDeliveryJob();
                    }
                    else if (vendorperlist.CurrentListItem.ToString() == "Antiques Delivery")
                    {
                        Delivery_Jobs.DrivingLocationsList.Clear();
                        Delivery_Jobs.DrivingLocationsNameList.Clear();

                        foreach (var item in AllPositionsListed.RiceCropsDrivingZonePositionList)
                        {
                            if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                            {
                                Delivery_Jobs.DrivingLocationsList.Add(item);
                                for (int i = 0; i < AllPositionsListed.RiceCropsDrivingZonePositionList.Count; i++)
                                {
                                    Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                                }
                            }                            
                        }
                        Delivery_Jobs.VendorAntiquesDeliveryJob();
                    }
                    UI.Notify(vendorperlist.CurrentListItem.ToString());
                    UI.Notify(VendorsEnum.ToString());
                }


                
                if (Delivery_Jobs.DrivingLocationsList.Count <= 0)
                {
                    List<Vector3> tempdrivinglocationsallinall = new List<Vector3>
                    {
                        new Vector3(-682.0571f, 5847.38f, 16.64597f),
                        new Vector3(2080.356f, 2341.643f, 93.95109f),
                        new Vector3(1061.937f, -2446.664f, 28.73324f),
                        new Vector3(1880.001f, -1040.597f, 78.88943f),
                        new Vector3(1707.123f, -1482.879f, 112.6227f),
                        new Vector3(-746.2863f, 5543.07f, 32.96365f),
                        new Vector3(-436.8799f, 6205.587f, 28.76165f),
                        new Vector3(1803.432f, -1344.814f, 98.06102f),
                        new Vector3(2370.615f, 2529.328f, 46.39578f),
                        new Vector3(2271.584f, 5153.612f, 56.27338f),
                    };

                    foreach (var item in tempdrivinglocationsallinall)
                    {
                        if (Delivery_Jobs.DrivingLocationsList.Count < 10)
                        {
                            Delivery_Jobs.DrivingLocationsList.Add(item);
                            for (int i = 0; i < tempdrivinglocationsallinall.Count; i++)
                            {
                                Delivery_Jobs.DrivingLocationsNameList.Add("Location " + (i + 1).ToString());
                            }
                        }
                    }
                    UI.Notify("No Driving Locations Found, Adding default ones");
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(-682.0571f, 5847.38f, 16.64597f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(2080.356f, 2341.643f, 93.95109f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(1061.937f, -2446.664f, 28.73324f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(1880.001f, -1040.597f, 78.88943f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(1707.123f, -1482.879f, 112.6227f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(-746.2863f, 5543.07f, 32.96365f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(-436.8799f, 6205.587f, 28.76165f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(1803.432f, -1344.814f, 98.06102f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(2370.615f, 2529.328f, 46.39578f));
                    //Delivery_Jobs.DrivingLocationsList.Add(new Vector3(2271.584f, 5153.612f, 56.27338f));
                }


                Attacker_Class.Trucking_Mission = true;

                _menuPool3.CloseAllMenus();
                _menuPool3.RemoveAllMenus();
                _menuPool3 = null;





            }            
        }

        #endregion

        void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (Class1.DeveloperMode)
            {
                if (e.KeyCode == Keys.K)
                {
                    
                    //int minimumlimit = (int)((double)selectedresources / 100 * 20);
                    //UI.ShowSubtitle(minimumlimit.ToString());
                }

                if (e.KeyCode == Keys.I)
                {
                    //AvailableResources.ResourcesNumbers.ChoppedWoodQuantity += 14f;
                    //AvailableResources.ResourcesNumbers.CowMilkQuantity += 10f;
                    //AvailableResources.ResourcesNumbers.BeetRootQuantity += 54f;
                    //AvailableResources.ResourcesNumbers.RiceQuantity += 87f;

                }

                if (e.KeyCode == Keys.O)
                {


                }
            }
        }


        public void ShowMenuonScreen()
        {
            if (!mainMenu.Visible)
            {
                mainMenu.Visible = true;
            }
        }

        private enum JobQueues
        {
            #region RawResourceJobQueue
            ChopWood,
            StoneMining,
            SteelMining,
            CopperMining,
            #endregion

            #region Harvesting&SowingJobQueue
            Cultivate_Wheat,
            Cultivate_Rice,
            Cultivate_BeetRoot,
            Cultivate_Weed,
            #endregion

            #region CattleJobQueue
            GatherMilk,
            MakeCheese,
            PigMeat,
            CowDunk,
            #endregion


        };

        public enum JobTypes
        {

            ChoppingJob,
            MiningJob,
            HarvestingJob,
            CattlingJob,
            DrivingJob,

        }

        #region Main_Menu_Options
        public void Menu1(UIMenu menu)
        {
            List<dynamic> listitems = new List<dynamic>()
            {
                //Raw Resources Job Queue
                JobQueues.ChopWood,
                JobQueues.StoneMining,
                JobQueues.SteelMining,
                JobQueues.CopperMining,
                
                
                //Harvesting Job Queue                
                JobQueues.Cultivate_Wheat,
                JobQueues.Cultivate_Rice,
                JobQueues.Cultivate_BeetRoot,
                JobQueues.Cultivate_Weed,

                ////Crops Job Queue
                //JobQueues.CropWheat,
                //JobQueues.CropRice,
                //JobQueues.CropBeetRoot,
                //JobQueues.CropWeed,

                //Cattling Job Queue
                JobQueues.GatherMilk,
                JobQueues.MakeCheese,
                JobQueues.PigMeat,
                JobQueues.CowDunk,

            };

            UIMenuListItem listsection = new UIMenuListItem("Available Jobs : ", listitems, 0);

            UIMenu submenu1 = _menuPool.AddSubMenu(menu, "Available Jobs");

            for (int i = 0; i < 1; i++)
            {
                submenu1.AddItem(listsection);
            }
            numberList.Add(0);
            
            //Quantityitem.Enabled = false;

            submenu1.OnListChange += (sender, item, index) =>
            {
                if (item == listsection)
                {
                    if (item.Index >= 4 && item.Index <= 7)
                    {
                        
                        if (item.Index == 4)
                        {
                            numberList.Clear();
                            int limit = int.Parse(AvailableResources.ResourcesNumbers.WheatSeedQuantity.ToString());
                            var numberList1 = Enumerable.Range(1, limit).ToList();
                            foreach (var itemm in numberList1)
                            {
                                numberList.Add(itemm);
                            }
                            if (numberList.Count <= 0)
                            {
                                numberList.Add(0);
                            }
                            UI.ShowSubtitle("Wheat : " + int.Parse(AvailableResources.ResourcesNumbers.WheatSeedQuantity.ToString()));
                            Quantityitem.Enabled = true;
                            submenu1.RemoveItemAt(3);
                            Quantityitem = new UIMenuListItem("Quantity", numberList, 0);
                            submenu1.AddItem(Quantityitem);                            
                            Quantityitem.Enabled = true;
                        }
                        else if (item.Index == 5)
                        {
                            numberList.Clear();
                            int limit = int.Parse(AvailableResources.ResourcesNumbers.RiceSeedQuantity.ToString());
                            var numberList1 = Enumerable.Range(1, limit).ToList();
                            foreach (var itemm in numberList1)
                            {
                                numberList.Add(itemm);
                            }

                            if (numberList.Count <= 0)
                            {
                                numberList.Add(0);
                            }

                            UI.ShowSubtitle("Rice : " + int.Parse(AvailableResources.ResourcesNumbers.RiceSeedQuantity.ToString()));
                            
                            submenu1.RemoveItemAt(3);
                            Quantityitem = new UIMenuListItem("Quantity", numberList, 0);
                            submenu1.AddItem(Quantityitem);
                            Quantityitem.Enabled = true;
                        }
                        else if (item.Index == 6)
                        {
                            numberList.Clear();
                            int limit = int.Parse(AvailableResources.ResourcesNumbers.BeetRootSeedQuantity.ToString());
                            var numberList1 = Enumerable.Range(1, limit).ToList();
                            foreach (var itemm in numberList1)
                            {
                                numberList.Add(itemm);
                            }

                            if (numberList.Count <= 0)
                            {
                                numberList.Add(0);
                            }
                            UI.ShowSubtitle("BeetRoot : " + int.Parse(AvailableResources.ResourcesNumbers.BeetRootSeedQuantity.ToString()));
                            
                            submenu1.RemoveItemAt(3);
                            Quantityitem = new UIMenuListItem("Quantity", numberList, 0);
                            submenu1.AddItem(Quantityitem);
                            Quantityitem.Enabled = true;
                        }
                        else if (item.Index == 7)
                        {
                            numberList.Clear();
                            int limit = AvailableResources.ResourcesNumbers.WeedSeedQuantity;                            
                            var numberList1 = Enumerable.Range(1, limit).ToList();
                            foreach (var itemm in numberList1)
                            {
                                numberList.Add(itemm);
                            }
                            if (numberList.Count <= 0)
                            {
                                numberList.Add(0);
                            }
                            UI.ShowSubtitle("Weed : " + int.Parse(AvailableResources.ResourcesNumbers.WeedSeedQuantity.ToString()));
                            submenu1.RemoveItemAt(3);
                            Quantityitem = new UIMenuListItem("Quantity", numberList, 0);
                            submenu1.AddItem(Quantityitem);
                            Quantityitem.Enabled = true;

                        }
                        submenu1.RefreshIndex();
                    }
                    else
                    {
                        //UI.ShowSubtitle("Only Few options can be selected quanity-wise");
                        submenu1.RemoveItemAt(3);
                        numberList.Clear();
                        numberList.Add(0);
                        Quantityitem = new UIMenuListItem("Quantity", numberList, 0);
                        submenu1.AddItem(Quantityitem);
                        Quantityitem.Enabled = false;
                    }
                }
                if (item == Quantityitem)
                {

                    selectedquantity = item.Index + 1;
                    //UI.ShowSubtitle("Seeds Selected : " + selectedquantity);
                    //UI.ShowSubtitle("Only Few options can be selected quanity-wise");

                }
            };

            //Do Action
            
            submenu1.AddItem(ActivateJob = new UIMenuItem("Activate Job", "Activates the Current Job on the List"));
            submenu1.AddItem(CancleCurrentJob = new UIMenuItem("Cancle Assigned Job", "To cancle the assigned Job"));

            Quantityitem = new UIMenuListItem("Quantity", numberList, 0);
            submenu1.AddItem(Quantityitem);
            Quantityitem.Enabled = false;

            submenu1.OnItemSelect += (sender, item, index) =>
            {
                if (modactive)
                {
                    if (item == Quantityitem)
                    {
                       
                    }
                    if (item == ActivateJob)
                    {
                        #region ActivateJob Tab Definition
                        for (int i = 0; i < listitems.Count; i++)
                        {
                            if (i == listsection.Index)
                            {
                                if (CurrentJobPlayer.Count < 1)
                                {
                                    #region Chop Wood Job
                                    if (listsection.IndexToItem(i) == JobQueues.ChopWood)
                                    {
                                        if (Class1.XPPoints.PlayerXP >= 400)
                                        {
                                            if (Class1.PlayerPed.Weapons.HasWeapon(WeaponHash.Hatchet))
                                            //if (Class1.PlayerPed.Weapons.Current.Hash == WeaponHash.Hatchet)
                                            {
                                                UI.Notify("Job Assigned : " + listsection.IndexToItem(i));
                                                string tempvalueholder = Convert.ToString(listsection.IndexToItem(i));
                                                CurrentJobPlayer.Add(tempvalueholder);
                                                UI.ShowSubtitle(CurrentJobPlayer[0].ToString());
                                                JobCriticality = 1;
                                                JobQueue.PlayerJobSelected = JobQueues.ChopWood.ToString();
                                                JobQueue.PlayerJobTypeSelected = JobTypes.ChoppingJob.ToString();
                                                AreaandWayPointSet();

                                            }
                                            else
                                            {
                                                UI.Notify("You don't have a proper tool for this job. Suitable tool for this job Hatchet, Get one from the weapon store.");

                                            }
                                        }
                                        else
                                        {
                                            UI.Notify(listsection.IndexToItem(i) + "  Requires 400+ XP, earn More XP by performing Lower Jobs first such as Vendor Goods Delivery ");
                                            break;
                                        }

                                    }
                                    #endregion

                                    #region StoneMiningJob
                                    if (listsection.IndexToItem(i) == JobQueues.StoneMining)
                                    {
                                        if (Class1.XPPoints.PlayerXP >= 1000)
                                        {
                                            if (Class1.PlayerPed.Weapons.HasWeapon(WeaponHash.Hammer))
                                            {
                                                UI.Notify("Job Assigned : " + listsection.IndexToItem(i));
                                                string tempvalueholder = Convert.ToString(listsection.IndexToItem(i));
                                                CurrentJobPlayer.Add(tempvalueholder);
                                                UI.ShowSubtitle(CurrentJobPlayer[0].ToString());
                                                JobCriticality = 2;
                                                JobQueue.PlayerJobSelected = JobQueues.StoneMining.ToString();
                                                JobQueue.PlayerJobTypeSelected = JobTypes.MiningJob.ToString();
                                                AreaandWayPointSet();
                                                //JobCreation.StoneBar.Percentage = 0.0f;
                                            }
                                            else
                                            {
                                                UI.Notify("You don't have a proper tool for this job. Suitable tools for this job : Hammer");
                                            }
                                        }
                                        else
                                        {
                                            UI.Notify(listsection.IndexToItem(i) + "  Requires 1000+ XP, earn More XP by performing Lower Jobs first such as Vendor Goods Delivery ");
                                            break;
                                        }

                                    }
                                    #endregion

                                    #region SteelMiningJob

                                    if (listsection.IndexToItem(i) == JobQueues.SteelMining)
                                    {
                                        if (Class1.XPPoints.PlayerXP >= 2000)
                                        {
                                            if (Class1.PlayerPed.Weapons.HasWeapon(WeaponHash.Hammer))
                                            {
                                                UI.Notify("Job Assigned : " + listsection.IndexToItem(i));
                                                string tempvalueholder = Convert.ToString(listsection.IndexToItem(i));
                                                CurrentJobPlayer.Add(tempvalueholder);
                                                UI.ShowSubtitle(CurrentJobPlayer[0].ToString());
                                                JobCriticality = 3;
                                                JobQueue.PlayerJobSelected = JobQueues.SteelMining.ToString();
                                                JobQueue.PlayerJobTypeSelected = JobTypes.MiningJob.ToString();
                                                AreaandWayPointSet();
                                                //JobCreation.SteelBar.Percentage = 0.0f;
                                            }
                                            else
                                            {
                                                UI.Notify("You don't have a proper tool for this job. Suitable tools for this job : Hammer");
                                            }
                                        }
                                        else
                                        {
                                            UI.Notify(listsection.IndexToItem(i) + "  Requires 2000+ XP, earn More XP by performing Lower Jobs first such as Vendor Goods Delivery ");
                                            break;
                                        }

                                    }
                                    #endregion

                                    #region CopperMiningJob

                                    if (listsection.IndexToItem(i) == JobQueues.CopperMining)
                                    {
                                        if (Class1.XPPoints.PlayerXP >= 3000)
                                        {
                                            if (Class1.PlayerPed.Weapons.HasWeapon(WeaponHash.Machete))
                                            {
                                                UI.Notify("Job Assigned : " + listsection.IndexToItem(i));
                                                string tempvalueholder = Convert.ToString(listsection.IndexToItem(i));
                                                CurrentJobPlayer.Add(tempvalueholder);
                                                UI.ShowSubtitle(CurrentJobPlayer[0].ToString());
                                                JobCriticality = 4;
                                                JobQueue.PlayerJobSelected = JobQueues.CopperMining.ToString();
                                                JobQueue.PlayerJobTypeSelected = JobTypes.MiningJob.ToString();
                                                AreaandWayPointSet();
                                                //JobCreation.CopperBar.Percentage = 0.0f;
                                            }
                                            else
                                            {
                                                UI.Notify("You don't have a proper tool for this job. Suitable tools for this job : Matchet");
                                            }
                                        }
                                        else
                                        {
                                            UI.Notify(listsection.IndexToItem(i) + "  Requires 3000+ XP, earn More XP by performing Lower Jobs first such as Vendor Goods Delivery ");
                                            break;
                                        }

                                    }
                                    #endregion

                                    #region HarvestingJob


                                    #region Wheat_Cultivate
                                    if (listsection.IndexToItem(i) == JobQueues.Cultivate_Wheat)
                                    {
                                        if (LandPurchased)
                                        {
                                            if (Class1.XPPoints.PlayerXP >= 5000)
                                            {
                                                if (Vehicel_Shop_Dealer.PlayerTractor != null && Vehicel_Shop_Dealer.PlayerCultivator != null)
                                                {
                                                       
                                                    if (AvailableResources.ResourcesNumbers.WheatSeedQuantity > 100)
                                                    {
                                                        if (selectedquantity > 99)
                                                        {
                                                            if (!Farming_Class.seedssowed)//if (Farming_Class.CropName != "" && Farming_Class.seedssowed)
                                                            {
                                                                AvailableResources.ResourcesNumbers.WheatSeedQuantity -= selectedquantity;
                                                                UI.Notify("Job Assigned : " + listsection.IndexToItem(i));
                                                                string tempvalueholder = Convert.ToString(listsection.IndexToItem(i));
                                                                CurrentJobPlayer.Add(tempvalueholder);
                                                                UI.ShowSubtitle(CurrentJobPlayer[0].ToString());
                                                                JobCriticality = 5;
                                                                JobQueue.PlayerJobSelected = JobQueues.Cultivate_Wheat.ToString();
                                                                JobQueue.PlayerJobTypeSelected = JobTypes.HarvestingJob.ToString();
                                                                Farming_Class.CropName = "Wheat";
                                                                Farming_Class.farming = true;
                                                                //JobCreation.WheatBar.Percentage = 0.0f;
                                                            }
                                                            else
                                                            {
                                                                UI.Notify("Current Crop found : " + Farming_Class.CropName);
                                                                UI.Notify("Wait for the current crops to grow completly.");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            UI.Notify("Select more atleast 100 units of the seeds to sow");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        UI.Notify("You will need to have atleast 100 seeds of this type, inorder to plant the crop");
                                                        UI.Notify("Tip : Seeds can be purchased from the market by selecting the shop location from menu");
                                                        break;
                                                        
                                                    }
                                                }
                                                else
                                                {
                                                    UI.Notify("You do not own a tractor and cultivator yet, buy that from the Vehicle Dealer first");
                                                }

                                            }
                                            else
                                            {
                                                UI.Notify(listsection.IndexToItem(i) + "  Requires 5000+ XP, earn More XP by performing Lower Jobs first such as Vendor Goods Delivery ");
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            UI.Notify("You will need to own the Land first, in order to start with the harvesting job");
                                            UI.Notify("Tip : Land can be purchased from the Property Dealers by selecting their office location from menu. This feature will get auto unlocked as you progress further.");
                                        }

                                    }
                                    #endregion

                                    #region Rice_Cultivate
                                    if (listsection.IndexToItem(i) == JobQueues.Cultivate_Rice)
                                    {
                                        if (LandPurchased)
                                        {
                                            if (Class1.XPPoints.PlayerXP >= 5000)
                                            {
                                                if (AvailableResources.ResourcesNumbers.RiceSeedQuantity > 100)
                                                {
                                                    if (Vehicel_Shop_Dealer.PlayerTractor != null && Vehicel_Shop_Dealer.PlayerCultivator != null)
                                                    {
                                                        if (selectedquantity > 99)
                                                        {
                                                            if (!Farming_Class.seedssowed)//if (Farming_Class.CropName != "" && Farming_Class.seedssowed)
                                                            {
                                                                UI.Notify("Job Assigned : " + listsection.IndexToItem(i));
                                                                AvailableResources.ResourcesNumbers.RiceSeedQuantity -= selectedquantity;
                                                                string tempvalueholder = Convert.ToString(listsection.IndexToItem(i));
                                                                CurrentJobPlayer.Add(tempvalueholder);
                                                                UI.ShowSubtitle(CurrentJobPlayer[0].ToString());
                                                                JobCriticality = 6;
                                                                JobQueue.PlayerJobSelected = JobQueues.Cultivate_Rice.ToString();
                                                                JobQueue.PlayerJobTypeSelected = JobTypes.HarvestingJob.ToString();
                                                                Farming_Class.CropName = "Rice";
                                                                Farming_Class.farming = true;
                                                            }
                                                            else
                                                            {
                                                                UI.Notify("Current Crop found : " + Farming_Class.CropName);
                                                                UI.Notify("Wait for the current crops to grow completly.");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            UI.Notify("Select more atleast 100 units of the seeds to sow");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        UI.Notify("You do not own a tractor and cultivator yet, buy that from the Vehicle Dealer first");
                                                    }
                                                }
                                                else
                                                {
                                                    UI.Notify("You will need to have atleast 100 seeds of this type, inorder to plant the crop");
                                                    UI.Notify("Tip : Seeds can be purchased from the market by selecting the shop location from menu");
                                                    break;
                                                }

                                            }
                                            else
                                            {
                                                UI.Notify(listsection.IndexToItem(i) + "  Requires 5000+ XP, earn More XP by performing Lower Jobs first such as Vendor Goods Delivery ");
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            UI.Notify("You will need to own the Land first, in order to start with the harvesting job");
                                            UI.Notify("Tip : Land can be purchased from the Property Dealers by selecting their office location from menu. This feature will get auto unlocked as you progress further.");
                                        }

                                    }
                                    #endregion


                                    #region BeetRoot_Cultivate
                                    if (listsection.IndexToItem(i) == JobQueues.Cultivate_BeetRoot)
                                    {
                                        if (LandPurchased)
                                        {
                                            if (Class1.XPPoints.PlayerXP >= 5000)
                                            {
                                                if (AvailableResources.ResourcesNumbers.BeetRootSeedQuantity > 100)
                                                {
                                                    if (Vehicel_Shop_Dealer.PlayerTractor != null && Vehicel_Shop_Dealer.PlayerCultivator != null)
                                                    {
                                                        if (selectedquantity > 99)
                                                        {
                                                            if (!Farming_Class.seedssowed)//if (Farming_Class.CropName != "" && Farming_Class.seedssowed)
                                                            {
                                                                UI.Notify("Job Assigned : " + listsection.IndexToItem(i));
                                                                AvailableResources.ResourcesNumbers.BeetRootSeedQuantity -= selectedquantity;
                                                                string tempvalueholder = Convert.ToString(listsection.IndexToItem(i));
                                                                CurrentJobPlayer.Add(tempvalueholder);
                                                                UI.ShowSubtitle(CurrentJobPlayer[0].ToString());
                                                                JobCriticality = 7;
                                                                JobQueue.PlayerJobSelected = JobQueues.Cultivate_BeetRoot.ToString();
                                                                JobQueue.PlayerJobTypeSelected = JobTypes.HarvestingJob.ToString();
                                                                Farming_Class.CropName = "BeetRoot";
                                                                Farming_Class.farming = true;
                                                            }
                                                            else
                                                            {
                                                                UI.Notify("Current Crop found : " + Farming_Class.CropName);
                                                                UI.Notify("Wait for the current crops to grow completly.");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            UI.Notify("Select more atleast 100 units of the seeds to sow");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        UI.Notify("You do not own a tractor and cultivator yet, buy that from the Vehicle Dealer first");
                                                    }
                                                }
                                                else
                                                {
                                                    UI.Notify("You will need to have atleast 100 seeds of this type, inorder to plant the crop");
                                                    UI.Notify("Tip : Seeds can be purchased from the market by selecting the shop location from menu");
                                                    break;
                                                }

                                            }
                                            else
                                            {
                                                UI.Notify(listsection.IndexToItem(i) + "  Requires 5000+ XP, earn More XP by performing Lower Jobs first such as Vendor Goods Delivery ");
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            UI.Notify("You will need to own the Land first, in order to start with the harvesting job");
                                            UI.Notify("Tip : Land can be purchased from the Property Dealers by selecting their office location from menu. This feature will get auto unlocked as you progress further.");
                                        }

                                    }
                                    #endregion


                                    #region Weed_Cultivate
                                    if (listsection.IndexToItem(i) == JobQueues.Cultivate_Weed)
                                    {
                                        if (LandPurchased)
                                        {
                                            if (Class1.XPPoints.PlayerXP >= 5000)
                                            {
                                                if (AvailableResources.ResourcesNumbers.WeedSeedQuantity > 100)
                                                {
                                                    if (Vehicel_Shop_Dealer.PlayerTractor != null && Vehicel_Shop_Dealer.PlayerCultivator != null)
                                                    {
                                                        if (selectedquantity > 99)
                                                        {
                                                            if (!Farming_Class.seedssowed)//if (Farming_Class.CropName != "" && Farming_Class.seedssowed)
                                                            {
                                                                UI.Notify("Job Assigned : " + listsection.IndexToItem(i));
                                                                AvailableResources.ResourcesNumbers.WeedSeedQuantity -= selectedquantity;
                                                                string tempvalueholder = Convert.ToString(listsection.IndexToItem(i));
                                                                CurrentJobPlayer.Add(tempvalueholder);
                                                                UI.ShowSubtitle(CurrentJobPlayer[0].ToString());
                                                                JobCriticality = 8;
                                                                JobQueue.PlayerJobSelected = JobQueues.Cultivate_Weed.ToString();
                                                                JobQueue.PlayerJobTypeSelected = JobTypes.HarvestingJob.ToString();
                                                                Farming_Class.CropName = "Weed";
                                                                Farming_Class.farming = true;
                                                            }
                                                            else
                                                            {
                                                                UI.Notify("Current Crop found : " + Farming_Class.CropName);
                                                                UI.Notify("Wait for the current crops to grow completly.");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            UI.Notify("Select more atleast 100 units of the seeds to sow");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        UI.Notify("You do not own a tractor and cultivator yet, buy that from the Vehicle Dealer first");
                                                    }
                                                }
                                                else
                                                {
                                                    UI.Notify("You will need to have atleast 100 seeds of this type, inorder to plant the crop");
                                                    UI.Notify("Tip : Seeds can be purchased from the market by selecting the shop location from menu");
                                                    break;
                                                }

                                            }
                                            else
                                            {
                                                UI.Notify(listsection.IndexToItem(i) + "  Requires 5000+ XP, earn More XP by performing Lower Jobs first such as Vendor Goods Delivery ");
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            UI.Notify("You will need to own the Land first, in order to start with the harvesting job");
                                            UI.Notify("Tip : Land can be purchased from the Property Dealers by selecting their office location from menu. This feature will get auto unlocked as you progress further.");
                                        }

                                    }
                                    #endregion


                                    #endregion                                  


                                }
                                else
                                {
                                    UI.Notify("Cancle the Active Job First : " + CurrentJobPlayer[0].ToString());
                                    break;
                                }
                            }
                            if (AllPositionsListed.ZonePositionList.Count <= 0)
                            {
                                AllPositionsListed.ZonePositionList.Add(new Vector3(-1576.859f, 4527.854f, 17.702f));
                                AllPositionsListed.ZonePositionList.Add(new Vector3(-1544.063f, 4479.107f, 18.95f));
                                AllPositionsListed.ZonePositionList.Add(new Vector3(-1375.717f, 4422.273f, 29.05f));
                                AllPositionsListed.ZonePositionList.Add(new Vector3(-1092.079f, 4415.969f, 14.042f));
                                AllPositionsListed.ZonePositionList.Add(new Vector3(-200.811f, 4015.894f, 33.721f));
                                AllPositionsListed.ZonePositionList.Add(new Vector3(-236.1767f, 3816.449f, 40.66367f));
                                AllPositionsListed.ZonePositionList.Add(new Vector3(-166.4523f, 3721.109f, 39.56227f));
                                AllPositionsListed.ZonePositionList.Add(new Vector3(147.871f, 3257.442f, 41.07584f));
                                AllPositionsListed.ZonePositionList.Add(new Vector3(187.3716f, 3149.84f, 42.36256f));
                                UI.Notify("No Resource Locations Found, Adding default list.");
                            }

                        }
                        #endregion

                       
                    }

                    if (item == CancleCurrentJob)
                    {
                        if (GTA.Game.Player.Character.IsOnFoot)
                        {
                            #region CancleJob Definition
                            if (CurrentJobPlayer.Count > 0)
                            {
                                Attacker_Class.JobCancled = true;
                                //AttackerClassTempObject.CancleJobEnemyRemove();
                                if (Delivery_Jobs.WoodTruck != null)
                                {
                                    Delivery_Jobs.CancleJob();                                    
                                }

                                if (CurrentJobPlayer.Count > 0)
                                {
                                    UI.Notify("Job Removed : " + CurrentJobPlayer[0]);
                                    CurrentJobPlayer.RemoveAt(0);
                                }                                


                                if (JobCreation.myBar != null)
                                {
                                    //JobQueue.PlayerJobSelected = "";                                    
                                    //JobCreation.MyBarText.Text = "";
                                    //JobCreation.MyBarText.Label = "";
                                    //JobCreation.myBar.Label = "";
                                    //JobCreation.myPool.Remove(JobCreation.myBar);
                                    //JobCreation.myPool.Remove(JobCreation.MyBarText);
                                    //JobCreation.myBar = null;
                                    //JobCreation.MyBarText = null;
                                    JobCreation.baradded = false;
                                }

                                if (Game.IsWaypointActive)
                                {
                                    Function.Call(Hash.SET_WAYPOINT_OFF);
                                }

                                Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                                Function.Call(Hash.REFRESH_WAYPOINT);

                                if (TemporaryVehicle != null)
                                {
                                    if (TemporaryVehicle.Exists() && TemporaryVehicle.CurrentBlip.Exists())
                                    {
                                        TemporaryVehicle.CurrentBlip.Remove();
                                    }

                                    TemporaryVehicle.Delete();
                                    TemporaryVehicle = null;

                                }

                                if (ZoneBlip != null)
                                {
                                    if (ZoneBlip.Exists())
                                    {
                                        ZoneBlip.Remove();
                                        ZoneBlip = null;
                                        if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                                        {
                                            Function.Call(Hash.SET_WAYPOINT_OFF);
                                        }
                                    }
                                }
                                if (holder != null)
                                {
                                    if (holder.Exists() && holderblip.Exists())
                                    {
                                        holderblip.Remove();
                                        holderblip = null;
                                    }

                                    holder.Delete();
                                    holder = null;
                                }
                                
                                XML_Import_Export.XMLSaving();
                            }
                            else
                            {
                                UI.Notify("No Active Jobs Found");
                            }
                            #endregion
                        }
                        else
                        {
                            UI.Notify("Exit the current vehicle in order to cancle the job");
                        }
                    }
                }
                else
                {
                    UI.Notify("Activate the Mod from previous menu");
                }

            };
            //submenu1.AddItem(new UIMenuItem("2nd Item", "This is a different way to add a new menu item; however, it has the same result!"));



        }//Menu1 ends here


        private void Submenu1_OnItemSelect(UIMenu sender, UIMenuItem selectedItem, int index)
        {
            throw new NotImplementedException();
        }

        public void Menu2(UIMenu menu)
        {
            UIMenu submenu = _menuPool.AddSubMenu(menu, "Resources", "Current Available Resources with you");

            for (int i = 0; i < 1; i++)
            {
                submenu.AddItem(PlayerXPAndMoney = new UIMenuItem("XP & Money", "~b~Units of available XP Points & Money with you"));
                UIMenu RawResources = _menuPool.AddSubMenu(submenu, "Raw Resources", "~b~Units of available Raw Resources with you");
                UIMenu ProcessedResources = _menuPool.AddSubMenu(submenu, "Processed Resources", "~b~Units of available Processed Resources with you");
                UIMenu HarvestingResources = _menuPool.AddSubMenu(submenu, "Harvesting Resources", "~b~Units of available to Harvest");
                UIMenu CattleResources = _menuPool.AddSubMenu(submenu, "Cattle Resources", "~b~Units of available Cattle");

                #region RawResources Region                
                RawResources.AddItem(new UIMenuItem("Hit Enter on each option for more info"));
                RawResources.AddItem(Woodinfo = new UIMenuItem("Wood", "~b~Units of available chopped wood with you"));
                RawResources.AddItem(Stoneinfo = new UIMenuItem("Stone", "~r~Units of available mined stone with you"));
                RawResources.AddItem(Steelinfo = new UIMenuItem("Raw Steel", "~g~Units of available raw steel with you"));
                RawResources.AddItem(Copperinfo = new UIMenuItem("Copper", "~b~Units of available copper with you"));
                #endregion

                #region ProcessedResources Region
                ProcessedResources.AddItem(new UIMenuItem("Hit Enter on each option for more info"));
                ProcessedResources.AddItem(Wheatinfo = new UIMenuItem("Wheat", "~b~Units of available Wheat with you"));
                ProcessedResources.AddItem(Riceinfo = new UIMenuItem("Rice", "~r~Units of available Rice with you"));
                ProcessedResources.AddItem(BeetRootinfo = new UIMenuItem("BeetRoot", "~g~Units of available Beet Root with you"));
                ProcessedResources.AddItem(Weedinfo = new UIMenuItem("Weed", "~b~Units of available Weed with you"));
                #endregion

                #region Harvesting Region
                HarvestingResources.AddItem(new UIMenuItem("Hit Enter on each option for more info"));
                HarvestingResources.AddItem(WheatSeedsinfo = new UIMenuItem("Wheat Seeds", "~b~Units of available Wheat Seeds with you"));
                HarvestingResources.AddItem(RiceSeedsinfo = new UIMenuItem("Rice Seeds", "~r~Units of available Rice Seeds with you"));
                HarvestingResources.AddItem(BeetRootSeedsinfo = new UIMenuItem("BeetRoot Seeds", "~g~Units of available Beet Root Seeds with you"));
                HarvestingResources.AddItem(WeedSeedsinfo = new UIMenuItem("Weed Seeds", "~b~Units of available Weed Seeds with you"));
                #endregion

                #region Cattle Region
                CattleResources.AddItem(new UIMenuItem("Hit Enter on each option for more info"));
                CattleResources.AddItem(CowMilkinfo = new UIMenuItem("Cow Milk", "~b~Units of available Cow Milk with you"));
                CattleResources.AddItem(Cheeseinfo = new UIMenuItem("Cheese", "~r~Units of available Cheese with you"));
                CattleResources.AddItem(PigMeatinfo = new UIMenuItem("Pig Meat", "~g~Units of available Pig Meat with you"));
                CattleResources.AddItem(CowDunkinfo = new UIMenuItem("Cow Dunk", "~b~Units of available Cow Dunk with you"));
                #endregion

                submenu.OnItemSelect += (sender, item, index) => 
                {
                    if (item == PlayerXPAndMoney)
                    {
                        int Availablemoney = Class1.XPPoints.PlayerMoney;
                        int AvailableXP = Class1.XPPoints.PlayerXP;
                        UI.Notify("Available XP : " + AvailableXP.ToString());
                        UI.Notify("Available Money : " + Availablemoney.ToString());
                    }
                };
                RawResources.OnItemSelect += (sender, item, index) =>
                {
                    if (modactive)
                    {
                        #region RawResources onItemSelect
                        if (item == Woodinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString() + " units of Chopped Wood");
                        }
                        else if (item == Stoneinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.StoneQuantity.ToString() + " units of stone");
                        }
                        else if (item == Steelinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.RawSteelQuantity.ToString() + " units of steel");
                        }
                        else if (item == Copperinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.CopperQuantity.ToString() + " units of copper");
                        }
                        #endregion                        
                    }
                    else
                    {
                        UI.Notify("Activate the Mod from previous menu");
                    }
                };

                ProcessedResources.OnItemSelect += (sender, item, index) =>
                {
                    if (modactive)
                    {
                        #region ProcessedResources onItemSelect
                        if (item == Wheatinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.WheatQuantity.ToString() + " units of Wheat");
                        }
                        else if (item == Riceinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.RiceQuantity.ToString() + " units of Rice");
                        }
                        else if (item == BeetRootinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.BeetRootQuantity.ToString() + " units of BeetRoot");
                        }
                        else if (item == Weedinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.WeedQuantity.ToString() + " units of Weed");
                        }
                        #endregion
                    }
                    else
                    {
                        UI.Notify("Activate the Mod from previous menu");
                    }
                };

                HarvestingResources.OnItemSelect += (sender, item, index) =>
                {
                    if (modactive)
                    {
                        #region SeedResources onItemSelect
                        if (item == WheatSeedsinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.WheatSeedQuantity.ToString() + " units of Wheat Seed");
                        }
                        else if (item == RiceSeedsinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.RiceSeedQuantity.ToString() + " units of Rice Seed");
                        }
                        else if (item == BeetRootSeedsinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.BeetRootSeedQuantity.ToString() + " units of BeetRoot Seed");
                        }
                        else if (item == WeedSeedsinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.WeedSeedQuantity.ToString() + " units of Weed Seed");
                        }
                        #endregion
                    }
                    else
                    {
                        UI.Notify("Activate the Mod from previous menu");
                    }
                };

                CattleResources.OnItemSelect += (sender, item, index) =>
                {
                    if (modactive)
                    {
                        #region CattleResources onItemSelect
                        if (item == CowMilkinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.CowMilkQuantity.ToString() + " units of Cow Milk");
                        }
                        else if (item == Cheeseinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.CheeseQuantity.ToString() + " units of Cheese");
                        }
                        else if (item == PigMeatinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.PigMeatQuantity.ToString() + " units of PigMeat");
                        }
                        else if (item == CowDunkinfo)
                        {
                            UI.Notify("You have " + AvailableResources.ResourcesNumbers.CowDunkQuantity.ToString() + " units of Cow Dunk");
                        }
                        #endregion
                    }
                    else
                    {
                        UI.Notify("Activate the Mod from previous menu");
                    }
                };

            }

            //UIMenuItem fixcar = new UIMenuItem("Current Market Price", "~r~Description with red text");
            //submenu.AddItem(fixcar);

        }//Menu2 ends here


        #region Staff_Settings

        private enum Assistants_Queue
        {
            #region Assistant_Queue
            Employee1,
            Employee2,
            Employee3,
            Employee4,
            Employee5
            #endregion

        };

        private enum Priorities
        {
            
            
        };
        public void Menu3(UIMenu menu)
        {
            //int[] arr = Enumerable.Range(0, 10).ToArray();

            List<dynamic> PriorityRange = new List<dynamic>()
            {
                1,2,3,4,5,6,7,8,9,10
            };

            List<dynamic> AssitantList = new List<dynamic>()
            {   
                Assistants_Queue.Employee1,
                Assistants_Queue.Employee2,
                Assistants_Queue.Employee3,
                Assistants_Queue.Employee4,
                Assistants_Queue.Employee5,
            };

            UIMenuListItem Employee_List_Section = new UIMenuListItem("Available Staff : ", AssitantList, 0);
            UIMenuListItem Gun_Fight_Priority = new UIMenuListItem("Gun-Fight : ", PriorityRange, 0);
            UIMenuListItem Driving_Priority = new UIMenuListItem("Driving : ", PriorityRange, 0);
            UIMenuListItem Farming_Priority = new UIMenuListItem("Farming : ", PriorityRange, 0);
            UIMenuListItem Negotiation_Priority = new UIMenuListItem("Negotiation : ", PriorityRange, 0);


            UIMenu submenu = _menuPool.AddSubMenu(menu, "Staff", "Current Available Staff with you");
            UIMenu submenu2 = _menuPool.AddSubMenu(submenu, "Available to Hire", "Shows Available Staff to hire");
            UIMenu Priorities_Menu = _menuPool.AddSubMenu(submenu, "Priorities", "Set Priority of the Staff");
            UIMenuItem DoneItem = new UIMenuItem("Done", "Finalize the Settings");
            UIMenuItem DoneItemHire = new UIMenuItem("Done", "Hire the Selected Resource");

            for (int i = 0; i < 1; i++)
            {
                submenu2.AddItem(Employee_List_Section);
                Priorities_Menu.AddItem(Employee_List_Section);
                Priorities_Menu.AddItem(Gun_Fight_Priority);
                Priorities_Menu.AddItem(Driving_Priority);
                Priorities_Menu.AddItem(Farming_Priority);
                Priorities_Menu.AddItem(Negotiation_Priority);
                Priorities_Menu.AddItem(DoneItem);
            }

            

            for (int i = 0; i < 1; i++)
            {   
                submenu.AddItem(Seasonal_Help = new UIMenuItem("Seasonal Help : 0", "~b~Seasonal Help"));
                Seasonal_Help.Enabled = false;                
                submenu.AddItem(Fire_Staff = new UIMenuItem("Fire a Staff Member", "~g~Fire a Staff Member"));
                submenu2.AddItem(DoneItemHire);
            }

            Priorities_Menu.OnListChange += (sender, item, index) => 
            {
                if (item == Employee_List_Section)
                {
                   
                    UI.ShowSubtitle(Employee_List_Section.Index.ToString());

                    if (Employee_List_Section.Index == 0)
                    {
                        Gun_Fight_Priority.Index = Priorities_Class.GunFight_Assistant1 - 1;
                        Driving_Priority.Index = Priorities_Class.Driving_Assistant1 - 1;
                        Farming_Priority.Index = Priorities_Class.Farming_Assistant1 - 1;
                        Negotiation_Priority.Index = Priorities_Class.Negotiation_Assistant1 - 1;
                    }


                    if (Employee_List_Section.Index == 1)
                    {
                        Gun_Fight_Priority.Index = Priorities_Class.GunFight_Assistant2 - 1;
                        Driving_Priority.Index = Priorities_Class.Driving_Assistant2 - 1;
                        Farming_Priority.Index = Priorities_Class.Farming_Assistant2 - 1;
                        Negotiation_Priority.Index = Priorities_Class.Negotiation_Assistant2 - 1;
                    }


                    if (Employee_List_Section.Index == 2)
                    {
                        Gun_Fight_Priority.Index = Priorities_Class.GunFight_Assistant3 - 1;
                        Driving_Priority.Index = Priorities_Class.Driving_Assistant3 - 1;
                        Farming_Priority.Index = Priorities_Class.Farming_Assistant3 - 1;
                        Negotiation_Priority.Index = Priorities_Class.Negotiation_Assistant3 - 1;
                    }

                    if (Employee_List_Section.Index == 3)
                    {
                        Gun_Fight_Priority.Index = Priorities_Class.GunFight_Assistant4 - 1;
                        Driving_Priority.Index = Priorities_Class.Driving_Assistant4 - 1;
                        Farming_Priority.Index = Priorities_Class.Farming_Assistant4 - 1;
                        Negotiation_Priority.Index = Priorities_Class.Negotiation_Assistant4 - 1;
                    }

                    if (Employee_List_Section.Index == 4)
                    {
                        Gun_Fight_Priority.Index = Priorities_Class.GunFight_Assistant5 - 1;
                        Driving_Priority.Index = Priorities_Class.Driving_Assistant5 - 1;
                        Farming_Priority.Index = Priorities_Class.Farming_Assistant5 - 1;
                        Negotiation_Priority.Index = Priorities_Class.Negotiation_Assistant5 - 1;
                    }

                    if (Employee_List_Section.Index == 5)
                    {
                        Gun_Fight_Priority.Index = Priorities_Class.GunFight_Assistant6 - 1;
                        Driving_Priority.Index = Priorities_Class.Driving_Assistant6 - 1;
                        Farming_Priority.Index = Priorities_Class.Farming_Assistant6 - 1;
                        Negotiation_Priority.Index = Priorities_Class.Negotiation_Assistant6 - 1;
                    }
                    //UI.Notify("Reached Here");
                }

                
            };
            Priorities_Menu.OnItemSelect += (sender, item, index) =>
            {
                if (item == DoneItem)
                {
                    if (Employee_List_Section.Index == 0)
                    {
                        Priorities_Class.GunFight_Assistant1 = (Gun_Fight_Priority.Index + 1);
                        Priorities_Class.Driving_Assistant1 = (Driving_Priority.Index + 1);
                        Priorities_Class.Farming_Assistant1 = (Farming_Priority.Index + 1);
                        Priorities_Class.Negotiation_Assistant1 = (Negotiation_Priority.Index + 1);                        
                    }

                    if (Employee_List_Section.Index == 1)
                    {
                        Priorities_Class.GunFight_Assistant2 = (Gun_Fight_Priority.Index + 1);
                        Priorities_Class.Driving_Assistant2 = (Driving_Priority.Index + 1);
                        Priorities_Class.Farming_Assistant2 = (Farming_Priority.Index + 1);
                        Priorities_Class.Negotiation_Assistant2 = (Negotiation_Priority.Index + 1);
                    }


                    if (Employee_List_Section.Index == 2)
                    {
                        Priorities_Class.GunFight_Assistant3 = (Gun_Fight_Priority.Index + 1);
                        Priorities_Class.Driving_Assistant3 = (Driving_Priority.Index + 1);
                        Priorities_Class.Farming_Assistant3 = (Farming_Priority.Index + 1);
                        Priorities_Class.Negotiation_Assistant3 = (Negotiation_Priority.Index + 1);
                    }


                    if (Employee_List_Section.Index == 3)
                    {
                        Priorities_Class.GunFight_Assistant4 = (Gun_Fight_Priority.Index + 1);
                        Priorities_Class.Driving_Assistant4 = (Driving_Priority.Index + 1);
                        Priorities_Class.Farming_Assistant4 = (Farming_Priority.Index + 1);
                        Priorities_Class.Negotiation_Assistant4 = (Negotiation_Priority.Index + 1);
                    }

                    if (Employee_List_Section.Index == 4)
                    {
                        Priorities_Class.GunFight_Assistant5 = (Gun_Fight_Priority.Index + 1);
                        Priorities_Class.Driving_Assistant5 = (Driving_Priority.Index + 1);
                        Priorities_Class.Farming_Assistant5 = (Farming_Priority.Index + 1);
                        Priorities_Class.Negotiation_Assistant5 = (Negotiation_Priority.Index + 1);
                    }

                    if (Employee_List_Section.Index == 5)
                    {
                        Priorities_Class.GunFight_Assistant6 = (Gun_Fight_Priority.Index + 1);
                        Priorities_Class.Driving_Assistant6 = (Driving_Priority.Index + 1);
                        Priorities_Class.Farming_Assistant6 = (Farming_Priority.Index + 1);
                        Priorities_Class.Negotiation_Assistant6 = (Negotiation_Priority.Index + 1);
                    }

                    UI.Notify("Saved");
                }
            };



        }//Menu3 ends here

        #endregion

        public void DrivingGoodsMenu(UIMenu menu)
        {
            UIMenu DriveGoods = _menuPool.AddSubMenu(menu, "Delivery Jobs");
            UIMenuItem SelectDeliveryJobs = new UIMenuItem("Select the Jobs from Vendor", "This will earn you the XP and Money");
            UIMenuItem SelectGoods = new UIMenuItem("Deliver Goods from Your Inventory", "Hit Enter!, Select Goods & Quantities from next screen.");
            DriveGoods.AddItem(SelectDeliveryJobs);
            DriveGoods.AddItem(SelectGoods);
            
            DriveGoods.OnItemSelect += (sender, item, index) =>
            {//Driving main menu
                if (item == SelectDeliveryJobs)
                {
                    if (CurrentJobPlayer.Count == 0)
                    {
                        DrivingVendorJobsMenu();                        
                    }
                    else
                    {
                        UI.ShowSubtitle("Active Job Found.! " + CurrentJobPlayer[0].ToString() + " Either Complete the Assigned Job or Cancle it from the menu"); //UI.Notify("Cancle the Active Job First : " + CurrentJobPlayer[0].ToString());
                    }
                }

                if (item == SelectGoods)
                {
                  
                    if (CurrentJobPlayer.Count == 0)
                    {
                        DrivingSimpleMenu();
                    }
                    else
                    {
                        UI.ShowSubtitle("Active Job Found.! " + CurrentJobPlayer[0].ToString() + " Either Complete the Assigned Job or Cancle it from the menu"); //UI.Notify("Cancle the Active Job First : " + CurrentJobPlayer[0].ToString());
                    }

                }
            };
        }

        private void DrivingVendorJobsMenu()
        {
            if (modactive)
            {
                #region Driving_Menu_onItemSelect
                InitMenu2();
                _menuPool.CloseAllMenus();                
                _menuPool3.ProcessMenus();
                mainMenu.Visible = false;
                mainMenu3.IsVisible = true;
                //_menuPool3.AddMenu(mainMenu3);
                //InitMenu2();
                #endregion
            }
            else
            {
                UI.Notify("Activate the Mod from previous menu");
            }
        }

        private void MainNAtiveUIMenu()
        {
            if (modactive)
            {
                #region Driving_Menu_onItemSelect
                _menuPool2.CloseAllMenus();
                _menuPool.ProcessMenus();
                mainMenu.Visible = true;
                mainMenu2.IsVisible = false;
                #endregion
            }
            else
            {
                UI.Notify("Activate the Mod from previous menu");
            }
        }

        private void DrivingSimpleMenu()
        {
            if (modactive)
            {
                #region Driving_Menu_onItemSelect
                _menuPool.CloseAllMenus();
                _menuPool2.ProcessMenus();
                mainMenu.Visible = false;
                mainMenu2.IsVisible = true;
                #endregion
            }
            else
            {
                UI.Notify("Activate the Mod from previous menu");
            }
        }


        public void Nearest_Shop(UIMenu menu)
        {
            UIMenu NearestShop = _menuPool.AddSubMenu(menu, "Shop Menu");
            UIMenuItem SelectNearestToolsShop = new UIMenuItem("Find Nearest Tool Shop", "Option will Auto Locate the Nearest Shop");
            UIMenuItem SelectNearestVehicleShop = new UIMenuItem("Vehicle Dealer", "Option will Auto Locate the Nearest Shop");
            UIMenuItem SelectNearestLandDealer = new UIMenuItem("Real Estate", "Option will Auto Locate the Nearest Shop");

            NearestShop.AddItem(SelectNearestToolsShop);
            NearestShop.AddItem(SelectNearestVehicleShop);
            NearestShop.AddItem(SelectNearestLandDealer);
            NearestShop.OnItemSelect += (sender, item, index) =>
            {//Driving main menu

                if (item == SelectNearestToolsShop)
                {
                    if (modactive)
                    {
                        #region Nearest_Tool_onItemSelect
                        if (CurrentJobPlayer.Count == 0)
                        {
                            Vector3 PlayerCurrentPosition = GTA.Game.Player.Character.Position;

                            floatStoredistance1 = World.GetDistance(PlayerCurrentPosition, new Vector3(377.999f, 326.656f, 103.566f));
                            floatStoredistance2 = World.GetDistance(PlayerCurrentPosition, new Vector3(1137.728f, -981.551f, 46.416f));
                            floatStoredistance3 = World.GetDistance(PlayerCurrentPosition, new Vector3(-1223.76f, -905.604f, 12.326f));
                            floatStoredistance4 = World.GetDistance(PlayerCurrentPosition, new Vector3(-1488.766f, -380.33f, 40.163f));
                            floatStoredistance5 = World.GetDistance(PlayerCurrentPosition, new Vector3(-2970.112f, 390.817f, 15.043f));
                            floatStoredistance6 = World.GetDistance(PlayerCurrentPosition, new Vector3(1165.886f, 2706.462f, 38.158f));
                            floatStoredistance7 = World.GetDistance(PlayerCurrentPosition, new Vector3(29.839f, -1345.9f, 29.497f));
                            floatStoredistance8 = World.GetDistance(PlayerCurrentPosition, new Vector3(-51.545f, -1753.753f, 29.421f));
                            floatStoredistance9 = World.GetDistance(PlayerCurrentPosition, new Vector3(1157.933f, -323.593f, 69.205f));
                            floatStoredistance10 = World.GetDistance(PlayerCurrentPosition, new Vector3(2555.16f, 385.334f, 108.623f));
                            floatStoredistance11 = World.GetDistance(PlayerCurrentPosition, new Vector3(-1824.991f, 790.218f, 138.211f));
                            floatStoredistance12 = World.GetDistance(PlayerCurrentPosition, new Vector3(-3042.18f, 588.288f, 7.909f));
                            floatStoredistance13 = World.GetDistance(PlayerCurrentPosition, new Vector3(-3243.737f, 1004.556f, 12.831f));
                            floatStoredistance14 = World.GetDistance(PlayerCurrentPosition, new Vector3(-712.511f, -913.182f, 19.216f));
                            floatStoredistance15 = World.GetDistance(PlayerCurrentPosition, new Vector3(544.962f, 2668.66f, 42.157f));
                            floatStoredistance16 = World.GetDistance(PlayerCurrentPosition, new Vector3(2678.268f, 3284.534f, 55.241f));
                            floatStoredistance17 = World.GetDistance(PlayerCurrentPosition, new Vector3(1963.079f, 3744.127f, 32.344f));
                            floatStoredistance18 = World.GetDistance(PlayerCurrentPosition, new Vector3(1702.28f, 4927.774f, 42.064f));
                            floatStoredistance19 = World.GetDistance(PlayerCurrentPosition, new Vector3(1733.161f, 6415.16f, 35.037f));
                            floatStoredistance20 = World.GetDistance(PlayerCurrentPosition, new Vector3(446.018f, -1242.455f, 30.286f));

                            Vector3[] allshopslocations = new Vector3[]
                            {
                                new Vector3(377.999f, 326.656f, 103.566f),
                                new Vector3(1137.728f, -981.551f, 46.416f),
                                new Vector3(-1223.76f, -905.604f, 12.326f),
                                new Vector3(-1488.766f, -380.33f, 40.163f),
                                new Vector3(-2970.112f, 390.817f, 15.043f),
                                new Vector3(1165.886f, 2706.462f, 38.158f),
                                new Vector3(29.839f, -1345.9f, 29.497f),
                                new Vector3(-51.545f, -1753.753f, 29.421f),
                                new Vector3(1157.933f, -323.593f, 69.205f),
                                new Vector3(2555.16f, 385.334f, 108.623f),
                                new Vector3(-1824.991f, 790.218f, 138.211f),
                                new Vector3(-3042.18f, 588.288f, 7.909f),
                                new Vector3(-3243.737f, 1004.556f, 12.831f),
                                new Vector3(-712.511f, -913.182f, 19.216f),
                                new Vector3(544.962f, 2668.66f, 42.157f),
                                new Vector3(2678.268f, 3284.534f, 55.241f),
                                new Vector3(1963.079f, 3744.127f, 32.344f),
                                new Vector3(1702.28f, 4927.774f, 42.064f),
                                new Vector3(1733.161f, 6415.16f, 35.037f),
                                new Vector3(446.018f, -1242.455f, 30.286f),
                            };
                            float[] allshopslocationsdistance = new float[] { floatStoredistance1, floatStoredistance2, floatStoredistance3, floatStoredistance4, floatStoredistance5, floatStoredistance6, floatStoredistance7, floatStoredistance8, floatStoredistance9, floatStoredistance10, floatStoredistance11, floatStoredistance12, floatStoredistance13, floatStoredistance14, floatStoredistance15, floatStoredistance16, floatStoredistance17, floatStoredistance18, floatStoredistance19, floatStoredistance20 };
                            for (int i = 0; i < allshopslocationsdistance.Length; i++)
                            {
                                if (allshopslocationsdistance.Min() == allshopslocationsdistance[i])
                                {
                                    UI.Notify("Nearest Store is " + allshopslocationsdistance[i].ToString() + " Meters Away");
                                    //UI.Notify(DictValue.ToString());
                                    Function.Call(Hash.SET_NEW_WAYPOINT, allshopslocations[i].X, allshopslocations[i].Y);
                                    Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                                    Function.Call(Hash.REFRESH_WAYPOINT);
                                }
                            }
                            Tools_Shop.nearestshopenabled = true;
                            Tools_Shop.ToolShopMarkeron = true;
                            Tools_Shop.MarkerDraw();
                            _menuPool.CloseAllMenus();
                        }
                        else
                        {
                            UI.ShowSubtitle("Active Job Found.! Either Complete the Assigned Job or Cancle it from the menu");
                        }
                        #endregion
                    }
                    else
                    {
                        UI.Notify("Activate the Mod from previous menu");
                    }
                }
                else if (item == SelectNearestVehicleShop)
                {
                    if (modactive)
                    {
                        #region Vehicle_Dealer_onItemSelect
                        if (CurrentJobPlayer.Count == 0)
                        {
                            Vector3 PlayerCurrentPosition = GTA.Game.Player.Character.Position;
                            Vector3 VehicleDealerShopLocation = new Vector3(-40.87683f, -1112.449f, 26.43635f);
                            float VehicleStoredistance = World.GetDistance(PlayerCurrentPosition, VehicleDealerShopLocation);
                            UI.Notify("Vehicle Dealer is " + VehicleStoredistance.ToString() + " Meters Away");
                            if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                            {
                                Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                            }
                            Function.Call(Hash.SET_NEW_WAYPOINT, VehicleDealerShopLocation.X, VehicleDealerShopLocation.Y);
                            Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                            Function.Call(Hash.REFRESH_WAYPOINT);

                            Vehicel_Shop_Dealer.ShopSelected = true;
                            _menuPool.CloseAllMenus();                          
                            //Tools_Shop.MarkerDraw();
                            //_menuPool.CloseAllMenus();
                        }
                        else
                        {
                            UI.ShowSubtitle("Active Job Found.! Either Complete the Assigned Job or Cancle it from the menu");
                        }
                        #endregion
                    }
                    else
                    {
                        UI.Notify("Activate the Mod from previous menu");
                    }
                }
                else if (item == SelectNearestLandDealer)
                {
                    if (modactive)
                    {
                        #region Land_Dealer_onItemSelect
                        if (CurrentJobPlayer.Count == 0)
                        {   
                            Vector3 PlayerCurrentPosition = GTA.Game.Player.Character.Position;
                            Vector3 LandDealerOfficeLocation = new Vector3(-67.303f, -801.868f, 44.227f);
                            float LandDealerOfficedistance = World.GetDistance(PlayerCurrentPosition, LandDealerOfficeLocation);
                            UI.Notify("Land Dealer is " + LandDealerOfficedistance.ToString() + " Meters Away");
                            if (Function.Call<bool>(Hash.IS_WAYPOINT_ACTIVE))
                            {
                                Function.Call(Hash.IS_WAYPOINT_ACTIVE, false);
                            }
                            Function.Call(Hash.SET_NEW_WAYPOINT, LandDealerOfficeLocation.X, LandDealerOfficeLocation.Y);
                            Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                            Function.Call(Hash.REFRESH_WAYPOINT);
                            Land_Merchant.ShopSelected = true;
                            _menuPool.CloseAllMenus();
                            
                        }
                        else
                        {
                            UI.ShowSubtitle("Active Job Found.! Either Complete the Assigned Job or Cancle it from the menu");
                        }
                        #endregion
                    }
                    else
                    {
                        UI.Notify("Activate the Mod from previous menu");
                    }
                }
            };
        }

        private string MenuItemName;
        public void OptionMenu(UIMenu menu)
        {
            UIMenu submenu = _menuPool.AddSubMenu(menu, "Options Menu");
            List<dynamic> templistconversion2 = new List<dynamic>();

            List<AllPositionsListedEnums> tempholdingthis = Enum.GetValues(typeof(AllPositionsListedEnums)).Cast<AllPositionsListedEnums>().ToList();

            foreach (var item in tempholdingthis)
            {
                templistconversion2.Add(item);
            }
            
            UIMenuListItem DrivingLocationList = new UIMenuListItem("Driving Locations : ", templistconversion2, 0);

            for (int j = 0; j < 1; j++)
            {
                //Created by
                submenu.AddItem(new UIMenuItem("Created By : " + "~g~Ashish R. aka ~r~YCSM!!!"));
                //Mod Date
                submenu.AddItem(new UIMenuItem("Current Mod Version" + Class1.ScriptVersion.ToString()));
                DisplayCompititorStat = new UIMenuCheckboxItem("Display Compititor Stat", Compititor_Stat, "Check to Display Compititor Stat");
                submenu.AddItem(DisplayCompititorStat);
                DisplayCompititorStat.Checked = true;
                submenu.AddItem(InformatioAboutMod = new UIMenuItem("Mod Tour", "This will give the mod tour to user, How to use the mod and what it does."));
                submenu.AddItem(CheatMenu = new UIMenuItem("~b~Cheat ~b~Codes", "~g~Psst! you can add the cheat codes here for quick boost."));
                submenu.AddItem(ResourceZone = new UIMenuItem("Add Current Location to Resource Zone", "This will add the current position of player to Resource Zone, Job Resources can be spawned here"));
                submenu.AddItem(DrivingLocationList);
                submenu.AddItem(DrivingZone = new UIMenuItem("Add Current Location to Driving Zone", "This will add the current position of player to Driving Zone, Gathered Resources can be Delivered here"));

            }

            submenu.OnListChange += (sender, item, index) =>
            {
                if (item == DrivingLocationList)
                {
                    for (int i = 0; i < templistconversion2.Count; i++)
                    {
                        if (i == DrivingLocationList.Index)
                        {
                            //UI.ShowSubtitle("\"" + DrivingLocationList.IndexToItem(i) + "\" is selected."); //THIS IS WORKING
                            int value = i + 1;
                            AllPositionsListedEnums enumDisplayStatus = (AllPositionsListedEnums)value;
                            stringValue = enumDisplayStatus.ToString();

                            //UI.Notify(stringValue);


                            //MenuItemName = DrivingLocationList.IndexToItem(i);

                        }
                    }

                }
            };
            submenu.OnItemSelect += (sender, item, index) =>
            {
                if (item == CheatMenu)
                {
                    string input = Game.GetUserInput(255);

                    if (input == "show me the money")
                    {
                        Class1.XPPoints.PlayerXP += 10000;
                        Class1.XPPoints.PlayerMoney += 50000;
                        AvailableResources.ResourcesNumbers.ChoppedWoodQuantity += 100;
                        AvailableResources.ResourcesNumbers.StoneQuantity += 100;
                        AvailableResources.ResourcesNumbers.RawSteelQuantity += 100;                        
                        NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Cheat Code Accepted", "XP, Money and All stocks are added by 100.", HudColor.HUD_COLOUR_RED, HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                        GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                        
                    }
                    else
                    {
                        UI.ShowSubtitle("Incorrect Cheat Code Entered.");
                        UI.Notify("Psst.! You can contact Developer of this mod for cheat codes");
                        GTA.Game.PlaySound("Pre_Screen_Stinger", "DLC_HEISTS_FAILED_SCREEN_SOUNDS");
                    }
                }

                if (item == InformatioAboutMod)
                {
                    //Camera tempcamera;
                    //tempcamera = World.CreateCamera(GTA.Game.Player.Character.Position, Vector3.Zero, 75f);
                    //Vector3 Pedpositioncurrent11 = new Vector3(tempcamera.Position.X, tempcamera.Position.Y + 3f, tempcamera.Position.Z + 3f);

                    GTA.Native.Function.Call(Hash.DO_SCREEN_FADE_OUT, 1000);
                    //Wait(3000);
                    modtourdisplay = true;
                    _menuPool.CloseAllMenus();

                }


                if (item == ResourceZone)
                {
                    Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                    if (AllPositionsListed.ZonePositionList.Contains(PlayerCurrentPosition))
                    {
                        UI.Notify("Position is already saved!");
                    }
                    else
                    {
                        AllPositionsListed.ZonePositionList.Add(PlayerCurrentPosition);
                        UI.Notify("Zone Added Sucessfully!");
                        XML_Import_Export.XMLSaving();
                    }
                }

                if (item == DrivingZone)
                {

                    if (stringValue == AllPositionsListedEnums.WoodDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.WoodDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.WoodDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Wood List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.StoneDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.StoneDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.StoneDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Stone List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.SteelDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.SteelDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.SteelDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Steel List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.CopperDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.CopperDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.CopperDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Copper List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.WheatDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.WheatCropsDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.WheatCropsDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Wheat List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.RiceDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.RiceCropsDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.RiceCropsDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Rice List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.BeetRootDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.BeetRootCropsDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.BeetRootCropsDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to BeetRoot List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.WeedDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.WeedCropsDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.WeedCropsDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Weed List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.MilkDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.MilkDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.MilkDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Milk List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.CheeseDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.CheeseDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.CheeseDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Cheese List");
                        }
                    }
                    else if (stringValue == AllPositionsListedEnums.PorkDelivery.ToString())
                    {
                        Vector3 PlayerCurrentPosition = new Vector3(GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z);
                        if (AllPositionsListed.PorkDrivingZonePositionList.Contains(PlayerCurrentPosition))
                        {
                            UI.Notify("Position is already saved!");
                        }
                        else
                        {
                            AllPositionsListed.PorkDrivingZonePositionList.Add(PlayerCurrentPosition);
                            UI.Notify("Driving Zone Added Sucessfully to Pork List");
                        }
                    }

                    XML_Import_Export.XMLSaving();
                }
            };
        }

        #endregion


        #region Driving_Menu_Options
        public void DrivingMenu1(UIMenu menu)
        {
          
            InventoryList = new List<dynamic>()
            {
                //Raw Resources Job Queue
                "Chooped Wood : " + AvailableResources.ResourcesNumbers.ChoppedWoodQuantity.ToString(),                
            };

            List<dynamic> listitems2 = new List<dynamic>()
            {
                //Raw Resources Job Queue
                AvailableResources.ResourcesNumbers.ParentResourceQuantity,
                AvailableResources.ResourcesNumbers.ChoppedWoodQuantity,
                AvailableResources.ResourcesNumbers.StoneQuantity,
                AvailableResources.ResourcesNumbers.RawSteelQuantity,
                AvailableResources.ResourcesNumbers.CopperQuantity,

                //Harvesting Job Queue                
                AvailableResources.ResourcesNumbers.WheatQuantity,
                AvailableResources.ResourcesNumbers.RiceQuantity,
                AvailableResources.ResourcesNumbers.BeetRootQuantity,
                AvailableResources.ResourcesNumbers.WeedQuantity,
                
                //Cattling Job Queue
                AvailableResources.ResourcesNumbers.CowMilkQuantity,
                AvailableResources.ResourcesNumbers.CheeseQuantity,
                AvailableResources.ResourcesNumbers.PigMeatQuantity,
                AvailableResources.ResourcesNumbers.CowDunkQuantity,

            };

            InventoryListSection = new UIMenuListItem("~y~Driving ~g~Jobs : ", InventoryList, 0);
            //UIMenuListItem listsection2 = new UIMenuListItem("Quantity : ", listitems2, 0);

            UIMenu submenu1 = _menuPool.AddSubMenu(menu, "Available Driving Jobs");

            for (int i = 0; i < 1; i++)
            {
                submenu1.AddItem(InventoryListSection);
                submenu1.AddItem(InventoryNumbers);
            }

            submenu1.OnListChange += (sender, item, index) =>
            {
                if (item == InventoryListSection)
                {            

                }
            };
        }
        #endregion


        private void AreaandWayPointSet()
        {
            //Blip area zone needs to be set here from the list of the vector 3 positions either hardcoded or softcoded
            //Then call this method where the status reads, Job has been assigned to player
            if (AllPositionsListed.ZonePositionList.Count > 0)
            {
                Random ask = new Random();

                int randomnumber = ask.Next(AllPositionsListed.ZonePositionList.Count);
                ZoneBlip = GTA.World.CreateBlip(AllPositionsListed.ZonePositionList[randomnumber]);
                ZoneBlip.Scale = 10;
                ZoneBlip.Alpha = 80;
                ZoneBlip.IsShortRange = true;
                ZoneBlip.Color = BlipColor.Green;
                ZoneBlip.IsFriendly = true;
                ZoneBlip.HideNumber();
                ResourcesZoneActive = true;
                UI.Notify("Resources Can be found in the allocated zone");

                Function.Call(Hash.SET_NEW_WAYPOINT, ZoneBlip.Position.X, ZoneBlip.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);
            }
            else
            {
                ZoneBlip = GTA.World.CreateBlip(new Vector3(-1375.717f, 4422.273f, 29.050f));
                ZoneBlip.Scale = 10;
                ZoneBlip.Alpha = 80;
                ZoneBlip.IsShortRange = true;
                ZoneBlip.Color = BlipColor.Green;
                ZoneBlip.IsFriendly = true;
                ZoneBlip.HideNumber();
                ResourcesZoneActive = true;
                UI.Notify("Resources Can be found in the allocated zone");
                Function.Call(Hash.SET_NEW_WAYPOINT, ZoneBlip.Position.X, ZoneBlip.Position.Y);
                Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);
                Function.Call(Hash.REFRESH_WAYPOINT);
            }

        }

    }//Main Class ends here

}//NameSpace Ends here
