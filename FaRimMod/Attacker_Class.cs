﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GTA;
using GTA.Native;
using GTA.Math;
using System.Windows.Forms;

namespace FaRimMod
{
    class Attacker_Class : Script
    {
        Ped Playerped;
        Ped[] Attackers = new Ped[05];
        Vehicle[] AttackersVehicle = new Vehicle[05];
        Vector3 tempposition;

        private bool AttackerSpawned, RandomCounterRun;
        int AttackerCount, AttackerTargetCount, RandomCount, Enemy_RelationshipGroup, Target_Timer, 
            RandomTrigger_SpawnEnemy;

        public static bool JobCancled, Trucking_Mission;

        public static void Staic_Method()
        {
            Attacker_Class temp = new Attacker_Class();
            //temp.EnemyRemove();
        }


        public Attacker_Class()
        {
            Initialize();            
            Tick += onTick;            
            KeyUp += Attacker_Class_KeyUp;
            Interval = 5000;
        }//Default Constructor Ends here

        private void Attacker_Class_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.O)
            {
                
            }


            if (e.KeyCode == Keys.K)
            {
                
            }
        }

        private void onTick(object sender, EventArgs e)
        {
            if (MenuUI.modactive)
            {
                if (MenuUI.CurrentJobPlayer.Count > 0)
                {
                    if (Class1.XPPoints.PlayerXP >= 300)
                    {
                        if (!AttackerSpawned)
                        {  
                            if (Trucking_Mission)
                            {
                                if (Delivery_Jobs.DestinationSet)
                                {
                                    if (!RandomCounterRun)
                                    {
                                        RandomCounter();
                                    }

                                    if (Target_Timer < Game.GameTime)
                                    {
                                        tempposition = World.GetNextPositionOnStreet(Playerped.Position.Around(150f));
                                        AttackerTargetCount = RandomCount;
                                        if (MenuUI.JobCriticality < 3)
                                        {
                                            if (Playerped.IsOnFoot)
                                            {
                                                if (AttackerCount < AttackerTargetCount)
                                                {
                                                    EasyAttacker();
                                                }
                                            }
                                        }
                                        else if (MenuUI.JobCriticality >= 3 && MenuUI.JobCriticality < 6)
                                        {
                                            if (Playerped.IsAlive)
                                            {
                                                if (AttackerCount < AttackerTargetCount)
                                                {
                                                    MediumAttacker();
                                                }
                                            }
                                        }
                                        else if (MenuUI.JobCriticality >= 6)
                                        {
                                            if (Playerped.IsAlive)
                                            {
                                                if (AttackerCount < AttackerTargetCount)
                                                {
                                                    HardAttacker();
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                            }
                            else
                            {
                                if (AttackerCount == 0)
                                {
                                    if (!RandomCounterRun)
                                    {
                                        RandomCounter();
                                    }                                    
                                }
                                
                                if (Target_Timer < Game.GameTime)
                                {   
                                    AttackerTargetCount = RandomCount;
                                    if (MenuUI.JobCriticality < 10)
                                    {
                                        if (Playerped.IsAlive)
                                        {
                                            if (Playerped.IsOnFoot)
                                            {
                                                if (AttackerCount < AttackerTargetCount)
                                                {
                                                    if (JobCreation.bardisplayed)
                                                    {
                                                        EasyAttacker();
                                                    }                                                    
                                                }
                                            }
                                        }
                                    }                                   
                                }
                            }
                           
                        }

                        if (AttackerSpawned)
                        {
                            if (AttackerCount > 0)
                            {
                                if (Trucking_Mission)
                                {
                                    ChasePlayeronFoot(Playerped);
                                }
                                EnemyRemove();
                            }
                        }
                    }
                }
                else
                {
                    if (AttackerSpawned)
                    {
                        CancleJobEnemyRemove();
                    }
                    AttackerSpawned = false;
                    RandomCounterRun = false;
                    JobCreation.bardisplayed = false;
                    AttackerCount = 0;
                }
                
            }
        }


        private void ChasePlayeronFoot(Ped PedObject)
        {
            for (int i = 0; i < AttackerCount; i++)
            {
                if (Attackers[i] != null)
                {
                    if (Attackers[i].IsAlive)
                    {
                        if (!Attackers[i].IsOnFoot)
                        {
                            if (Attackers[i].CurrentVehicle != null)
                            {
                                if (Attackers[i].CurrentVehicle.IsDriveable)
                                {
                                    if (PedObject.IsOnFoot)
                                    {
                                        Attackers[i].Task.DriveTo(Attackers[i].CurrentVehicle, Playerped.Position.Around(4f), 5f, 30f);
                                        Wait(400);
                                        Attackers[i].Task.VehicleChase(PedObject);
                                        Function.Call(Hash.SET_DRIVEBY_TASK_TARGET, Attackers[i], PedObject, PedObject.CurrentVehicle, PedObject.Position.X, PedObject.Position.Y, PedObject.Position.Z);
                                    }
                                    else
                                    {
                                        Attackers[i].Task.DriveTo(Attackers[i].CurrentVehicle, Playerped.Position.Around(4f), 5f, 30f);
                                        Wait(400);
                                        Attackers[i].Task.VehicleChase(PedObject);
                                        Function.Call(Hash.SET_DRIVEBY_TASK_TARGET, Attackers[i], PedObject, PedObject.CurrentVehicle, PedObject.Position.X, PedObject.Position.Y, PedObject.Position.Z);
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void CancleJobEnemyRemove()
        {
            
            for (int i = 0; i < AttackerCount; i++)
            {
                Wait(500);
                if (Attackers[i] != null)
                {
                    Attackers[i].Delete();
                    Attackers[i] = null;
                    if (AttackersVehicle[i] != null)
                    {
                        AttackersVehicle[i].Delete();
                        AttackersVehicle[i] = null;
                    }
                    
                }
            }
            JobCancled = false;
            Trucking_Mission = false;
        }
        private void EnemyRemove()
        {
            if (Playerped.IsDead)
            {
                for (int i = 0; i < AttackerCount; i++)
                {
                    if (Attackers[i] != null)
                    {
                        if (Attackers[i].IsDead)
                        {
                            Attackers[i].Delete();
                            Attackers[i] = null;
                            if (AttackersVehicle[i] != null)
                            {
                                AttackersVehicle[i].Delete();
                                AttackersVehicle[i] = null;                                
                            }                            
                        }
                        else
                        {
                            Attackers[i].Delete();
                            Attackers[i] = null;
                            if (AttackersVehicle[i] != null)
                            {
                                AttackersVehicle[i].Delete();
                                AttackersVehicle[i] = null;                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < AttackerCount; i++)
                {
                    if (Attackers[i] != null)
                    {
                        if (Attackers[i].IsDead)
                        {
                            Attackers[i].Delete();
                            Attackers[i] = null;
                            if (AttackersVehicle[i] != null)
                            {
                                AttackersVehicle[i].Delete();
                                AttackersVehicle[i] = null;                                
                            }
                            
                        }
                        else
                        {
                            if (World.GetDistance(Playerped.Position, Attackers[i].Position) >= 300f)
                            {
                                if (Attackers[i].IsIdle)
                                {
                                    Attackers[i].Delete();
                                    Attackers[i] = null;
                                    if (AttackersVehicle[i] != null)
                                    {
                                        AttackersVehicle[i].Delete();
                                        AttackersVehicle[i] = null;
                                    }
                                    
                                }
                            }
                        }                                         
                    }                    
                }              
            }
        }

        private int RandomCounter()
        {
            Random rnd = new Random();
            RandomTrigger_SpawnEnemy = rnd.Next(1, 4);

            if (RandomTrigger_SpawnEnemy <= 3)
            {
                //if (MenuUI.JobCriticality <= 3)
                //{
                //    RandomCount = rnd.Next(1, 2);
                //    //RandomCount = rnd.Next(0, 4);
                //}
                //else if (MenuUI.JobCriticality > 3 && MenuUI.JobCriticality <= 5)
                //{
                //    RandomCount = rnd.Next(1, 2);
                //    //RandomCount = rnd.Next(0, 4);
                //}
                //else if (MenuUI.JobCriticality > 5 && MenuUI.JobCriticality <= 8)
                //{
                //    //RandomCount = rnd.Next(0, 4);
                //    RandomCount = rnd.Next(1, 2);
                //}

                if (Trucking_Mission)
                {
                    RandomCount = rnd.Next(1, 2);
                    Target_Timer = Game.GameTime + (RandomCount * 60000);
                }
                else
                {
                    RandomCount = rnd.Next(1, 6);
                    Target_Timer = Game.GameTime + (RandomCount * 5000);
                }
            }
            else
            {
                //DO NOTHING AS NO ENEMY WILL BE SPAWNED.
            }
            RandomCounterRun = true;
            return RandomCount;
        }


        private void Initialize()
        {
            Playerped = GTA.Game.Player.Character;

            Enemy_RelationshipGroup = World.AddRelationshipGroup("HatePlayer");
            World.SetRelationshipBetweenGroups(Relationship.Hate, Enemy_RelationshipGroup, Playerped.RelationshipGroup);
            //tempposition = World.GetNextPositionOnStreet(Playerped.Position.Around(150f));
        }

        #region Attaker_Definition
        private void EasyAttacker()
        {
            if (Playerped != null)
            {
                if (Playerped.IsAlive)
                {
                    if (Playerped.IsOnFoot)
                    {
                        for (int i = 0; i < AttackerTargetCount; i++)
                        {
                            Attackers[i] = World.CreateRandomPed(Playerped.Position.Around(30f));
                            Attackers[i].Task.FightAgainst(Playerped);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_BLOCKING_OF_NON_TEMPORARY_EVENTS, Attackers[i], 1);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_FLEE_ATTRIBUTES, Attackers[i], 0, 0);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_COMBAT_ATTRIBUTES, Attackers[i], 46, 1);
                            Attackers[i].RelationshipGroup = Enemy_RelationshipGroup;
                            Attackers[i].NeverLeavesGroup = true;
                            Attackers[i].Weapons.Give(WeaponHash.Dagger, 0, true, true);
                            Attackers[i].DropsWeaponsOnDeath = false;
                            Attackers[i].AlwaysKeepTask = true;
                            Attackers[i].Accuracy = 20;
                            Attackers[i].MaxHealth = 100;
                            Blip AttackerBlip = Attackers[i].AddBlip();
                            AttackerBlip.Color = BlipColor.Red;
                            AttackerBlip.Scale = 0.7f;
                            AttackerBlip.Name = "Attacker " + i;                            
                            AttackerCount++;
                            AttackerSpawned = true;
                        }
                    }                  
                }
            }         
        }


        private void MediumAttacker()
        {
            if (Playerped != null)
            {
                if (Playerped.IsAlive)
                {
                    if (Playerped.IsOnFoot)
                    {
                        for (int i = 0; i < AttackerTargetCount; i++)
                        {
                            Attackers[i] = World.CreateRandomPed(Playerped.Position.Around(10f));
                            Attackers[i].Task.FightAgainst(Playerped);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_BLOCKING_OF_NON_TEMPORARY_EVENTS, Attackers[i], 1);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_FLEE_ATTRIBUTES, Attackers[i], 0, 0);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_COMBAT_ATTRIBUTES, Attackers[i], 46, 1);
                            Attackers[i].RelationshipGroup = Enemy_RelationshipGroup;
                            Attackers[i].NeverLeavesGroup = true;
                            Attackers[i].Weapons.Give(WeaponHash.APPistol, 1000, true, true);
                            Attackers[i].DropsWeaponsOnDeath = false;
                            Attackers[i].AlwaysKeepTask = true;
                            Attackers[i].Accuracy = 50;
                            Attackers[i].Armor = 100;
                            Attackers[i].MaxHealth = 200;
                            Blip AttackerBlip = Attackers[i].AddBlip();
                            AttackerBlip.Color = BlipColor.Red;
                            AttackerBlip.Scale = 0.7f;
                            AttackerBlip.Name = "Attacker Medium " + i;                            
                            AttackerCount++;
                            AttackerSpawned = true;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < AttackerTargetCount; i++)
                        {
                            
                            AttackersVehicle[i] = World.CreateVehicle(new Model(VehicleHash.Panto), tempposition);                            
                            Wait(200);
                            Attackers[i] = World.CreateRandomPed(AttackersVehicle[i].Position.Around(05f));
                            Attackers[i].Weapons.Give(WeaponHash.APPistol, 1000, true, true);
                            Attackers[i].DropsWeaponsOnDeath = false;
                            Attackers[i].AlwaysKeepTask = true;
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_BLOCKING_OF_NON_TEMPORARY_EVENTS, Attackers[i], 1);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_FLEE_ATTRIBUTES, Attackers[i], 0, 0);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_COMBAT_ATTRIBUTES, Attackers[i], 46, 1);
                            Wait(400);
                            GTA.Native.Function.Call(GTA.Native.Hash.TASK_WARP_PED_INTO_VEHICLE, Attackers[i], AttackersVehicle[i], -1);                            
                            Attackers[i].Task.VehicleChase(Playerped);
                            Function.Call(Hash.SET_DRIVEBY_TASK_TARGET, Attackers[i], Playerped, Playerped.CurrentVehicle, Playerped.Position.X, Playerped.Position.Y, Playerped.Position.Z);
                            Attackers[i].RelationshipGroup = Enemy_RelationshipGroup;
                            Attackers[i].NeverLeavesGroup = true;                                                        
                            Attackers[i].Accuracy = 50;
                            Attackers[i].Armor = 100;
                            Attackers[i].MaxHealth = 200;
                            Attackers[i].CanBeKnockedOffBike = false;
                            Blip AttackerBlip = Attackers[i].AddBlip();
                            AttackerBlip.Color = BlipColor.Red;
                            AttackerBlip.Scale = 0.7f;
                            AttackerBlip.Name = "Attacker Medium " + i;
                            AttackersVehicle[i].IsInvincible = true;
                            AttackerCount++;
                            AttackerSpawned = true;
                        }
                    }
                }
            }
        }


        private void HardAttacker()
        {
            if (Playerped != null)
            {
                if (Playerped.IsAlive)
                {
                    if (Playerped.IsOnFoot)
                    {
                        for (int i = 0; i < AttackerTargetCount; i++)
                        {
                            Attackers[i] = World.CreateRandomPed(Playerped.Position.Around(10f));
                            Attackers[i].Task.FightAgainst(Playerped);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_BLOCKING_OF_NON_TEMPORARY_EVENTS, Attackers[i], 1);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_FLEE_ATTRIBUTES, Attackers[i], 0, 0);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_COMBAT_ATTRIBUTES, Attackers[i], 46, 1);
                            Attackers[i].RelationshipGroup = Enemy_RelationshipGroup;
                            Attackers[i].NeverLeavesGroup = true;
                            Attackers[i].Weapons.Give(WeaponHash.AdvancedRifle, 5000, true, true);
                            Attackers[i].DropsWeaponsOnDeath = false;
                            Attackers[i].AlwaysKeepTask = true;
                            Attackers[i].Accuracy = 90;
                            Attackers[i].Armor = 200;
                            Attackers[i].MaxHealth = 250;                            
                            Blip AttackerBlip = Attackers[i].AddBlip();
                            AttackerBlip.Color = BlipColor.Red;
                            AttackerBlip.Scale = 0.7f;
                            AttackerBlip.Name = "Attacker Hard " + i;
                            AttackerCount++;
                            AttackerSpawned = true;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < AttackerTargetCount; i++)
                        {   
                            AttackersVehicle[i] = World.CreateVehicle(new Model(VehicleHash.Panto), tempposition);                            
                            Wait(200);
                            Attackers[i] = World.CreateRandomPed(AttackersVehicle[i].Position.Around(05f));
                            Attackers[i].Weapons.Give(WeaponHash.AdvancedRifle, 5000, true, true);
                            Attackers[i].DropsWeaponsOnDeath = false;
                            Attackers[i].AlwaysKeepTask = true;
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_BLOCKING_OF_NON_TEMPORARY_EVENTS, Attackers[i], 1);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_FLEE_ATTRIBUTES, Attackers[i], 0, 0);
                            GTA.Native.Function.Call(GTA.Native.Hash.SET_PED_COMBAT_ATTRIBUTES, Attackers[i], 46, 1);
                            Wait(400);
                            GTA.Native.Function.Call(GTA.Native.Hash.TASK_WARP_PED_INTO_VEHICLE, Attackers[i], AttackersVehicle[i], -1);
                            Attackers[i].Task.VehicleChase(Playerped);
                            Function.Call(Hash.SET_DRIVEBY_TASK_TARGET, Attackers[i], Playerped, Playerped.CurrentVehicle, Playerped.Position.X, Playerped.Position.Y, Playerped.Position.Z);
                            Attackers[i].RelationshipGroup = Enemy_RelationshipGroup;
                            Attackers[i].NeverLeavesGroup = true;
                            Attackers[i].Accuracy = 90;
                            Attackers[i].Armor = 200;
                            Attackers[i].MaxHealth = 250;
                            Attackers[i].CanBeKnockedOffBike = false;
                            Blip AttackerBlip = Attackers[i].AddBlip();
                            AttackerBlip.Color = BlipColor.Red;
                            AttackerBlip.Scale = 0.7f;
                            AttackerBlip.Name = "Attacker Medium " + i;
                            AttackersVehicle[i].IsInvincible = true;
                            AttackerCount++;
                            AttackerSpawned = true;
                        }
                    }
                }
            }
        }
        #endregion
      
        
    }//Main Class ends here


}//Namespace Ends here
