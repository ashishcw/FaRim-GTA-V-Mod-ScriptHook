﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Math;
using System.Drawing;
using System.Windows.Forms;
using SimpleUI;
using GTA.Native;

namespace FaRimMod
{
    class Vehicel_Shop_Dealer : Script
    {
        public static bool ShopSelected, MarkerOn, DisplayVehicleMenu, camSet, 
                           Tractor_Cultivator_Purchased, Truck_Purchased, Mini_TruckPurchased;

        private SimpleUI.MenuPool VehicleShop_menuPool;
        private SimpleUI.UIMenu mainMenu2;

        private SimpleUI.UIMenuItem PriceItem, DoneItem, CancleItem;
        private SimpleUI.UIMenuListItem VehicleList;
        private SimpleUI.UIMenuNumberValueItem itemIntegerControl;

        private GTA.Camera tempcamera;

        private Vehicle tempVehicle, tempVehicle2;

        public static Vehicle PlayerTruck, PlayerCargoMiniTruck, PlayerTractor, PlayerCultivator;

        Blip Truckblip;

        GTA.Ped Player;

        Vector3 StorePosition = new Vector3(-40.87683f, -1112.449f, 26.43635f);
        float DistanceRemaining;

        private int RotateValue, VehiclePrice;
        

        public Vehicel_Shop_Dealer()
        {
            Player = GTA.Game.Player.Character;
            Tick += onTick;
            KeyDown += keyDown;
            MenuIntializer();
        }//Default Constructor Ends here


        void MenuIntializer()
        {
            VehicleShop_menuPool = new SimpleUI.MenuPool();

            mainMenu2 = new SimpleUI.UIMenu("~g~Vehicle ~y~Menu");
            VehicleShop_menuPool.AddMenu(mainMenu2);

            //itemEnumControl = new SimpleUI.UIMenuNumberValueItem("Available Tools : ", testEnum, "Select the Tool to Purchase");
            //mainMenu2.AddMenuItem(itemEnumControl);
            List<dynamic> FarmingVehiclesandEquipments = new List<dynamic>()
            {
                "Tractor & Cultivator",
                //"Cultivator",
                //"Seeder",//1783355638
                "Goods Delivery Truck",
                "Cargo Truck"
            };            

            VehicleList = new SimpleUI.UIMenuListItem("Available Vehicles : ", "This shows Vehicle List which are needed to perform the job", FarmingVehiclesandEquipments);
            mainMenu2.AddMenuItem(VehicleList);

            itemIntegerControl = new UIMenuNumberValueItem("Rotate : ", RotateValue, "Here you can rotate the ped.");
            mainMenu2.AddMenuItem(itemIntegerControl);

            PriceItem = new SimpleUI.UIMenuItem("Price : $" + VehiclePrice, null, "Price of the Product");
            mainMenu2.AddMenuItem(PriceItem);

            DoneItem = new SimpleUI.UIMenuItem("Done", null, "Done to finalize the Purchase");
            mainMenu2.AddMenuItem(DoneItem);

            CancleItem = new SimpleUI.UIMenuItem("Cancle", null, "Cancle and close this menu");
            mainMenu2.AddMenuItem(CancleItem);

            mainMenu2.OnItemLeftRight += MainMenu_OnItemLeftRight;
            mainMenu2.OnItemSelect += MainMenu_OnItemSelect;
        }

        private void SetupCamera()
        {
            if (camSet)
            {
                //tempcamera = World.CreateCamera(new Vector3(-849.953f, 74.509f, 51.930f), Vector3.Zero, 75f);
                if (tempcamera == null)
                {
                    tempcamera = World.CreateCamera(new Vector3(-53.833f, -1117.318f, 26.434f), new Vector3(-10f, 0f, -02f), 75f);
                    tempcamera.Position = new Vector3(tempcamera.Position.X, tempcamera.Position.Y, tempcamera.Position.Z + 0.7f);
                    World.RenderingCamera = tempcamera;
                }
            }

        }//camera function ends here


        private void MainMenu_OnItemSelect(UIMenu sender, UIMenuItem selectedItem, int index)
        {
            if (selectedItem == DoneItem)
            {
                if (Land_Merchant.Land1Purchased)
                {
                    if (Class1.XPPoints.PlayerMoney >= VehiclePrice)
                    {
                        if (VehicleList.SelectedIndex == 0)
                        {
                            if (PlayerTractor == null)
                            {
                                PlayerTractor = tempVehicle;
                                PlayerCultivator = tempVehicle2;
                                NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Purchase Sucessful", VehicleList.CurrentListItem + " Purchased ", NativeUI.HudColor.HUD_COLOUR_BLACK, NativeUI.HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                                GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                                Tractor_Cultivator_Purchased = true;
                                GTA.Blip TractorBlip = Vehicel_Shop_Dealer.PlayerTractor.AddBlip();
                                TractorBlip.Sprite = BlipSprite.Minigun;
                                TractorBlip.Name = "Tractor";
                                DoneFunction();
                            }
                        }
                        //else if (VehicleList.SelectedIndex == 1)
                        //{
                        //    if (PlayerCultivator == null)
                        //    {
                        //        //tempVehicle.Position = new Vector3(612.213f, 6484.969f, 29.707f);
                        //        PlayerCultivator = tempVehicle;
                        //        PlayerCultivator.Position = new Vector3(125.947f, -1068.855f, 29.192f);
                        //        //if (Game.IsWaypointActive)
                        //        //{
                        //        //    Function.Call(Hash.SET_WAYPOINT_OFF);
                        //        //}
                        //        //Function.Call(Hash.SET_NEW_WAYPOINT, tempVehicle.Position.X, tempVehicle.Position.Y);
                        //        //Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);                    
                        //        //Function.Call(Hash.REFRESH_WAYPOINT);
                        //        NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Purchase Sucessful", VehicleList.CurrentListItem + " Purchased ", NativeUI.HudColor.HUD_COLOUR_BLACK, NativeUI.HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                        //        GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");                            
                        //        DoneFunction();
                        //    }
                        //}
                        else if (VehicleList.SelectedIndex == 1)
                        {
                            if (PlayerTruck == null)
                            {
                                PlayerTruck = tempVehicle;
                                //PlayerTruck.Position = new Vector3(125.947f, -1068.855f, 29.192f);

                                //Truckblip = PlayerTruck.AddBlip();
                                //Truckblip.Sprite = BlipSprite.GarbageTruck;


                                NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Purchase Sucessful", VehicleList.CurrentListItem + " Purchased ", NativeUI.HudColor.HUD_COLOUR_BLACK, NativeUI.HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                                GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                                Truck_Purchased = true;
                                GTA.Blip TruckBlip = Vehicel_Shop_Dealer.PlayerTruck.AddBlip();
                                TruckBlip.Sprite = BlipSprite.GarbageTruck;
                                TruckBlip.Name = "Cargo_Truck";
                                DoneFunction();
                            }
                        }
                        else if (VehicleList.SelectedIndex == 2)
                        {
                            if (PlayerCargoMiniTruck == null)
                            {
                                PlayerCargoMiniTruck = tempVehicle;
                                NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Purchase Sucessful", VehicleList.CurrentListItem + " Purchased. " + "Remaining Balance is : " + Class1.XPPoints.PlayerMoney, NativeUI.HudColor.HUD_COLOUR_BLACK, NativeUI.HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                                GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                                Mini_TruckPurchased = true;
                                GTA.Blip Mini_TruckBlip = Vehicel_Shop_Dealer.PlayerCargoMiniTruck.AddBlip();
                                Mini_TruckBlip.Sprite = BlipSprite.TowTruck;
                                Mini_TruckBlip.Name = "Cargo_Truck";
                                DoneFunction();
                            }
                        }
                        //if (VehicleList.SelectedIndex != 1)
                        //{
                        //    UI.Notify("Collect the Purchase vehicle from the Garage");
                        //}
                        //else
                        //{
                        //    UI.Notify("Equipment has been delivered to your farm");
                        //}

                        //if (Game.IsWaypointActive)
                        //{
                        //    Function.Call(Hash.SET_WAYPOINT_OFF);
                        //}
                        //Function.Call(Hash.SET_NEW_WAYPOINT, tempVehicle.Position.X, tempVehicle.Position.Y);
                        //Function.Call(Hash.IS_WAYPOINT_ACTIVE, true);                    
                        //Function.Call(Hash.REFRESH_WAYPOINT);

                    }
                    else
                    {
                        UI.ShowSubtitle("Transaction Denied, You do not have enough balance to purchase the vehicle.");
                        UI.Notify("You need more $" + (VehiclePrice - Class1.XPPoints.PlayerMoney) + " to make this purchase, Try performing lower jobs first");
                    }
                }
                else
                {
                    UI.ShowSubtitle("You will need to own the land before you can Purchase any vehicles.");
                    UI.Notify("Purchasing the Land will give you place to park the vehicles as well.");
                }
                
            }
            else if (selectedItem == CancleItem)
            {
                CancleFunction();
            }
        }

        private void MainMenu_OnItemLeftRight(UIMenu sender, UIMenuItem selectedItem, int index, bool left)
        {
            if (selectedItem == VehicleList)
            {
                if (VehicleList.SelectedIndex == 0)
                {
                    RotateValue = 0;
                    itemIntegerControl.Value = 0;
                    if (tempVehicle != null)
                    {
                        tempVehicle.Delete();
                        tempVehicle = null;                        
                    }

                    if (tempVehicle2 != null)
                    {
                        tempVehicle2.Delete();
                        tempVehicle2 = null;
                    }
                    tempVehicle = World.CreateVehicle(new Model(-2076478498), new Vector3(tempcamera.Position.X, tempcamera.Position.Y + 04f, tempcamera.Position.Z));
                    tempVehicle2 = World.CreateVehicle(new Model(390902130), new Vector3(tempcamera.Position.X, tempcamera.Position.Y + 04f, tempcamera.Position.Z));
                    if (!tempVehicle.IsAttachedTo(tempVehicle2))
                    {
                        Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, tempVehicle, tempVehicle2, 01f);
                    }                   
                    tempVehicle.Rotation = new Vector3(tempVehicle.Rotation.X, tempVehicle.Rotation.Y, RotateValue);
                    //tempVehicle2.Rotation = new Vector3(tempVehicle.Rotation.X, tempVehicle.Rotation.Y, tempVehicle.Rotation.Z);
                    VehiclePrice = 65770;
                    PriceItem.Text = "Price : $" + VehiclePrice;                    
                }
                //else if (VehicleList.SelectedIndex == 1)
                //{
                //    RotateValue = 0;
                //    itemIntegerControl.Value = 0;
                //    if (tempVehicle != null)
                //    {
                //        tempVehicle.Delete();
                //        tempVehicle = null;
                //    }
                //    tempVehicle = World.CreateVehicle(new Model(390902130), new Vector3(tempcamera.Position.X, tempcamera.Position.Y + 04f, tempcamera.Position.Z));
                //    tempVehicle.Rotation = new Vector3(tempVehicle.Rotation.X, tempVehicle.Rotation.Y, RotateValue);
                //    VehiclePrice = 12000;
                //    PriceItem.Text = "Price : $" + VehiclePrice;
                //}
                else if (VehicleList.SelectedIndex == 1)
                {
                    RotateValue = 0;
                    itemIntegerControl.Value = 0;
                    if (tempVehicle != null)
                    {
                        tempVehicle.Delete();
                        tempVehicle = null;
                    }
                    tempVehicle = World.CreateVehicle(new Model(-2137348917), new Vector3(tempcamera.Position.X, tempcamera.Position.Y + 07f, tempcamera.Position.Z));
                    tempVehicle.Rotation = new Vector3(tempVehicle.Rotation.X, tempVehicle.Rotation.Y, RotateValue);
                    VehiclePrice = 150000;
                    PriceItem.Text = "Price : $" + VehiclePrice;
                }
                else if (VehicleList.SelectedIndex == 2)
                {
                    RotateValue = 0;
                    itemIntegerControl.Value = 0;
                    if (tempVehicle != null)
                    {
                        tempVehicle.Delete();
                        tempVehicle = null;
                    }
                    tempVehicle = World.CreateVehicle(new Model(904750859), new Vector3(tempcamera.Position.X, tempcamera.Position.Y + 07f, tempcamera.Position.Z));
                    VehiclePrice = 50000;
                    PriceItem.Text = "Price : $" + VehiclePrice;

                }
            }
            else if (selectedItem == itemIntegerControl)
            {
                mainMenu2.ControlIntValue(ref RotateValue, itemIntegerControl, left, 10, 20, true, 0, 360);
                tempVehicle.Rotation = new Vector3(tempVehicle.Rotation.X, tempVehicle.Rotation.Y, RotateValue);
                if (tempVehicle != null)
                {
                    if (tempVehicle2 != null)
                    {
                        tempVehicle2.Rotation = new Vector3(tempVehicle.Rotation.X, tempVehicle.Rotation.Y, tempVehicle.Rotation.Z);
                        //Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, tempVehicle, tempVehicle2, 01f);                        
                        //if (!tempVehicle.IsAttachedTo(tempVehicle2))
                        //{
                        //    Function.Call(Hash.ATTACH_VEHICLE_TO_TRAILER, tempVehicle, tempVehicle2, 01f);
                        //}
                    }
                }
            }
            
        }

        private void keyDown(object sender, KeyEventArgs e)
        {   
            if (MenuUI.modactive)
            {
                float DistanceRemaining1 = World.GetDistance(Player.Position, StorePosition);
                if (Game.IsControlPressed(2, GTA.Control.VehicleHorn) && DistanceRemaining1 <= 01f)
                {   
                    var allvechicles = World.GetAllVehicles();

                    if (allvechicles.Length > 0)
                    {
                        foreach (var item in allvechicles)
                        {
                            if (World.GetDistance(item.Position, Player.Position) <= 20f)
                            {
                                item.Delete();
                            }
                        }
                    }
                    
                    camSet = true;                    
                    DisplayVehicleMenu = true;                    
                }
               
            }
        }


        private void DisplayVehicleShopMenu()
        {
            if (DisplayVehicleMenu)
            {
                if (!mainMenu2.IsVisible)
                {
                    mainMenu2.IsVisible = true;
                    MarkerOn = false;
                }
                
            }
        }

        private void onTick(object sender, EventArgs e)
        {
            if (MenuUI.modactive)
            {
                
                if (ShopSelected)
                {
                    VehicleShop_menuPool.ProcessMenus();
                    DistanceRemaining = World.GetDistance(Player.Position, StorePosition);

                    if (ShopSelected && DistanceRemaining <= 30f)
                    {
                        MarkerOn = true;
                        Class1.DisplayHelpTextThisFrame("Go to the Red Marker and Pres E");
                        MarkerDrawVehicleShop();
                        DisplayVehicleShopMenu();
                    }
                    else
                    {
                        DisplayVehicleMenu = false;
                    }


                    if (camSet && !mainMenu2.IsVisible)
                    {
                        CancleFunction();                        
                    }

                    if (MarkerOn)
                    {
                        if (DisplayVehicleMenu)
                        {
                            if (mainMenu2.IsVisible)
                            {
                                SetupCamera();
                                DisplayVehicleMenu = false;
                                MarkerOn = false;
                            }
                        }
                    }
                }
            }
        }


        private void DoneFunction()
        {
            if (tempcamera != null)
            {
                World.DestroyAllCameras();
                World.RenderingCamera = null;
                tempcamera = null;
                camSet = false;
            }

            if (mainMenu2.IsVisible)
            {
                mainMenu2.IsVisible = false;
            }
            DisplayVehicleMenu = false;
            //ShopSelected = false;
        }

        private void CancleFunction()
        {
            if (tempVehicle != null)
            {
                tempVehicle.Delete();
                tempVehicle = null;
            }
            if (tempcamera != null)
            {
                World.DestroyAllCameras();
                World.RenderingCamera = null;
                tempcamera = null;
                camSet = false;
            }

            if (mainMenu2.IsVisible)
            {
                mainMenu2.IsVisible = false;
            }            
            DisplayVehicleMenu = false;
            //ShopSelected = false;
        }


        private void MarkerDrawVehicleShop()
        {
            if (MarkerOn)
            {
                World.DrawMarker(MarkerType.VerticalCylinder, new Vector3(StorePosition.X, StorePosition.Y, StorePosition.Z - (0.9f + 0.1f)), Vector3.Zero, Vector3.Zero, new Vector3(0.90f, 0.90f, 0.90f), Color.Red);
            }
        }


    }//Main Class ends here

}//Namespace ends here
