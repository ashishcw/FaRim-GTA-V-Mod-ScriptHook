﻿using System;
using System.Collections.Generic;
using System.Text;
using GTA;
using GTA.Native;
using GTA.Math;
using System.Windows.Forms;
using NativeUI;
using System.Drawing;
using System.Timers;

namespace FaRimMod
{
    class Farming_Class : Script
    {
        public static bool farming, seedssowed;

        public static string TextBar, TextPercentageText, CropName = "";

        public static int CropQuantity;

        UIContainer container;
        UIText text, text2;

        int screen_height = Game.ScreenResolution.Height;
        int screen_width = Game.ScreenResolution.Width;
        
        const int PANEL_WIDTH = 250;        
        const int PANEL_HEIGHT = 55;

        Color backColor = Color.FromArgb(100, 255, 255, 255);
        Color textColor = Color.Red;
        Color textColor2 = Color.Green;

        int CropGrowth, intervaltime, TargetIntervalTime = 5000;//20000;

        private Ped PlayerPed = GTA.Game.Player.Character;

        //System.Timers.Timer SowingCropsTimer = new System.Timers.Timer();

        

        NativeUI.TimerBarPool Farming_myPool, Farming_myPool2;
        NativeUI.TextTimerBar Farming_MyBarText, Farming_MyBarText2;
        NativeUI.BarTimerBar Farming_myBar, Farming_myBar2;


        public Farming_Class()
        {            
            Tick += onTick;
            KeyDown += onKeyDown;
            _Farming_Menu_Bars();           
            intervaltime = Game.GameTime + TargetIntervalTime;
            
            container = new UIContainer(new Point((screen_width / 2 + 500), 0), new Size(PANEL_WIDTH, PANEL_HEIGHT), backColor);
            text = new UIText("Crop Growth ", new Point(PANEL_WIDTH / 2, 0), 0.60f, textColor, GTA.Font.Monospace, true);
            text2 = new UIText(CropName + " : 0%", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.75f, Color.BlueViolet, GTA.Font.Pricedown, true);
            //text2 = new UIText(CropName + " : 0%", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.75f, Color.BlueViolet, GTA.Font.Pricedown, true);


            container.Items.Add(text);
            container.Items.Add(text2);
            
        }//Default Constructor        

        private void onTick(object sender, EventArgs e)
        {
            if (MenuUI.modactive)
            {
                
                if (farming)
                {
                    _Farming_Funtion();
                    Farming_myPool.Draw();
                }
                else
                {
                    if (!farming && seedssowed)
                    {
                        if (Farming_myBar == null)
                        {
                            _Farming_Menu_Bars();
                        }

                        if (seedssowed)
                        {
                            if (CropGrowth < 100)
                            {
                                container.Draw();
                                if (intervaltime < Game.GameTime)
                                {
                                    Sowing_SeedsStatus();
                                }
                            }
                            else
                            {
                                Class1.DisplayHelpTextThisFrame("Crops have grown completly. Harvest them before they get spoiled.");
                                return;
                            }                            
                        }                        
                    }
                }
            }
        }//OnTick Function Ends here

        private void onKeyDown(object sender, KeyEventArgs e)
        {
            if (MenuUI.modactive)
            {
                if (e.KeyCode == Keys.E)
                {
                    //seedssowed = true;
                    //if (CropName == "")
                    //{
                    //    //CropName = "Wheat";
                    //    CropName = "Rice";
                    //}

                }
            }
        }//OnKeyDown Function Ends here

        private void _Farming_Funtion()
        {
            if (farming)
            {
                if (!PlayerPed.IsOnFoot)//if (PlayerPed.CurrentVehicle.Model == 1641462412)
                {
                    if (PlayerPed.CurrentVehicle == Vehicel_Shop_Dealer.PlayerTractor)
                    {
                        if (Vehicel_Shop_Dealer.PlayerTractor != null && Vehicel_Shop_Dealer.PlayerCultivator != null)//if (PlayerPed.CurrentVehicle.Model == 1641462412)
                        {
                            if (PlayerPed.CurrentVehicle.IsAttachedTo(Vehicel_Shop_Dealer.PlayerCultivator))
                            {
                                if (PlayerPed.CurrentVehicle == Vehicel_Shop_Dealer.PlayerTractor)
                                {
                                    if (PlayerPed.Position.X > 605f && PlayerPed.Position.X < 750f && PlayerPed.Position.Y > 6454f && PlayerPed.Position.Y < 6501)
                                    {
                                        if (Farming_myBar != null && Farming_myBar.Percentage < 1.0f)
                                        {
                                            if (!Game.IsControlPressed(2, GTA.Control.VehicleBrake) && PlayerPed.CurrentVehicle.Speed >= 1.5f && PlayerPed.CurrentVehicle.Speed <= 10.8f)
                                            {
                                                if (Farming_myBar.Percentage <= 1.1f)
                                                {
                                                    Farming_myBar.Percentage += 0.0005f;
                                                    Farming_myPool.Remove(Farming_MyBarText);
                                                    float percentage = (Farming_myBar.Percentage / 1.0f) * 100;
                                                    Farming_MyBarText = new TextTimerBar(TextPercentageText, ((int)percentage + "%").ToString());
                                                    Farming_myPool.Add(Farming_MyBarText);
                                                    if (Farming_myBar == null)
                                                    {
                                                        Farming_myPool.Add(Farming_myBar);
                                                    }
                                                    
                                                    //myPool.Add(myBar);
                                                }
                                                else
                                                {
                                                    
                                                }

                                            }
                                            else
                                            {
                                                UI.ShowSubtitle("Drive the tractor with Normal speed : 10 - 40 kmph");
                                            }
                                        }
                                        else
                                        {
                                            if (Farming_myBar.Percentage >= 1.0f)
                                            {
                                                NativeUI.BigMessageThread.MessageInstance.ShowColoredShard("Field has been Cultivated and Sowed", "Leave the Tractor here", NativeUI.HudColor.HUD_COLOUR_BLACK, NativeUI.HudColor.HUD_COLOUR_FACEBOOK_BLUE, 10000);
                                                GTA.Game.PlaySound("BASE_JUMP_PASSED", "HUD_AWARDS");
                                                Farming_myPool.Remove(Farming_myBar);
                                                Farming_myPool.Remove(Farming_MyBarText);
                                                RemoveCurrentJobFromQueue();
                                                Farming_myBar = null;
                                                farming = false;
                                                seedssowed = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        UI.ShowSubtitle("Only field can be cultivated the one you've purchased");
                                    }
                                }
                            }
                            else
                            {
                                if (World.GetDistance(PlayerPed.Position, Vehicel_Shop_Dealer.PlayerCultivator.Position) <= 20f && !PlayerPed.CurrentVehicle.IsAttachedTo(Vehicel_Shop_Dealer.PlayerCultivator))
                                {
                                    UI.ShowSubtitle("Attach the Cultivator by honking the horn when near it");
                                    if (Vehicel_Shop_Dealer.PlayerCultivator != null)
                                    {
                                        World.DrawMarker(MarkerType.UpsideDownCone, new Vector3(Vehicel_Shop_Dealer.PlayerCultivator.Position.X, Vehicel_Shop_Dealer.PlayerCultivator.Position.Y, Vehicel_Shop_Dealer.PlayerCultivator.Position.Z + 5.0f), new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f), new Vector3(1.0f, 1.0f, 1.0f), System.Drawing.Color.Red, true, false, 0, false, "", "", false);
                                    }
                                }
                            }                           
                        }
                    }
                    //farming = false;
                }
            }
        }

        private void RemoveCurrentJobFromQueue()
        {
            #region CancleJob Definition
            if (MenuUI.CurrentJobPlayer.Count > 0)
            {
                if (MenuUI.CurrentJobPlayer.Count > 0)
                {
                    UI.Notify("Job Removed : " + MenuUI.CurrentJobPlayer[0]);
                    MenuUI.CurrentJobPlayer.RemoveAt(0);
                }

                if (Game.IsWaypointActive)
                {
                    Function.Call(Hash.SET_WAYPOINT_OFF);
                }

                Function.Call(Hash.REFRESH_WAYPOINT);

                CropName = "";
                //CropName = null;

                XML_Import_Export.XMLSaving();
            }
            else
            {
                UI.Notify("No Active Jobs Found");
            }
            #endregion
        }

        private void _Farming_Menu_Bars()
        {
            if (Farming_myBar == null)
            {
                TextBar = "Wheat Seeds";
                TextPercentageText = "Cultivate & Sowing";

                Farming_myPool = new TimerBarPool();

                Farming_myBar = new BarTimerBar(TextBar);
                Farming_myBar.BackgroundColor = Color.Black;
                Farming_myBar.ForegroundColor = Color.Green;
                Farming_myBar.Percentage = 0.0f;
                Farming_myPool.Add(Farming_myBar);

                Farming_MyBarText = new TextTimerBar(TextPercentageText, ((int)Farming_myBar.Percentage).ToString());
                Farming_myPool.Add(Farming_MyBarText);
            }
        }       

        private void Sowing_SeedsStatus()
        {
            //if (CropGrowth < 100 && PreviousCropGrowth != CropGrowth)
            if (CropGrowth < 100)
            {
                CropGrowth += 01;
                container.Items.Remove(text2);
                //if (CropName == "BeetRoot")
                //{
                //    CropName = "Beet";
                //}
                //text2 = new UIText(CropName +" "+ CropGrowth + "%", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.75f, Color.BlueViolet, GTA.Font.Pricedown, true);
                text2 = new UIText(CropName + " : " + CropGrowth + "%", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.65f, Color.BlueViolet, GTA.Font.Pricedown, true);
                //text2 = new UIText(CropGrowth + "%", new Point(200, PANEL_HEIGHT / 2), 0.95f, Color.BlueViolet, GTA.Font.Pricedown, true);
                container.Items.Add(text2);
                intervaltime = Game.GameTime + TargetIntervalTime;                
            }                     
        }


    }//Main class ends here

}//Namespace Ends here
