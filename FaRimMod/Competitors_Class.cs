﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GTA;
using GTA.Native;
using GTA.Math;
using System.Drawing;
using System.Globalization;
using CustomExtention;

namespace CustomExtention
{
    public static class CustomExtensionMethod
    {
        public static string ToKMB(this decimal num)
        {
            if (num > 999999999999999999 || num < -999999999999999999)
            {
                return num.ToString("0,,,.#Quint", CultureInfo.InvariantCulture); //18 Digits
            }
            else if (num > 999999999999999 || num < -999999999999999)
            {
                return num.ToString("0,,,.#Quadr", CultureInfo.InvariantCulture); //15 Digits
            }
            else if (num > 999999999999 || num < -999999999999)
            {
                return num.ToString("0,,,.#T", CultureInfo.InvariantCulture); //12 Digits
            }
            else if (num > 999999999 || num < -999999999)
            {
                return num.ToString("0,,,.#B", CultureInfo.InvariantCulture); //09 Digits
            }
            else
            if (num > 999999 || num < -999999)
            {
                return num.ToString("0,,.#M", CultureInfo.InvariantCulture); //06 Digits
            }
            else
            if (num > 999 || num < -999)
            {
                return num.ToString("0,.#K", CultureInfo.InvariantCulture); //03 Digits
            }
            else
            {
                return num.ToString(CultureInfo.InvariantCulture);
            }
        }

    }//Default Class ends here

}//Custom Extension Namespace ends here
namespace FaRimMod
{
    
    class Competitors_Class : Script
    {
        public static bool MenuActive;
        UIContainer Rival_container, Player_container;
        UIText Rival_text, Rival_text2, Player_text, Player_text2;

        int screen_height = Game.ScreenResolution.Height;
        int screen_width = Game.ScreenResolution.Width;

        const int PANEL_WIDTH = 250;
        //const int PANEL_HEIGHT = 55;
        const int PANEL_HEIGHT = 55;

        Color backColor = Color.FromArgb(100, 255, 255, 255);
        Color textColor = Color.Red;
        Color textColor2 = Color.Green;

        int intervaltime, TargetIntervalTime = 10000, Player_Money_Previous;
        Random ran = new Random();

        Random ran2 = new Random();

        float Earnings;   
        public Competitors_Class()
        {
            Rival_container = new UIContainer(new Point((screen_width / 2 - 400), 300), new Size(PANEL_WIDTH, PANEL_HEIGHT), backColor);
            //Rival_text = new UIText("Rival " + RivalNumber + " Net Worth", new Point(PANEL_WIDTH / 2, 0), 0.60f, textColor, GTA.Font.Monospace, true);
            Rival_text = new UIText("Rival Net Worth", new Point(PANEL_WIDTH / 2, 0), 0.60f, textColor, GTA.Font.Monospace, true);
            Rival_text2 = new UIText("$0", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.65f, Color.BlueViolet, GTA.Font.ChaletLondon, true);

            Rival_container.Items.Add(Rival_text);
            Rival_container.Items.Add(Rival_text2);

            Player_container = new UIContainer(new Point((screen_width / 2 - 400), Rival_container.Position.Y + 65), new Size(PANEL_WIDTH, PANEL_HEIGHT), backColor);
            Player_text = new UIText("Player Net Worth", new Point(PANEL_WIDTH / 2, 0), 0.60f, textColor, GTA.Font.Monospace, true);
            Player_text2 = new UIText("$0", new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.65f, Color.BlueViolet, GTA.Font.ChaletLondon, true);

            Player_container.Items.Add(Player_text);
            Player_container.Items.Add(Player_text2);

            Player_Money_Previous = Class1.XPPoints.PlayerMoney;

            intervaltime = Game.GameTime + TargetIntervalTime;

            Tick += onTick;
            

        }//Default Constructor Ends here

   
        private void onTick(object sender, EventArgs e)
        {
            if (MenuUI.modactive)
            {
                if (MenuUI.DisplayCompititorStat.Checked)
                {
                    if (!MenuActive)                    
                    {
                        Rival_container.Draw();
                        Player_container.Draw();
                    }                   
                }
                if (intervaltime < Game.GameTime)
                {
                    Compititor_Status();
                    Player_Status();
                }
            }
        }
        private void Player_Status()
        {
            if (MenuUI.modactive)
            {
                if (MenuUI.DisplayCompititorStat.Checked)
                {  
                    if (intervaltime < Game.GameTime)
                    {
                        string arrowhandling = "";
                        string transformedfigure2 = ((decimal)Class1.XPPoints.PlayerMoney).ToKMB();
                        Player_container.Items.Remove(Player_text2);
                        if (Class1.XPPoints.PlayerMoney > Player_Money_Previous)
                        {
                            arrowhandling = char.ConvertFromUtf32(0x2191);
                        }
                        else if (Class1.XPPoints.PlayerMoney == Player_Money_Previous && Class1.XPPoints.PlayerMoney != 0)//else if (int.Parse(transformedfigure2) == Player_Money_Previous)
                        {
                            arrowhandling = "=";
                        }
                        else if (Class1.XPPoints.PlayerMoney < Player_Money_Previous)
                        {
                            arrowhandling = char.ConvertFromUtf32(0x2193);
                        }                        
                        Player_text2 = new UIText("$" + transformedfigure2 + " " + arrowhandling, new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.65f, Color.BlueViolet, GTA.Font.ChaletLondon, true);
                        Player_container.Items.Add(Player_text2);
                        intervaltime = Game.GameTime + TargetIntervalTime;
                        Player_Money_Previous = Class1.XPPoints.PlayerMoney;                        
                    }
                }
            }
        }



        private void Compititor_Status()
        {
            if (MenuUI.modactive)
            {
                if (MenuUI.DisplayCompititorStat.Checked)
                {
                    if (intervaltime < Game.GameTime)
                    {
                        string arrowhandling = "";
                        int rand = ran.Next(0, 3);
                        int random2 = 0;

                        if (Class1.XPPoints.PlayerMoney <= 1000)
                        {
                            random2 = ran2.Next(99, 999);
                        }
                        else if (Class1.XPPoints.PlayerMoney > 1000 && Class1.XPPoints.PlayerMoney <= 100000)
                        {
                            random2 = ran2.Next(999, 99999);
                        }


                        if (Earnings > Class1.XPPoints.PlayerMoney)
                        {
                            if (rand == 0)
                            {                                
                                if ((random2) <= (Class1.XPPoints.PlayerMoney / 100 * 20))
                                {
                                    Earnings += random2;
                                    arrowhandling = char.ConvertFromUtf32(0x2191);
                                }
                                else
                                {
                                    arrowhandling = "=";
                                }

                            }
                            else if (rand == 1)
                            {
                                if (Earnings > random2 && (Earnings - random2) > Class1.XPPoints.PlayerMoney)
                                {
                                    Earnings -= random2;
                                    arrowhandling = char.ConvertFromUtf32(0x2193);
                                }
                                else
                                {
                                    arrowhandling = "=";
                                }
                                
                            }
                            else if (rand == 2)
                            {
                                //Earnings -= random2;
                                arrowhandling = "=";
                            }

                        }
                        else
                        {
                            Earnings += ((Class1.XPPoints.PlayerMoney + random2) / 100 * 50);
                        }


                        //EarningsinDecimal = Math.Round((decimal)Earnings, 2);
                        string transformedfigure = ((decimal)Earnings).ToKMB();
                        Rival_container.Items.Remove(Rival_text2);
                        Rival_text2 = new UIText("$" + transformedfigure + " " +arrowhandling, new Point(PANEL_WIDTH / 2, PANEL_HEIGHT / 2), 0.65f, Color.BlueViolet, GTA.Font.ChaletLondon, true);                        
                        Rival_container.Items.Add(Rival_text2);
                        //UI.ShowSubtitle("Randome Number was : " + rand);
                    }
                }
            }
        }
    }//Main Class Ends here

}//Namespace Ends here
